import 'package:flutter/material.dart';

final primary = const Color(0xFF437BC1);
final primaryLight = const Color(0xFFE6EFF7);

TextStyle authHeader() {
  return TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 1.0,
***REDACTED***
