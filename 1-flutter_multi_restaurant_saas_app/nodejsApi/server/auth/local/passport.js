import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
 
function localAuthenticate(User, email, password, done) {
  User.findOne({
    email: email.toLowerCase()
  }).exec()
    .then(user => {
      if(!user) {
        return done(null, false, {
***REDACTED***
