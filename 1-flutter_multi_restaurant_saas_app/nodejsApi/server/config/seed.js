/**   
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Location from '../api/location/location.model';
import Order from '../api/order/order.model';
import Category from '../api/category/category.model';
***REDACTED***
