/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import SubcategoryEvents from './subcategory.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
