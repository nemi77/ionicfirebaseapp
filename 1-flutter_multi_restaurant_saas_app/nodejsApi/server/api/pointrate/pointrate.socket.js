/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import PointrateEvents from './pointrate.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
