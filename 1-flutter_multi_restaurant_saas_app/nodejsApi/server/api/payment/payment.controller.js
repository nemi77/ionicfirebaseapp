/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/payments              ->  index
 * POST    /api/payments              ->  create
 * GET     /api/payments/:id          ->  show
 * PUT     /api/payments/:id          ->  upsert
 * PATCH   /api/payments/:id          ->  patch
 * DELETE  /api/payments/:id          ->  destroy
 */

***REDACTED***
