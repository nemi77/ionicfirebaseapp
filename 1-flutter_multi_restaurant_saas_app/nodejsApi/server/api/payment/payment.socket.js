/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import PaymentEvents from './payment.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
