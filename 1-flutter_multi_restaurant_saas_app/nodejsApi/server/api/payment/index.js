'use strict';
//currently not using this model
var express = require('express');
var controller = require('./payment.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

router.get('/', auth.hasRole('Admin'),controller.index);

router.get('/:id', auth.hasRole('Admin'),controller.show);
***REDACTED***
