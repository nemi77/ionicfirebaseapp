'use strict';

var express = require('express'); 
var controller = require('./wallet.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Wallets
router.get('/',  auth.isAuthenticated(),controller.index);

***REDACTED***
