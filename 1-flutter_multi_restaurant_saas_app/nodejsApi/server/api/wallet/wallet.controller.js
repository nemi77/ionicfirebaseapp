/**restaurantOwnerWalletInfo orderCollection
 * Using Rails-like standard naming convention for endpoints. orderCollection
 * GET     /api/wallets              ->  index
 * POST    /api/wallets              ->  create
 * GET     /api/wallets/:id          ->  show
 * PUT     /api/wallets/:id          ->  upsert
 * PATCH   /api/wallets/:id          ->  patch
 * DELETE  /api/wallets/:id          ->  destroy
 */

***REDACTED***
