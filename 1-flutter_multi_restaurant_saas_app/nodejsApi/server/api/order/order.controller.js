
/**  
 * Using Rails-like standard naming convention for endpoints. dashboardData
 * GET     /api/orders              ->  index minOrdLoyality
 * POST    /api/orders              ->  create upsert
 * GET     /api/orders/:id          ->  show
 * PUT     /api/orders/:id          ->  upsert search allRestaurantCollectionInfos
 * PATCH   /api/orders/:id          ->  patch
 * DELETE  /api/orders/:id          ->  destroy
 */
***REDACTED***
