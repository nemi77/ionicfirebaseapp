/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/deliveryareas              ->  index
 * POST    /api/deliveryareas              ->  create
 * GET     /api/deliveryareas/:id          ->  show
 * PUT     /api/deliveryareas/:id          ->  upsert
 * PATCH   /api/deliveryareas/:id          ->  patch
 * DELETE  /api/deliveryareas/:id          ->  destroy
 */

***REDACTED***
