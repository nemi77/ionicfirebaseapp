/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ProductratingEvents from './productrating.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
