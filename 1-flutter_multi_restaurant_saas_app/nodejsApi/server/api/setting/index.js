'use strict'; 

var express = require('express');
var controller = require('./setting.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

// Gets a list of Settings
router.get('/', auth.hasRole('Owner'), controller.index);
***REDACTED***
