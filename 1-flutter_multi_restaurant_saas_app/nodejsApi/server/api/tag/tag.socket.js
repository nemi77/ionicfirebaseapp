/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import TagEvents from './tag.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
