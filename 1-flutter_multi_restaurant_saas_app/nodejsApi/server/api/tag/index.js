'use strict';

var express = require('express');
var controller = require('./tag.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Tags by location id
router.get('/all/:id', auth.isAuthenticated(),controller.index);

***REDACTED***
