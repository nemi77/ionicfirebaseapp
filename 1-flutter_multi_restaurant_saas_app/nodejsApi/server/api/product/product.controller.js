/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products/restaurant/:id     ->  index 
 * POST    /api/products                    ->  create
 * GET     /api/products/:id                ->  show
 * PUT     /api/products/:id/:flag          ->  upsert
 * DELETE  /api/products/:id                ->  destroy
 */

'use strict'; 
***REDACTED***
