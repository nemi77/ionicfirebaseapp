/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ProductEvents from './product.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
