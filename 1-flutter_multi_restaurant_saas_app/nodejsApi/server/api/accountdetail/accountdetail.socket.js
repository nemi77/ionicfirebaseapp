/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import AccountdetailEvents from './accountdetail.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
