'use strict';

var express = require('express');
var controller = require('./notification.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Notifications
router.get('/', controller.index);

***REDACTED***
