/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import LocationEvents from './location.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
