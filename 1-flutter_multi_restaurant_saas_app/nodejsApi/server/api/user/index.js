'use strict'; 

import {Router} from 'express';
import * as controller from './user.controller';    
var auth = require('../../auth/auth.service');      

var router = new Router(); 

//get verification 
router.get('/contact/verify', auth.isAuthenticated(), controller.contactNoVerify);
***REDACTED***
