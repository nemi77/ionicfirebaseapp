'use strict';
 //enableAllCategoryList
var express = require('express');
var controller = require('./category.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Categories of a single restaurant
router.get('/restaurant/:id',auth.isAuthenticated(), controller.index);

***REDACTED***
