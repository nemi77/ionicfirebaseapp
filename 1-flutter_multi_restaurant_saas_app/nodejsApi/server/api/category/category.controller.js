/**byLocation
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/categories/restaurant/:id     ->  index byLocation
 * POST    /api/categories/:flag   tax        ->  create
 * GET     /api/categories/:id                ->  show
 * PUT     /api/categories/:id/:flag          ->  upsert
 * DELETE  /api/categories   byLocation       ->  destroy
 */
        
'use strict';
***REDACTED***
