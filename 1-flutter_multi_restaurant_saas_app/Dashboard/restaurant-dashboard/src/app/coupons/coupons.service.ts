import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class CouponsService {
  constructor(private crud: CrudService) { }
  // returns all coupons
  public getAllCouponData(id: string) {
    return this.crud.get('coupons/'+id);
  }

***REDACTED***
