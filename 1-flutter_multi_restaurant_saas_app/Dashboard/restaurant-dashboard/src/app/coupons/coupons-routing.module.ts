import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CouponsComponent } from './coupons.component';
import { AddCouponComponent } from './add-coupon/add-coupon.component';
import { UpdateCouponComponent } from './update-coupon/update-coupon.component';

const routes: Routes = [
    {
        path: '',
        component: CouponsComponent,
***REDACTED***
