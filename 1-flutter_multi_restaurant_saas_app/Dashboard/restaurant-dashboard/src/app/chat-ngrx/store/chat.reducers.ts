import { Chat } from '../chat-ngrx.model';
import * as ChatActions from './chat.actions';

export interface FeatureState {
    chat: State;
}

export interface State {
    chat1: Chat[];
    chat2: Chat[];
***REDACTED***
