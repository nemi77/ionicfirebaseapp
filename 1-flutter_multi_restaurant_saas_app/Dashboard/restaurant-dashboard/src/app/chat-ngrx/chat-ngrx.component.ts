import { Component, ViewChild, ElementRef, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Chat } from './chat-ngrx.model';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as ChatActions from './store/chat.actions';
import * as fromChat from './store/chat.reducers';

***REDACTED***
