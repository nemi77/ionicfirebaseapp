import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class SalesService {
    constructor(private crud: CrudService) { }

    getSalesDataByLocationId(locationId) { // get sales data by location Id
        return this.crud.getOne('orders/delivered/location', locationId);
    }
    getSalesCountsByLocationId(locationId) { // get Sales Counts data by location Id
***REDACTED***
