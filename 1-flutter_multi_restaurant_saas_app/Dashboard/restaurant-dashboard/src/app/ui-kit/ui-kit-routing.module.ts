import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GridsComponent } from "./grids/grids.component";
import { TypographyComponent } from "./typography/typography.component";
import { HelperClassesComponent } from "./helper-classes/helper-classes.component";
import { SyntaxHighlighterComponent } from "./syntax-highlighter/syntax-highlighter.component";
import { TextUtilitiesComponent } from "./text-utilities/text-utilities.component";
import { FeatherComponent } from './icons/feather/feather.component';
import { FontAwesomeComponent } from './icons/font-awesome/font-awesome.component';
***REDACTED***
