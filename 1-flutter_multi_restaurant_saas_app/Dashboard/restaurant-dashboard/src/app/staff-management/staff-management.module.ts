import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { StaffManagementRoutingModule } from "./staff-management-routing.module";
import { StaffManagementComponent } from "./staff-management.component";
import { ViewStaffComponent } from './view-staff/view-staff.component';
import { AddStaffComponent } from './add-staff/add-staff.component'
import { UpdateStaffComponent } from './update-staff/update-staff.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { FormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
***REDACTED***
