import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class StaffService {
    constructor(private crud: CrudService) { }

    // returns all staff data
    public getAllMemberData() {
        return this.crud.get('users/all/staff/');
    }
***REDACTED***
