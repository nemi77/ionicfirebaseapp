import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {StaffManagementComponent} from './staff-management.component';
import {AddStaffComponent} from './add-staff/add-staff.component'
import {ViewStaffComponent} from './view-staff/view-staff.component'
import {UpdateStaffComponent} from './update-staff/update-staff.component'
const routes: Routes = [
    {
      path: '',
      component: StaffManagementComponent,
***REDACTED***
