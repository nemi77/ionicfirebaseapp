import { Component, ElementRef, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';

import { debounceTime} from 'rxjs/operators';
import { distinctUntilChanged} from 'rxjs/operators';

// override p with div tag
***REDACTED***
