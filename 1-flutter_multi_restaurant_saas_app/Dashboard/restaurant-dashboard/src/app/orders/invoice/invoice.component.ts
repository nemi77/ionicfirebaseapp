import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import { OrdersService } from '../order.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
  providers: [OrdersService, ToastrService]
***REDACTED***
