import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { OrdersRoutingModule } from "./orders-routing.module";
import { OrdersComponent } from "./orders.component";
import { ViewOrdersComponent } from "./view-orders/view-orders.component"
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomFormsModule } from "ng2-validation";
import { InvoiceComponent } from './invoice/invoice.component';
import { LaddaModule } from 'angular2-ladda';

***REDACTED***
