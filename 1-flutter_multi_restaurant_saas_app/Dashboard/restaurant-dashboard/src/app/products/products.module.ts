import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { ProductsRoutingModule } from "./products-routing.module";
import { ProductsComponent } from "./products.component";
import { ViewProductComponent } from './view-product/view-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { UiSwitchModule } from "ngx-ui-switch";
import { AddProductsComponent } from './add-products/add-products.component';
***REDACTED***
