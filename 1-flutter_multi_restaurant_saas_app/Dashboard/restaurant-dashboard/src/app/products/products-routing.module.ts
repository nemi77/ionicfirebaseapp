import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProductsComponent} from './products.component';
import {ViewProductComponent} from './view-product/view-product.component';
import {EditProductComponent} from './edit-product/edit-product.component';
import {AddProductsComponent} from './add-products/add-products.component';
import { ProductByLocationComponent } from './product-by-location/product-by-location/product-by-location.component';
const routes: Routes = [
  {
    path: '',
***REDACTED***
