import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';
const routes: Routes = [
    {
        path: '', component: CustomerComponent, data: {
            title: 'Customers'
        }
    },
***REDACTED***
