import { Component, OnInit } from '@angular/core';
import { CustomerService } from './customer.service';
import { ToastrService } from '../components/extra/toastr/toastr.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [CustomerService, ToastrService]
})
***REDACTED***
