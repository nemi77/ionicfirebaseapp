import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CustomerRoutingConfig } from './customer-routing.module';
import { CustomFormsModule } from 'ng2-validation';
import { CustomerComponent } from './customer.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';
@NgModule({
    imports: [
        CommonModule,
***REDACTED***
