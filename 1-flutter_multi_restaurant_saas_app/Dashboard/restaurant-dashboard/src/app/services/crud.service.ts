import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from './constant.service';
@Injectable()
export class CrudService extends ConstantService {

    headers: HttpHeaders;
    authToken: string;
    constructor(private http: HttpClient, private _constantService: ConstantService) {
***REDACTED***
