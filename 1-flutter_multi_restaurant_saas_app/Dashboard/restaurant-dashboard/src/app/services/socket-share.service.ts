'use strict';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { ConstantService } from './constant.service';
@Injectable()
export class SocketShareService {
  socket: any;
  constructor(private _constantService: ConstantService) {
    this.socket = io.connect(this._constantService.SOCKET_URL);
***REDACTED***
