import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
import * as chartsData from '../../shared/data/chartjs';
import { LocationsService } from '../locations.service';
import { ToastrService } from 'ngx-toastr';
declare var require: any;

const data: any = require('../../shared/data/chartist.json');
***REDACTED***
