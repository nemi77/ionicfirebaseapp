import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { LocationRoutingModule } from "./location-routing.module";
import { LocationComponent } from "./location.component";
import { UiSwitchModule } from 'ngx-ui-switch';
import { EditLocationComponent } from './edit-location/edit-location.component';
import { ViewLocationComponent } from './view-location/view-location.component';
import { AddLocationComponent } from './add-location/add-location.component';
import { LocationsService } from './locations.service';
import { ToastrModule } from 'ngx-toastr';
***REDACTED***
