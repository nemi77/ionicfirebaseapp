import { Task } from '../taskboard-ngrx.model';
import * as TaskActions from './taskboard.actions';

export interface FeatureState {
    task: State;
}

export interface State {
    todo: Task[];
    inProcess: Task[];
***REDACTED***
