import { Component, ViewEncapsulation, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Task } from './taskboard-ngrx.model';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as TaskboadActions from './store/taskboard.actions';
import * as fromTaskboard from './store/taskboard.reducers';

***REDACTED***
