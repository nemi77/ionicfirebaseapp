import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../payments.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss'],
  providers: [PaymentsService, ToastrService]
***REDACTED***
