import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../payments.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
  providers: [PaymentsService, ToastrService]
})
***REDACTED***
