import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NotificationRoutingModule } from "./notification-routing.module";
import { NotificationComponent } from "./notification.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomFormsModule } from "ng2-validation";
import { HttpClientModule } from '@angular/common/http';
import { LaddaModule } from 'angular2-ladda';

@NgModule({
***REDACTED***
