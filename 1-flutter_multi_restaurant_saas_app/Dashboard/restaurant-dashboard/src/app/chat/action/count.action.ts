import {Action} from '@ngrx/store';
export const UPDATE_COUNT = '[Count] Update';
export const DOWN_COUNT = '[Count] Down';
export const SET = '[Count] Set';
export const ASSIGN_ID = '[Count] AssignId';


export class UpdateCount implements Action {
  readonly type = UPDATE_COUNT;

***REDACTED***
