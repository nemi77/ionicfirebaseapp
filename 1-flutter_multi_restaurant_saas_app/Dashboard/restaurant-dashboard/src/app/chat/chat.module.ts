import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { ChatRoutingModule } from "./chat-routing.module";
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ChatComponent } from "./chat.component";
***REDACTED***
