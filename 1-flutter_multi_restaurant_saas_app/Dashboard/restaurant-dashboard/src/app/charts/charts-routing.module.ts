import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartistComponent } from "./chartist/chartist.component";
import { ChartjsComponent } from "./chartjs/chartjs.component";
import { NGXChartsComponent } from "./ngx-charts/ngx-charts.component";

const routes: Routes = [
  {
    path: '',     
***REDACTED***
