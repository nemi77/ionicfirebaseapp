import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';

@Component({
  selector: 'app-delivery-area',
  templateUrl: './delivery-area.component.html',
  styleUrls: ['./delivery-area.component.scss'],
  providers: [SettingsService, ToastrService]
})
***REDACTED***
