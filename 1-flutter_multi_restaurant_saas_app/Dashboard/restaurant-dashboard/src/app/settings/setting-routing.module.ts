import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from "./settings.component";
import { TaxInfoComponent } from './tax-info/tax-info.component'
import { TagsComponent } from './tags/tags.component';
import { AddTagComponent } from './tags/add-tag/add-tag.component';
import { UpdateTagComponent } from './tags/update-tag/update-tag.component';
import { DeliveryAreaComponent } from './delivery-area/delivery-area.component';
import { WorkingHoursComponent } from './working-hours/working-hours.component';
import { OptionsComponent } from './options/options.component';
***REDACTED***
