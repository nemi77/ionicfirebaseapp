import { Injectable } from '@angular/core';
import { ConstantService } from "../../services/constant.service";
import { CrudService } from '../../services/crud.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class ContentPagesService {
  constructor(private constantService: ConstantService, private http: HttpClient, private crud: CrudService) {
  }

***REDACTED***
