import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms'
import { ContentPagesService } from '../content-pages.service';
import { CustomValidators } from 'ng2-validation';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

const password = new FormControl('', [Validators.required, Validators.minLength(6)]);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));
***REDACTED***
