import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { DiscountRoutingModule } from "./discount-routing.module";
import { DiscountComponent } from "./discount.component";
import { LoyalityComponent } from './loyality/loyality.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { LaddaModule } from 'angular2-ladda';


***REDACTED***
