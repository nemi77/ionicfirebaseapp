import { Injectable } from '@angular/core';
import { CrudService } from '../../services/crud.service';
@Injectable()
export class LoyaltyService {
    constructor(private crud: CrudService) { }
    
    // returns loyalty information
    public getAllLoyaltyData() {
        return this.crud.get('settings');
    }
***REDACTED***
