import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class MyInfoService {
    constructor(private crud: CrudService) { }
    public getUserInfo() {
        return this.crud.get('users/me');
    }

    // update user
***REDACTED***
