import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyInfoComponent } from './my-info.component';
import {MyInfoUpdateComponent} from './my-info-update/my-info-update.component';

const routes: Routes = [
  { path: '',
    component: MyInfoComponent,
    data: {
      title: 'MyInfo'
***REDACTED***
