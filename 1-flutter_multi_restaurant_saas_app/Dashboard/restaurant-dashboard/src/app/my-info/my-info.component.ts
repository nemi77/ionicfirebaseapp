import { Component, OnInit } from '@angular/core';
import { MyInfoService } from './my-info.service'
import { ToastrService } from '../components/extra/toastr/toastr.service';
@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  styleUrls: ['./my-info.component.scss'],
  providers: [MyInfoService, ToastrService]
})
export class MyInfoComponent implements OnInit {
***REDACTED***
