import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { TaskBoardService } from './taskboard.service';
import { Task } from './taskboard.model';

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.scss'],
  providers: [TaskBoardService],
  encapsulation: ViewEncapsulation.None
***REDACTED***
