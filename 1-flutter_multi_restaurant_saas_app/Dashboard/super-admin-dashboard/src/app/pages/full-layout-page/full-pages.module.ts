import { ViewAdvertisementComponent } from './../restaurant/view-advertisement/view-advertisement.component';
import { AdvertisementComponent } from './../restaurant/advertisement/advertisement.component';
import { LaddaModule } from 'angular2-ladda';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartistModule } from 'ng-chartist';
import { MatchHeightModule } from '../../shared/directives/match-height.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
***REDACTED***
