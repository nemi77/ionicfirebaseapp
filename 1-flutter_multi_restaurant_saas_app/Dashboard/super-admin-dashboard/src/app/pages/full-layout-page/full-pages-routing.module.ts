import { ViewAdvertisementComponent } from './../restaurant/view-advertisement/view-advertisement.component';
import { AdvertisementComponent } from './../restaurant/advertisement/advertisement.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutPageComponent } from 'app/pages/full-layout-page/full-layout-page.component';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { RestaurantComponent } from '../../pages/restaurant/restaurant.component';
import { AddRestaurantComponent } from '../restaurant/add-restaurant/add-restaurant.component';
import { ViewRestaurantComponent } from '../restaurant/view-restaurant/view-restaurant.component';
import { ChatComponent } from '../restaurant/chat/chat.component'
***REDACTED***
