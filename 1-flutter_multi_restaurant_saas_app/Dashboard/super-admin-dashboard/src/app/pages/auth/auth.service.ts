import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from '../../services/constant.service';
@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient, private constantService: ConstantService) { }
    login(data) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
***REDACTED***
