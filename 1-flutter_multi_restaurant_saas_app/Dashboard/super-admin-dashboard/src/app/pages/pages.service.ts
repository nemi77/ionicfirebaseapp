import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';

@Injectable()
export class PagesService {
    public cloudinarUpload: any;
    constructor(private crud: CrudService) {
        this.cloudinarUpload = crud.cloudinaryConfig;
    }

***REDACTED***
