import { Component, ViewChild, ElementRef, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { userlist } from './chat-ngrx.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as CountAction from './chat-store/count.action';
import { UnreadCount } from './chat-store/count';
import { ChatService } from './chat.service';
import { SocketSharedService } from '../../../services/socket-share.service';
import { NgForm } from '@angular/forms';
***REDACTED***
