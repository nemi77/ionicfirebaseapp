import { ToastrService } from 'ngx-toastr';
import { PagesService } from './../../pages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.scss'],
  providers: [PagesService]
***REDACTED***
