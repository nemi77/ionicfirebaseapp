import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

  {
    path: '/full-layout/dashboard', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '/full-layout/restaurant', title: 'Restaurant', icon: 'fa fa-cutlery', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
***REDACTED***
