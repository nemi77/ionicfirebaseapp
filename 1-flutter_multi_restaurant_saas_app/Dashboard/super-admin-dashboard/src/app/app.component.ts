import { Component } from '@angular/core';
import { PagesService } from './pages/pages.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    providers: [PagesService]
})
export class AppComponent {
***REDACTED***
