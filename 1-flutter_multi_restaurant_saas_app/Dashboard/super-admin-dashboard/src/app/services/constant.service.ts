import { environment } from "environments/environment";

export class ConstantService {
    public LOGIN_AUTH: string;  // contains login url
    public API_ENDPOINT: string;  // contains crud url
    public SOCKET_URL: string;
    public cloudinaryConfig: any;
    constructor() {
        this.API_ENDPOINT = environment.API_ENDPOINT + 'api/';
        this.LOGIN_AUTH = environment.API_ENDPOINT;
***REDACTED***
