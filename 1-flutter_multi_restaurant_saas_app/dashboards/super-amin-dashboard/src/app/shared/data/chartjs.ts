// barChart
export var barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true,
  maintainAspectRatio: false

};
export var barChartLabels: string[] = [];
export var barChartType = 'bar';
export var barChartLegend = true;
***REDACTED***
