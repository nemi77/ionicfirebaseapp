import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as CountAction from '../../pages/restaurant/chat/chat-store/count.action';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UnreadCount } from '../../pages/restaurant/chat/chat-store/count';
import { NavBarService } from './navbar.service';
import { SocketSharedService } from '../../services/socket-share.service';

***REDACTED***
