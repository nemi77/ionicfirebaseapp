export interface userlist {
  name: string;
  _id: number;
  image: string;
  email: string;
  count: number;
  lastmsgTime: string;
  lastMessage: string;
}

***REDACTED***
