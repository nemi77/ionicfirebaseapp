import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';
@Injectable()
export class ChatService {
    constructor(private crud: CrudService) { }
    getRestaurantChatList() {  // If user role MANAGER
        return this.crud.get('messages/owner/list/by/admin');
    }


***REDACTED***
