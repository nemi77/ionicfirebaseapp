import { Injectable } from '@angular/core';
import { CrudService } from '../../../../services/crud.service';

@Injectable()
export class SettingsService {
  constructor(private crud: CrudService) { }

  getLoggedInUserDetails() {
    return this.crud.get('users/me');
  }
***REDACTED***
