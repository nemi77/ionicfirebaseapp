import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { CookieModule } from 'ngx-cookie';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
***REDACTED***
