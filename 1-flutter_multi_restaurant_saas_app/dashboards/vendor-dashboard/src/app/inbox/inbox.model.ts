export class Mail {
  public mailId: number;
  public mailFrom: string;
  public subject: string;
  public body: string;
  public time: string;
  public isRead: boolean;
  public isImportant: boolean;
  public hasAttachment: boolean;
  public hasImage: boolean;
***REDACTED***
