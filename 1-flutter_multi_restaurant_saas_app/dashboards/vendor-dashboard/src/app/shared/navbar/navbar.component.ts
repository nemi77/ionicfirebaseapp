import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import { CrudService } from '../../services/crud.service';
import { SocketShareService } from 'app/services/socket-share.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
***REDACTED***
