import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as CountAction from './action/count.action';
import { UnreadCount } from './action/count';

import { ChatService } from './chat.service';
import { SocketShareService } from '../services/socket-share.service';
import { userlist } from './chat';
***REDACTED***
