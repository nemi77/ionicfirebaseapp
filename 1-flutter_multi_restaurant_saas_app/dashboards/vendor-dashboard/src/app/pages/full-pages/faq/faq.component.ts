import { Component } from '@angular/core';
import { FaqService } from './faq.service';
import { FAQ } from './faq.model';
import { Object } from 'core-js/library/web/timers';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  providers: [FaqService]
***REDACTED***
