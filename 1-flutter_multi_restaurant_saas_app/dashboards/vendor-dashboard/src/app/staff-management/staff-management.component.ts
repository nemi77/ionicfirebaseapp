import { Component, OnInit } from '@angular/core';
import { StaffService } from './staff.service';
import { ToastrService } from '../components/extra/toastr/toastr.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-staff-management',
  templateUrl: './staff-management.component.html',
  styleUrls: ['./staff-management.component.scss'],
  providers: [StaffService, ToastrService]
})
***REDACTED***
