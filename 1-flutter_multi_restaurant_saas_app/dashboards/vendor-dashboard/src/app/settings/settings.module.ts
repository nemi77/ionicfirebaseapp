import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { SettingsRoutingModule } from "./setting-routing.module";
import { SettingsComponent } from "./settings.component";
import { TaxInfoComponent } from './tax-info/tax-info.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { TagsComponent } from './tags/tags.component';
import { AddTagComponent } from './tags/add-tag/add-tag.component';
import { UpdateTagComponent } from './tags/update-tag/update-tag.component';
***REDACTED***
