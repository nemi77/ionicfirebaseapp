import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class SettingsService {
    constructor(private crud: CrudService) { }

    //======================Tax Section===========================//
    public getTaxData(restaurant: any) { // Tax save inside Restaurant Info ;
        return this.crud.getOne('users', restaurant);
    }
***REDACTED***
