import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';

@Component({
  selector: 'app-tax-info',
  templateUrl: './tax-info.component.html',
  styleUrls: ['./tax-info.component.scss'],
  providers: [SettingsService, ToastrService]
})
***REDACTED***
