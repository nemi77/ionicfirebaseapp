import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  providers: [SettingsService, ToastrService]
***REDACTED***
