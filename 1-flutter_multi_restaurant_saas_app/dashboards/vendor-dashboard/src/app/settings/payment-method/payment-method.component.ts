import { Component } from '@angular/core';
import { options } from 'app/shared/data/dropdowns';
import { SettingsService } from '../settings.service';
import { ToastrService } from 'app/components/extra/toastr/toastr.service';

@Component({
    selector: 'app-payment-method',
    templateUrl: './payment-method.component.html',
    styleUrls: ['./payment-method.component.scss'],
    providers: [SettingsService, ToastrService]
***REDACTED***
