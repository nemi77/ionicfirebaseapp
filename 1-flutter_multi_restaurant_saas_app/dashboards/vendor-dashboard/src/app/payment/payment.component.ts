import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentsService } from './payments.service';
import { ToastrService } from '../components/extra/toastr/toastr.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
  providers: [PaymentsService, ToastrService]
***REDACTED***
