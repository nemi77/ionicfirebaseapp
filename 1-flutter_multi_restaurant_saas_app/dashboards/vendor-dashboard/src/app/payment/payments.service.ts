import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class PaymentsService {
  constructor(private crud: CrudService) { }
  getLocationSalesHistory(restaurantId) {
    return this.crud.getOne('wallets/collection/details', restaurantId);
  }


***REDACTED***
