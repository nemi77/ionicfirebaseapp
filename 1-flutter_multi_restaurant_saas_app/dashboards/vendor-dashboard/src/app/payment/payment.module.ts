import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { PaymentRoutingModule } from "./payment-routing.module";
import { PaymentComponent } from "./payment.component";
import { BankComponent } from './bank/bank.component';
import { WalletComponent } from './wallet/wallet.component';
import { WithdrawlComponent } from './withdrawl/withdrawl.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ChartistModule } from "ng-chartist";
***REDACTED***
