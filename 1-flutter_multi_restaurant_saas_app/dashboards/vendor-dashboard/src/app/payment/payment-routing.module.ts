import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { PaymentComponent } from "./payment.component";
import { BankComponent } from './bank/bank.component';
import { WalletComponent } from './wallet/wallet.component';
import { WithdrawlComponent } from './withdrawl/withdrawl.component';
const routes: Routes = [
    {
      path: '',
      component: PaymentComponent,
***REDACTED***
