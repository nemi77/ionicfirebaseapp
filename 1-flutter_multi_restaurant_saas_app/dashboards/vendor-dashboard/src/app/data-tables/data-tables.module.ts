import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesRoutingModule } from "./data-tables-routing.module";

import { DTFullScreenComponent } from "./fullscreen/dt-fullscreen.component";
import { DTEditingComponent } from "./editing/dt-editing.component";
import { DTFilterComponent } from "./filter/dt-filter.component";
import { DTPagingComponent } from "./paging/dt-paging.component";
***REDACTED***
