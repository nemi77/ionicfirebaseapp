import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from '../order.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.scss'],
  providers: [OrdersService, ToastrService]
***REDACTED***
