import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersService } from './order.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { ToastrService } from '../components/extra/toastr/toastr.service';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  providers: [OrdersService, ToastrService]
***REDACTED***
