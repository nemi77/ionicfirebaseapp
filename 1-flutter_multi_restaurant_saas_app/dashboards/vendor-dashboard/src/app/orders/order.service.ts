import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class OrdersService {
    constructor(private crud: CrudService) { }
    // get all location
    public getAllLocationsData(myId) {
        return this.crud.getOne('locations/all/data', myId);
    }

***REDACTED***
