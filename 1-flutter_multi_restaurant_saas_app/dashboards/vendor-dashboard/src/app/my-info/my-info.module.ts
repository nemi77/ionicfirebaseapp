import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { MyInfoRoutingModule } from './my-info-routing.module';
import { MyInfoComponent } from './my-info.component';
import { MyInfoUpdateComponent } from './my-info-update/my-info-update.component';
import { LaddaModule } from 'angular2-ladda';
***REDACTED***
