import { CloudinaryUploader, CloudinaryOptions } from 'ng2-cloudinary';
import { Component, OnInit } from '@angular/core';
import { MyInfoService } from '../my-info.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-my-info-update',
  templateUrl: './my-info-update.component.html',
***REDACTED***
