import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { CategoriesRoutingModule } from "./categories-routing.module";
import { CategoriesComponent } from './categories.component';
import { CategoriesByLocationComponent } from './categories-by-location/categories-by-location.component'
import { AddCategoriesComponent } from './add-categories/add-categories.component';
import { ViewCategoriesComponent } from './view-categories/view-categories.component';
import { EditCategoriesComponent } from './edit-categories/edit-categories.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
***REDACTED***
