import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { CategoriesComponent} from './categories.component'
import { AddCategoriesComponent } from './add-categories/add-categories.component';
import { ViewCategoriesComponent} from './view-categories/view-categories.component';
import { EditCategoriesComponent} from './edit-categories/edit-categories.component';
import {CategoriesByLocationComponent} from './categories-by-location/categories-by-location.component'

const routes: Routes = [
    {
***REDACTED***
