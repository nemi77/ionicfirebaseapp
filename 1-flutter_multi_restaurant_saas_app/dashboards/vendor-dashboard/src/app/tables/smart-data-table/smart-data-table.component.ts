import { Component, ViewEncapsulation } from '@angular/core';
import * as tableData from '../../shared/data/smart-data-table';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-smart-data-table',
    templateUrl: './smart-data-table.component.html',
    styleUrls: ['./smart-data-table.component.scss'],
    encapsulation: ViewEncapsulation.None
})
***REDACTED***
