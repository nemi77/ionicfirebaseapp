import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesComponent } from "./sales.component";
const routes: Routes = [
  {
    path: 'sales/:id',
    component: SalesComponent,
    data: {
      title: 'settings'
    }
***REDACTED***
