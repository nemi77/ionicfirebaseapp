import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { SalesService } from './sales.service';
import { ToastrService } from '../components/extra/toastr/toastr.service';
@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
  providers: [SalesService, ToastrService]
***REDACTED***
