import { Injectable }         from '@angular/core';
import { STEPS }              from './workflow.model';

@Injectable()
export class WorkflowService {
    private workflow = [
        { step: STEPS.personal, valid: false },
        { step: STEPS.work, valid: false },
        { step: STEPS.address, valid: false },
        { step: STEPS.result, valid: false }
***REDACTED***
