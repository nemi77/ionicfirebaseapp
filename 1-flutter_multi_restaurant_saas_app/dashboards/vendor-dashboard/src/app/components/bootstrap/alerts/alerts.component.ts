import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { debounceTime} from 'rxjs/operators';

//Interface
export interface IAlert {
    id: number;
    type: string;
    message: string;
}
***REDACTED***
