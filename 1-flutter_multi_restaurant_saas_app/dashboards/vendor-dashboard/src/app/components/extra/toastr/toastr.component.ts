import { Component } from '@angular/core';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ToastrService } from './toastr.service'
// let style = require("../../../../../src/assets/css/ng2-toastr.min.css");
@Component({
    selector: 'app-toastr',
    templateUrl: './toastr.component.html',
    styleUrls: ['./toastr.component.scss'],
    providers: [ToastrService]
})
***REDACTED***
