import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Injectable } from '@angular/core';

@Injectable()
export class ToastrService {
    constructor(public toastr: ToastsManager) { }

    // Success Type
    typeSuccess() {
        this.toastr.success('You are awesome!', 'Success!');
***REDACTED***
