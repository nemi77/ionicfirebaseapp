import * as Quill from 'quill'

//interface starts
export interface Config {
  container: string
  unit: 'word' | 'char'
}

export interface QuillInstance {
  on: Function
***REDACTED***
