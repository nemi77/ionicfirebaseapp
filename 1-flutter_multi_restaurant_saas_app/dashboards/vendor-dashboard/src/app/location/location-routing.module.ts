import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationComponent } from './location.component';
import { EditLocationComponent } from './edit-location/edit-location.component';
import { ViewLocationComponent } from './view-location/view-location.component';
import { AddLocationComponent } from './add-location/add-location.component';
import { ProductByLocationComponent } from 'app/products/product-by-location/product-by-location/product-by-location.component';

const routes: Routes = [
***REDACTED***
