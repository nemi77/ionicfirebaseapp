'use strict';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { ConstantService } from './constant.service';
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class SocketShareService {
  socket: any;
  private orderNotification = new BehaviorSubject(null);        // creates a subject that emits when an order is placed
***REDACTED***
