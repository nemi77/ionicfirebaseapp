import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class CustomerService {
    constructor(private crud: CrudService) { }
    // to get all customer list of restaurant
    public getAllCustomerByRestaurant(restaurantId: any) {
        return this.crud.getOne('orders/customer/restaurant', restaurantId);
    }

***REDACTED***
