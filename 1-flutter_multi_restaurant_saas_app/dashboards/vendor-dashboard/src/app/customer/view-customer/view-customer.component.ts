import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.scss'],
  providers: [CustomerService, ToastrService]
***REDACTED***
