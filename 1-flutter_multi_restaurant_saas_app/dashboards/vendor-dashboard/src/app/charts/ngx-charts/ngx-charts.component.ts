import { Component } from '@angular/core';
import { barChartSingle, barChartmulti, pieChartSingle, pieChartmulti, lineChartSingle, lineChartMulti, areaChartSingle, areaChartMulti } from '../../shared/data/ngxChart';
import * as chartsData from '../../shared/configs/ngx-charts.config';

@Component({
    selector: 'app-ngx',
    templateUrl: './ngx-charts.component.html',
    styleUrls: ['./ngx-charts.component.scss']
})

***REDACTED***
