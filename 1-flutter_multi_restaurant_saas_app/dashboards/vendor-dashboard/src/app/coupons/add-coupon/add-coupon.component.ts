import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CouponsService } from '../coupons.service';
import { ToastrService } from '../../components/extra/toastr/toastr.service';
@Component({
  selector: 'app-add-coupon',
  templateUrl: './add-coupon.component.html',
  styleUrls: ['./add-coupon.component.scss'],
  providers: [CouponsService, ToastrService]
***REDACTED***
