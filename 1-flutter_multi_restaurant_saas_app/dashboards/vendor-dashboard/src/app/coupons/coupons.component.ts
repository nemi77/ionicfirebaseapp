import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CouponsService } from './coupons.service';
import { ToastrService } from '../components/extra/toastr/toastr.service';

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss'],
  providers: [CouponsService, ToastrService]
***REDACTED***
