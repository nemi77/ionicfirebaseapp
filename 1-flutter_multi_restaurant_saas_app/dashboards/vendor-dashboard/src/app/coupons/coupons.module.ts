import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CouponsRoutingModule } from './coupons-routing.module';
import { CouponsComponent } from './coupons.component';
import { AddCouponComponent } from './add-coupon/add-coupon.component';
import { UpdateCouponComponent } from './update-coupon/update-coupon.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { LaddaModule } from 'angular2-ladda';

***REDACTED***
