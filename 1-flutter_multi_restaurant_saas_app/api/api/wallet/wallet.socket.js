/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import WalletEvents from './wallet.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
