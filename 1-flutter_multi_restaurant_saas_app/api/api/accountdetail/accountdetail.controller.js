/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/accountdetails              ->  index
 * POST    /api/accountdetails              ->  create
 * GET     /api/accountdetails/:id          ->  show
 * PUT     /api/accountdetails/:id          ->  upsert
 * PATCH   /api/accountdetails/:id          ->  patch
 * DELETE  /api/accountdetails/:id          ->  destroy
 */

***REDACTED***
