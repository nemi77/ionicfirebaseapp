'use strict';

var express = require('express');
var controller = require('./accountdetail.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

//get all accoountdetails of a single user
router.get('/', auth.isAuthenticated(),controller.index);

***REDACTED***
