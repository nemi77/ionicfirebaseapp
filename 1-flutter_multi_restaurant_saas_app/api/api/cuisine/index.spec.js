'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var cuisineCtrlStub = {
  index: 'cuisineCtrl.index',
  show: 'cuisineCtrl.show',
  create: 'cuisineCtrl.create',
***REDACTED***
