/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CuisineEvents from './cuisine.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
