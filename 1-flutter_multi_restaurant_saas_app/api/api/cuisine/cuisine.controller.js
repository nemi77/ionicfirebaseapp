/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/cuisines              ->  index
 * POST    /api/cuisines              ->  create
 * GET     /api/cuisines/:id          ->  show
 * PUT     /api/cuisines/:id          ->  upsert
 * PATCH   /api/cuisines/:id          ->  patch
 * DELETE  /api/cuisines/:id          ->  destroy
 */

***REDACTED***
