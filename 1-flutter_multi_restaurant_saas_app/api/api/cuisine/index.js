'use strict';

var express = require('express');
var controller = require('./cuisine.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

router.post('/', auth.hasRole('Owner'), controller.create);
router.get('/', controller.index);
router.get('/:id', controller.show);
***REDACTED***
