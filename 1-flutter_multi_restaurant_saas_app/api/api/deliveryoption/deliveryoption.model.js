import mongoose from 'mongoose';
import {registerEvents} from './deliveryoption.events';

var DeliveryoptionSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

registerEvents(DeliveryoptionSchema);
***REDACTED***
