/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var deliveryoptionCtrlStub = {
  index: 'deliveryoptionCtrl.index',
  show: 'deliveryoptionCtrl.show',
  create: 'deliveryoptionCtrl.create',
  upsert: 'deliveryoptionCtrl.upsert',
  patch: 'deliveryoptionCtrl.patch',
***REDACTED***
