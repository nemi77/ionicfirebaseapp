/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/deliveryoptions              ->  index
 * POST    /api/deliveryoptions              ->  create
 * GET     /api/deliveryoptions/:id          ->  show
 * PUT     /api/deliveryoptions/:id          ->  upsert
 * PATCH   /api/deliveryoptions/:id          ->  patch
 * DELETE  /api/deliveryoptions/:id          ->  destroy
 */

***REDACTED***
