var express = require('express');
var controller = require('./deliveryoption.controller');

var router = express.Router();
var auth 	   = require('../../auth/auth.service');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
***REDACTED***
