/**
 * Deliveryoption model events
 */

import {EventEmitter} from 'events';
var DeliveryoptionEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
DeliveryoptionEvents.setMaxListeners(0);

***REDACTED***
