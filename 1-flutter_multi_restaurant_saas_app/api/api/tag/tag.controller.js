/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/tags    tax          ->  index
 * POST    /api/tags              ->  create
 * GET     /api/tags/:id          ->  show
 * PUT     /api/tags/:id          ->  upsert
 * PATCH   /api/tags/:id          ->  patch
 * DELETE  /api/tags/:id          ->  destroy
 */

***REDACTED***
