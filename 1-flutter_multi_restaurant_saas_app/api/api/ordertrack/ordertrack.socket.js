/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import OrdertrackEvents from './ordertrack.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
