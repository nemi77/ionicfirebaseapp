'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './ordertrack.events';
import {Schema} from 'mongoose';
var OrdertrackSchema = new mongoose.Schema({
  deliveryBy:{
    type: Schema.ObjectId,
    ref: 'User'
  },
***REDACTED***
