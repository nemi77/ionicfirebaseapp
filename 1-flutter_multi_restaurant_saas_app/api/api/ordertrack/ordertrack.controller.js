/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/ordertracks              ->  index
 * POST    /api/ordertracks              ->  create
 * GET     /api/ordertracks/:id          ->  show
 * PUT     /api/ordertracks/:id          ->  upsert
 * PATCH   /api/ordertracks/:id          ->  patch
 * DELETE  /api/ordertracks/:id          ->  destroy
 */

***REDACTED***
