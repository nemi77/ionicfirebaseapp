/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/cuisineLocations              ->  index
 * POST    /api/cuisineLocations              ->  create
 * GET     /api/cuisineLocations/:id          ->  show
 * PUT     /api/cuisineLocations/:id          ->  upsert
 * PATCH   /api/cuisineLocations/:id          ->  patch
 * DELETE  /api/cuisineLocations/:id          ->  destroy
 */

***REDACTED***
