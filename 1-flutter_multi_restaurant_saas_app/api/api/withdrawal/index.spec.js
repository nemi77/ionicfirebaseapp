/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var withdrawalCtrlStub = {
  index: 'withdrawalCtrl.index',
  show: 'withdrawalCtrl.show',
  create: 'withdrawalCtrl.create',
  upsert: 'withdrawalCtrl.upsert',
  patch: 'withdrawalCtrl.patch',
***REDACTED***
