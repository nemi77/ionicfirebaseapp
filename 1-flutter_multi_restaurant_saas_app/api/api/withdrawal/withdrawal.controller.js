/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/withdrawals              ->  index
 * POST    /api/withdrawals              ->  create
 * GET     /api/withdrawals/:id          ->  show
 * PUT     /api/withdrawals/:id          ->  upsert
 * PATCH   /api/withdrawals/:id          ->  patch
 * DELETE  /api/withdrawals/:id          ->  destroy
 */

***REDACTED***
