/**
 * Withdrawal model events
 */

import {EventEmitter} from 'events';
var WithdrawalEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
WithdrawalEvents.setMaxListeners(0);

***REDACTED***
