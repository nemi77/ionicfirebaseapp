/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import SettingEvents from './setting.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
