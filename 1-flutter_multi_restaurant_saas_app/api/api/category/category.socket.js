/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CategoryEvents from './category.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
