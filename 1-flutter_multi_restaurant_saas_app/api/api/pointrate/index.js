'use strict';

var express = require('express');
var controller = require('./pointrate.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

//get a list of Pointrate
router.get('/restaurant/:id', auth.isAuthenticated(),controller.index);

***REDACTED***
