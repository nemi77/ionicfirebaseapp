/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pointrates              ->  index
 * POST    /api/pointrates              ->  create
 * GET     /api/pointrates/:id          ->  show
 * PUT     /api/pointrates/:id          ->  upsert
 * PATCH   /api/pointrates/:id          ->  patch
 * DELETE  /api/pointrates/:id          ->  destroy
 */

***REDACTED***
