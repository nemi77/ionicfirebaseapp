/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import NotificationEvents from './notification.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
