'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var notificationCtrlStub = {
  index: 'notificationCtrl.index',
  show: 'notificationCtrl.show',
  create: 'notificationCtrl.create',
***REDACTED***
