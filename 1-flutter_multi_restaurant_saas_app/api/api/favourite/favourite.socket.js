/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import FavouriteEvents from './favourite.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
