'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var favouriteCtrlStub = {
  index: 'favouriteCtrl.index',
  show: 'favouriteCtrl.show',
  create: 'favouriteCtrl.create',
***REDACTED***
