'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './favourite.events';
import {Schema} from 'mongoose';

var FavouriteSchema = new mongoose.Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
***REDACTED***
