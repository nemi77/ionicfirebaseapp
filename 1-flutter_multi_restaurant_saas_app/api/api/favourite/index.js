'use strict';

var express = require('express');
var controller = require('./favourite.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

//get a list of favourite products of a user
router.get('/', auth.isAuthenticated(), controller.index);
***REDACTED***
