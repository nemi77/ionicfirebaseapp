/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/productratings              ->  index
 * POST    /api/productratings              ->  create
 * GET     /api/productratings/:id          ->  show
 * PUT     /api/productratings/:id          ->  upsert
 * PATCH   /api/productratings/:id          ->  patch
 * DELETE  /api/productratings/:id          ->  destroy
 */

***REDACTED***
