'use strict';

var express = require('express');
var controller = require('./productrating.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();
router.get('/', controller.index);

// Gets a single Productrating from the DB
router.get('/:id', controller.show);
***REDACTED***
