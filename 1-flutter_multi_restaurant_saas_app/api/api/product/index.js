'use strict';

var express = require('express');
var controller = require('./product.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Get a list of products by restaurant id
router.get('/restaurant', auth.isAuthenticated(),controller.index);

***REDACTED***
