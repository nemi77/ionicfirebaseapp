/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import DeliveryareaEvents from './deliveryarea.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
