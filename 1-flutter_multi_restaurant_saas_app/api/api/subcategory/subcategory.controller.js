/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subcategories              ->  index
 * POST    /api/subcategories              ->  create
 * GET     /api/subcategories/:id          ->  show
 * PUT     /api/subcategories/:id          ->  upsert
 * PATCH   /api/subcategories/:id          ->  patch
 * DELETE  /api/subcategories/:id          ->  destroy
 */

***REDACTED***
