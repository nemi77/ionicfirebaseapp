'use strict';

var express = require('express');
var controller = require('./subcategory.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Subcategories by restaurant
router.get('/',  auth.isAuthenticated(),controller.index);

***REDACTED***
