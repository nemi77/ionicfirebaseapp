'use strict'; 

var express = require('express');
var controller = require('./adminsetting.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

router.get('/', controller.show);
router.post('/', auth.hasRole('Admin'), controller.create);
router.put('/:id', auth.hasRole('Admin'), controller.upsert);
***REDACTED***
