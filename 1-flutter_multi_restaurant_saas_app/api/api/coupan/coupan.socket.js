/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CoupanEvents from './coupan.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
