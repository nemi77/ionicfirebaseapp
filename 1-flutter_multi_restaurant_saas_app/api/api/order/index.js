'use strict';    ////customer/restaurant orderAmountAndCount
  
var express = require('express');
var controller = require('./order.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Get all Order of a User
router.get('/user/:id', auth.isAuthenticated(),controller.index);

***REDACTED***
