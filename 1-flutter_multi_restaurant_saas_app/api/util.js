var dconfig = require('./developmentconfig/config');
var cloudinary = require('cloudinary');
cloudinary.config({
  cloud_name: dconfig.cloudinaryConfig.cloud_name,
  api_key: dconfig.cloudinaryConfig.api_key,
  api_secret: dconfig.cloudinaryConfig.api_secret
})
let DAYCODE = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
module.exports = {
  test(path) {
***REDACTED***
