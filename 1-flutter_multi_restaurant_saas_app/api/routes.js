/**
 * Main application routes
 */

import errors from './components/errors';
import path from 'path';
export default function(app) {
  app.use('/api/withdrawals', require('./api/withdrawal'));
  app.use('/api/deliveryoptions', require('./api/deliveryoption'));
  app.use('/api/cuisineLocations', require('./api/cuisineLocation'));
***REDACTED***
