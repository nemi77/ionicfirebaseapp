import 'package:flutter/material.dart';

// const PRIMARY = const Color(0xFFFC830F);
const PRIMARY = const Color(0xFFB6236C); //primary color for eat out pal
final PRIMARY_LIGHT = const Color(0xFFFCA029);
final SECONDARY = const Color(0xFFEE3324);
final BORDER = const Color(0xF707070);
final BG_COLOR = const Color(0xFFF5F5F5);

final SUCCESS = const Color(0xFF00C000);
***REDACTED***
