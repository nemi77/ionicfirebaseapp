import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  final String message;
  final IconData icon;

  NoData({Key key, this.message, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
***REDACTED***
