import 'package:http/http.dart' show Client;
import 'constant.dart';
import 'dart:convert';

class MainService {
  static final Client client = Client();

  // default get all - users/list/restaurant
  static Future<dynamic> getTopRatedRestaurants({String count}) async {
    if (count == null) count = 'All';
***REDACTED***
