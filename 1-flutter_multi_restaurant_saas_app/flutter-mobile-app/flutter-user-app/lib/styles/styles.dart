import 'package:flutter/material.dart';

const PRIMARY = const Color(0xFFB6236C);
final primaryLight = const Color(0xFFD489AC);
final secondary = const Color(0xFF2AA1BF);
final border = const Color(0xF707070);
final bgcolor = const Color(0xFF8FD3F4);

final green = const Color(0xFF5ECB56);
final red = const Color(0xFFff5757);
***REDACTED***
