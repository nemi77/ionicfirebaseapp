import 'package:flutter/material.dart';
import '../../styles/styles.dart';
import 'order-upcoming.dart';
import 'order-history.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:RestaurantSaas/initialize_i18n.dart' show initializeI18n;
import 'package:RestaurantSaas/constant.dart' show languages;
import 'package:RestaurantSaas/localizations.dart'
    show MyLocalizations, MyLocalizationsDelegate;
***REDACTED***
