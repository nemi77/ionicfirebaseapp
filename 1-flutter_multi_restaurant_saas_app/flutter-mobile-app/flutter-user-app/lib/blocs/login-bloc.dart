import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'validators.dart';

class LoginBloc extends Object with Validators {
  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<String> get password => _password.stream.transform(validatePassword);
***REDACTED***
