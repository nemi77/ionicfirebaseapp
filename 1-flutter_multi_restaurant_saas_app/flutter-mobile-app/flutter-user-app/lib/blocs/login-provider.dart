import 'package:flutter/material.dart';
import 'login-bloc.dart';

class LoginStateProvider extends InheritedWidget {
  final LoginBloc loginBloc = LoginBloc();

  LoginStateProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_) => true;
***REDACTED***
