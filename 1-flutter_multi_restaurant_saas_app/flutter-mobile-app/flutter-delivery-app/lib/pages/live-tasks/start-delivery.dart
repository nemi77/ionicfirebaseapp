import 'package:call_number/call_number.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/styles/styles.dart';
import 'package:delivery_app/pages/live-tasks/order-delivered.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class StartDelivery extends StatefulWidget {
  final orderDetail;
  StartDelivery({Key key, this.orderDetail}) : super(key: key);
***REDACTED***
