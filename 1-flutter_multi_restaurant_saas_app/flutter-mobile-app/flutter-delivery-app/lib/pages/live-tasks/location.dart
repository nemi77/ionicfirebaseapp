import 'package:flutter/material.dart';
import 'package:delivery_app/styles/styles.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:delivery_app/pages/live-tasks/order-placed.dart';

class LocationDetail extends StatefulWidget {
  static String tag = "location-page";
  final orderDetail;
  final deliveryBoyLatLong;
  LocationDetail({Key key, this.orderDetail, this.deliveryBoyLatLong})
***REDACTED***
