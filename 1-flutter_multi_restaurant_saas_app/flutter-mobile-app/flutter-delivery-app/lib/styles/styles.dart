import 'package:flutter/material.dart';

final primary = const Color(0xFFB6236C);
final blackb = const Color(0xFF565656);
final dullblack = const Color(0xFF565656).withOpacity(0.36);
final dblack = const Color(0xFF565656).withOpacity(0.44);
final blackc = const Color(0xFF565656).withOpacity(0.59);
final boldblack = const Color(0xFF565656).withOpacity(0.84);
final blacka = const Color(0xFF565656).withOpacity(0.91);
final lightblack = const Color(0xFF565656).withOpacity(0.69);
***REDACTED***
