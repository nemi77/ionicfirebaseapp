import 'package:flutter/material.dart';
import 'package:ecommerce/styles/styles.dart';
import 'package:ecommerce/screens/auth/login.dart';

class ForgotPassword extends StatefulWidget {
  static String tag = 'forgot-password';
  ForgotPassword({Key key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => new _ForgotPasswordState();
***REDACTED***
