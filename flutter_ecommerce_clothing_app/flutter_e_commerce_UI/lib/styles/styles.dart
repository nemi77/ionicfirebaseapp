import 'package:flutter/material.dart';

final primary = const Color(0xFFfd3863);
final orange = const Color(0xFFfe966c);
final lighttext = const Color(0xFF8a8a8a);
final darktext = const Color(0xFF484848);
final blacktext = const Color(0xFF4e4c4c);
final texta = const Color(0xFFb3484848);
final textb = const Color(0xFF2f2f2f);

***REDACTED***
