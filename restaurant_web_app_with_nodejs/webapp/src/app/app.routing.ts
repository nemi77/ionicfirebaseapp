
import {Routes, RouterModule} from'@angular/router';
import { LoginComponent} from './pages/login/login.component';
import { SignupComponent} from './pages/signup/signup.component';
import { HomeComponent } from './pages/home/home.component';
import { MenuListComponent} from './pages/menu-list/menu-list.component';
import { AllProductComponent} from './pages/all-product/all-product.component';
import { SingleProductComponent} from './pages/single-product/single-product.component';
import { CartComponent } from './pages/cart/cart.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
***REDACTED***
