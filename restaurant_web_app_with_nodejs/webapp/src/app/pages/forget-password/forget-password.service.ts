import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from '../../services/constant.service';
import { CrudService } from 'src/app/services/crud.service';

@Injectable()
export class ForgetPasswordService {
    constructor(private http: HttpClient, private constantService: ConstantService, public crud: CrudService) {
    }

***REDACTED***
