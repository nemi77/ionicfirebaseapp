import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AllProductsService} from './all-products.service';
import {CrudBaseService} from '../../services/base.service';

@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.component.html',
  styleUrls: ['./all-product.component.scss'],
  providers: [AllProductsService]
***REDACTED***
