import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MenuProductService} from './menu-product.service';
import {CrudBaseService} from '../../services/base.service';

@Component({
  selector: 'app-menu-product',
  templateUrl: './menu-product.component.html',
  styleUrls: ['./menu-product.component.scss'],
  providers: [MenuProductService]
***REDACTED***
