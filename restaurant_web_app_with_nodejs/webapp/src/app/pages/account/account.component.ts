import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
//import {NgForm} from '@angular/forms';
import { AccountService } from './account.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss'],
    providers: [AccountService]
***REDACTED***
