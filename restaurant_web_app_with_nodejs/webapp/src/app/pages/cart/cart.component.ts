import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
import {CartService} from './cart.service';
import {ToastrService} from 'ngx-toastr';
import {CrudBaseService} from '../../services/base.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
***REDACTED***
