import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {SingleProductService} from './single-product.service';
import swal from 'sweetalert2';
import {ToastrService} from 'ngx-toastr';
import {CrudBaseService} from '../../services/base.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
***REDACTED***
