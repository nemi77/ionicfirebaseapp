import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductCategoryPage } from './product-category';
import { CustomHeaderPageModule } from '../custom-header/custom-header.module';
import { FooterPageModule } from '../footer/footer.module';
import { TranslaterModule } from '../../app/translate.module';

@NgModule({
  declarations: [
    ProductCategoryPage,
***REDACTED***
