import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class FilterService {
  colors: any[];
  sizes: any[];

  constructor(private http: HttpClient, public constService: ConstService) { }
***REDACTED***
