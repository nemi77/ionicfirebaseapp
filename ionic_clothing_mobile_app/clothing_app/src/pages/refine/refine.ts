import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController } from "ionic-angular";
import { FilterService } from "./refine.service";

@IonicPage()
@Component({
  selector: "page-refine",
  templateUrl: "refine.html",
  providers: [FilterService]
})
***REDACTED***
