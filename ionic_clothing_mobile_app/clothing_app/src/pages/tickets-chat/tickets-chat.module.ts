import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TicketsChatPage } from "./tickets-chat";
import { MomentModule } from "angular2-moment";
import { TranslaterModule } from '../../app/translate.module';


@NgModule({
  declarations: [TicketsChatPage],
  imports: [IonicPageModule.forChild(TicketsChatPage), MomentModule, TranslaterModule]
***REDACTED***
