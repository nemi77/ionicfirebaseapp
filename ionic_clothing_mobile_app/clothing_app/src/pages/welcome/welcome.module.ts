import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { WelcomePage } from "./welcome";
import { TranslaterModule } from "../../app/translate.module";
@NgModule({
  declarations: [WelcomePage],
  imports: [IonicPageModule.forChild(WelcomePage), TranslaterModule],
  exports: [WelcomePage]
})
export class WelcomePageModule {}
***REDACTED***
