import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class ProductService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getProducts(subCategoryId) {
    const headers = new HttpHeaders();
***REDACTED***
