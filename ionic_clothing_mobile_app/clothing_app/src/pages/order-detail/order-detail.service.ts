import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class OrdersDetailService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getOrdersDetail(orderId: any) {
    let authtoken = localStorage.getItem("token");
***REDACTED***
