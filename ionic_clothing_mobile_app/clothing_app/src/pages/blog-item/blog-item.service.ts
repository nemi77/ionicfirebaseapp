import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class BlogItemService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getBlogById(blogId) {
    let authtoken = localStorage.getItem("token");
***REDACTED***
