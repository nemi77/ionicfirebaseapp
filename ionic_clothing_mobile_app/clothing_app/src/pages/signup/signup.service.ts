import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class SignupService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  createUser(user: any) {
    const body = JSON.stringify(user);
***REDACTED***
