import { Component, ViewChild } from "@angular/core";
import { Nav, Events, Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { SettingService } from "../pages/settings/settings.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  templateUrl: "app.html",
  providers: [SettingService]
***REDACTED***
