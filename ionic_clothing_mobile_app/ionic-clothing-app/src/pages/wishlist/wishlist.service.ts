import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class WishListService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getFavourites(userId) {
    let authtoken = localStorage.getItem("token");
***REDACTED***
