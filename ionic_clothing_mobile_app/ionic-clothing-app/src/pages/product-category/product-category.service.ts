import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class ProductCategoryService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getSubcategories(categoryId) {
    const headers = new HttpHeaders();
***REDACTED***
