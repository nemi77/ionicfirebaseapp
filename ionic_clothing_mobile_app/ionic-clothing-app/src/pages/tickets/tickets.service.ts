import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class TicketsService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getAllTickets() {
    let authtoken = localStorage.getItem("token");
***REDACTED***
