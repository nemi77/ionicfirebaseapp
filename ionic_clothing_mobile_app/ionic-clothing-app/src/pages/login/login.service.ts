import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class LoginService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  login(user: any) {
    const body = JSON.stringify(user);
***REDACTED***
