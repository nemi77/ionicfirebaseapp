import { FooterPageModule } from './../footer/footer.module';
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { BlogItemPage } from "./blog-item";
import { MomentModule } from "angular2-moment";
import { TranslaterModule } from '../../app/translate.module';

@NgModule({
  declarations: [BlogItemPage],
  imports: [IonicPageModule.forChild(BlogItemPage), MomentModule, TranslaterModule, FooterPageModule]
***REDACTED***
