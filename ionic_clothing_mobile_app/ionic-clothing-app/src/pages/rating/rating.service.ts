import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class RatingService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  submitReview(body) {
    let authtoken = localStorage.getItem("token");
***REDACTED***
