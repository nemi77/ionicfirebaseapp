import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstService } from "../../app/constService";

@Injectable()
export class SettingService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getUserInfo() {
    let authtoken = localStorage.getItem("token");
***REDACTED***
