import { FooterPageModule } from './../footer/footer.module';
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TicketFormPage } from "./ticket-form";
import { FileUploadModule } from "ng2-file-upload";
import { Ng2CloudinaryModule } from "ng2-cloudinary";
import { TranslaterModule } from '../../app/translate.module';

@NgModule({
  declarations: [TicketFormPage],
***REDACTED***
