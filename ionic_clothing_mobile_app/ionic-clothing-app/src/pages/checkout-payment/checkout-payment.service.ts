import { Injectable } from "@angular/core";
import { ConstService } from "../../app/constService";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable()
export class CheckoutPaymentService {
  constructor(private http: HttpClient, public constService: ConstService) { }

  getSavedCards() {
    let authtoken = localStorage.getItem("token");
***REDACTED***
