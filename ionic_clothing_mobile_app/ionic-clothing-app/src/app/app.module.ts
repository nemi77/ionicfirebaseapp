import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { MyApp } from "./app.component";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ConstService } from "./constService";
import { Ionic2RatingModule } from "ionic2-rating";
import { HttpClientModule, HttpClient } from "@angular/common/http";

***REDACTED***
