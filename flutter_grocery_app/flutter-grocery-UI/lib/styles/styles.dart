import 'package:flutter/material.dart';

final primary = const Color(0xFF6EDBE7);
final green = const Color(0xFF27C9A3);
final orange = const Color(0xFFFFA200);
final whiteText = const Color(0xFFF2F2F2);
final blacktext = const Color(0xFF191919);
final darkgrey = const Color(0xFF7E8EAA);
final border = const Color(0xFF7D8DBE9);
final colorblack = const Color(0xFF394047);
***REDACTED***
