import 'package:flutter/material.dart';
import 'package:online_grocery/styles/styles.dart';


class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: colorblack,
          title: Text(
            'Contact US',
            style: hintStyleboldwhitePSB(),
          ),
          // actions: <Widget>[
          //   Image.asset('lib/assets/icons/search.png'),
          // ],
        ),
        body: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Write To Us',
                    //  style: textregularsm(),
                  ),
                  Container(
                    width: 25.0,
                    margin: EdgeInsets.only(bottom: 20.0),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: primary, width: 3.0),
                      ),
                    ),
                  ),
                  inputfield('Your Name'),
                  inputfield('Your Email'),
                  inputfield('Your Mobile'),
                  inputfield('Your message')
                ],
              )),
        ),
        bottomNavigationBar: Padding(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    'CANCEL',
                    textAlign: TextAlign.start,
                    //  style: textregularredsmcondensedblack(),
                  ),
                )),
                Expanded(
                    child: Text(
                  'SEND',
                  textAlign: TextAlign.end,
                  //  style: textregularredsmcondensedred(),
                ))
              ],
            )));
  }

  Widget inputfield(title) {
    return Container(
      margin: EdgeInsets.only(top: 0.0, bottom: 20.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.black12),
        ),
      ),
      child: TextFormField(
        decoration: new InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 12.0),
          border: InputBorder.none,
          hintText: title,
          //  hintStyle: textregularsmall(),
        ),
        keyboardType: TextInputType.text,
        //  style: textregularsmall(),
      ),
    );
  }
}
