import 'package:flutter/material.dart';
import 'package:online_grocery/screens/pages/cart/cart.dart';
import 'package:online_grocery/screens/pages/product/product-details.dart';
import 'package:online_grocery/styles/styles.dart';


class NewStocks extends StatefulWidget {
  @override
  _NewStocksState createState() => _NewStocksState();
}

class _NewStocksState extends State<NewStocks> {
  int discount = 10;
  int price = 20;
  bool favSelected = true;
  var _currencies = ['1kg' , '2kg', '3kg'];
  var currentitem = '1kg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: colorblack,
        title: Text('New Stock', style: hintStyleboldwhitePSB(),),
        actions: <Widget>[
          Image.asset('lib/assets/icons/search.png'),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Filter(),
          newProductDetails(),
          newProductDetails(),
          newProductDetails(),
          newProductDetails(),
          newProductDetails()

        ],
      ),


    );
  }

  Widget Filter(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      height: 49.0,
      color: Color(0xFFEDEEEE),
      child: Row(
        children: <Widget>[
          Flexible(fit:FlexFit.tight, flex: 6,child: Text('1029 Items', style: hintStyleblacktextPM(),)),
          Flexible(fit:FlexFit.tight, flex: 2,child:
          Container(
//            width: 76.33,
            height: 31.0,
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xFF707070).withOpacity(0.13),),
            color: Colors.white,
          ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('lib/assets/icons/filter.png' , color: Color(0xFF0C0D0E).withOpacity(0.43),),
                Padding(padding: EdgeInsets.only(left: 5.0)),
                Text('Filter', style: hintStyleblacktextPM(),)
              ],
            ),
          ))
        ],
      ),
    );
  }

  Widget newProductDetails(){
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => ProductDetails(),
          ),
        );
      },
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top:5.0, bottom: 8.0),
            decoration: BoxDecoration(
                border: Border.all(color: border, width: 0.5)
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Flexible(flex:4, fit:FlexFit.tight,child:
                   Container(
                     margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 15.0),
                     width: 107.0,
                     height: 100.0,
                     decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage('lib/assets/images/product.png',), fit: BoxFit.fill)
                     ),
                   )
                    ),
                    Flexible(flex:5, fit:FlexFit.tight,child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top:10.0), child:  Row(
                          children: <Widget>[
                         Flexible(flex:1, fit:FlexFit.tight,child: Image.asset('lib/assets/icons/veg.png', height: 12.0,),),
                            Padding(padding: EdgeInsets.only(left: 5.0)),
                            Flexible(flex:9, fit:FlexFit.tight,child:  Text('Fresho', style: hintStylesblacktextPM(),),),
                          ],
                        )),
                        Text('Lorem ipsum dolor.', style: hintStylebtextPR(),),
                        Padding(padding: EdgeInsets.only(top:5.0), child:
                        Container(
                          height: 28.0,
                          width: 129.0,
                          margin: EdgeInsets.only(bottom: 5.0),
                          padding: EdgeInsets.only(left: 40.0, right: 5.0),
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xFF707070).withOpacity(0.26))
                          ),
                          child:  new DropdownButtonHideUnderline(
                            child: new DropdownButton<String>
                              (items: _currencies.map((String dropDownStringItem){
                              return DropdownMenuItem<String>(
                                value:dropDownStringItem,
                                child: Text(dropDownStringItem, textAlign: TextAlign.center, style: hintStyleblackPR(),),
                              );
                            }).toList(),
                              onChanged: (String newvalue){
                                setState(() {
                                  this.currentitem = newvalue;
                                });
                              },
                              value: currentitem,
                              style: hintStyleblackPR(),
                              icon: Icon(Icons.keyboard_arrow_down, size: 18.0,),
//                              isDense: true,
//                            isExpanded: true,

                            ) ,),
                        ),
                        ),
                        Row(
                          children: <Widget>[
                          
                            Flexible( flex:5, fit: FlexFit.tight,child: Text('Rs 67', style: hintStylesmallrPR(),)),
                            Flexible( flex:5, fit: FlexFit.tight,child: Text('Rs 60', style: hintStylebtextPR(), textAlign: TextAlign.center,)),
                          ],
                        )
                      ],
                    )
                    ),
                    Flexible(flex:3, fit:FlexFit.tight,child:
                    Padding(padding: EdgeInsets.only(bottom: 0.0), child:
                    Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              favSelected = !favSelected;
                            });
                          },
                          child: favSelected ? Image.asset('lib/assets/icons/fav.png') : Image.asset('lib/assets/icons/fav.png', color: Color(0xFFFF3737),),
                        ),

                        Container(
                          height: 30.0,
                          width: 65.0,
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top:45.0),
                          decoration: BoxDecoration(
                              color: green,
                              borderRadius: BorderRadius.circular(17.0)
                          ),
                          child: RawMaterialButton(onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => CartPage(),
                              ),
                            );
                          }, child: Text('Add', style: hintStylesmallwhitePR(),),),
                        )
                      ],
                    )
                    )),


                  ],
                ),
              ],
            ),
          ),



          Positioned(
            top:10.0,
              left: 10.0,
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(top:3.0, bottom: 3.0),
                width: 62.0,
                height: 25.0,
                decoration: BoxDecoration(
                  color:Color(0xFFF9571C),
                  borderRadius: new BorderRadius.only(
                    bottomRight: const Radius.circular(30.0),
                    topRight: const Radius.circular(30.0),
                  ),
                ),
                child: Text('20% Off', textAlign: TextAlign.center, style: hintStylesmallerwhitePR(),),
              ))
        ],
      )
    );
  }
}
