import 'package:flutter/material.dart';
import 'package:online_grocery/screens/pages/address/addAddress.dart';
import 'package:online_grocery/screens/pages/payment/payment.dart';
import 'package:online_grocery/styles/styles.dart';


class CheckAvailability extends StatefulWidget {
  @override
  _CheckAvailabilityState createState() => _CheckAvailabilityState();
}

class _CheckAvailabilityState extends State<CheckAvailability> {
  int discount = 10;
  int price = 120;
  int saved = 45;
  int value;
  bool _value= false;
  int groupValue, groupValue1;
  bool addAddress = false;
  var _currencies = ['1kg' , '2kg', '3kg'];
  var currentitem = '1kg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: colorblack,
          title: Text('Checking Availability', style: hintStyleboldwhitePSB(),),
        ),
        body: ListView(
          children: <Widget>[
            newProductDetails(),
           
           Padding(padding: EdgeInsets.only(left: 15.0, right: 15.0), child:
             Row(
               children: <Widget>[
                 Flexible(flex:5, fit: FlexFit.tight,
                     child:
                     Container(
                       height: 47.0,
                       alignment: Alignment.center,
                       margin: EdgeInsets.only(right: 3.0, top: 10.0),
                       decoration: BoxDecoration(
                         border:Border.all(color: Color(0xFF707070).withOpacity(0.20)),

                       ),
                   child: RawMaterialButton(onPressed: (){
                     setState(() {
                       addAddress = !addAddress;
                     });
                   }, child:
                   Text('Add Delivery Address', style: hintStyleblackPR(),),)
                 )),
                 Flexible(flex:4, fit: FlexFit.tight,
                     child:
                     Container(
                       height: 47.0,
                         margin: EdgeInsets.only(left: 3.0, top: 10.0),
                       alignment: Alignment.center,
                       decoration: BoxDecoration(
                         border:Border.all(color: Color(0xFF707070).withOpacity(0.20)),

                       ),
                       child:Row(
                         mainAxisAlignment: MainAxisAlignment.spaceAround,
                         children: <Widget>[
                           Text('Locate Me', style: hintStyleblackPR(),),
                           Image.asset('lib/assets/icons/location.png')

                         ],
                       )
                     ))
               ],
             )),
           addAddress ? Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Radio(value: 0,
                     groupValue: groupValue,
                     onChanged: (int value) {
                       setState(
                             () {
                           groupValue = value;
                           _value = !_value;
                         },
                       );
                     },
                     activeColor: Color(0xFFF9571C),
                   ),
                   Flexible( flex:10, fit: FlexFit.tight,child:
                   Container(
                     margin: EdgeInsets.only(right: 15.0, top: 15.0),
                     padding: EdgeInsets.all(5.0),
                     decoration: BoxDecoration(
                         border: Border.all(color: Color(0xFF707070).withOpacity(0.20))
                     ),
                     child: Text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
                       style: hintStyleBlackPR(),),
                   )
                   ),
                 ],
               ),

               Padding(padding: EdgeInsets.only(left: 50.0, top: 5.0), child:
              GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => AddAddress(),
                    ),
                  );
                },
                child:  Text('Add Delivery Address', style: hintStyleredPR(),),
              ))
             ],
           ): Container()

          ],
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.only(top:30.0, right: 10.0, left:10.0),
          height: 159.0,
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.only(
                topLeft:  const  Radius.circular(40.0),
                topRight: const  Radius.circular(40.0)),

//          borderRadius: BorderRadius.circular(40.0),
            border: Border.all(color: Color(0xFFD8DBE9)),
          ),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Grand Total', style: hintStyleblackatextPSB(),),
                      Text('Saved \$4.00', style: hintStyleblackPR(),)
                    ],
                  )),
                  Expanded(child: Padding(padding: EdgeInsets.only(bottom: 25.0), child: Text('\$21.80', style: hintStyleblackatextPSB(), textAlign: TextAlign.end,),))
                ],
              ),

              Container(
                  margin: EdgeInsets.only(top:20.0, ),
                  width: 265.0,
                  decoration: BoxDecoration(
                      color: green,
                      borderRadius: BorderRadius.circular(23.0)
                  ),
                  child: RawMaterialButton(onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => Payment(),
                      ),
                    );
                  }, child: Text('Confirm', style: hintStylewhitetextPSB(),),)
              ),

            ],
          ),
        )

    );
  }

  Widget newProductDetails(){
    return GestureDetector(
      onTap: (){
//        Navigator.push(
//          context,
//          MaterialPageRoute(
//            builder: (BuildContext context) => ProductDetails(),
//          ),
//        );
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: border, width: 0.5)
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Flexible(flex:4, fit:FlexFit.tight,child: Image.asset('lib/assets/images/productdetail.png',)),
                Flexible(flex:5, fit:FlexFit.tight,child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top:0.0), child:  Text('Suger', style: hintStylesmalltextPSB(),),),
                    Text('Price-\$16.00', style: hintStylesmalltextPSB(),),
                    Padding(padding: EdgeInsets.only(top:10.0), child:  Container(
                      height: 28.0,
                      width: 129.0,
                      margin: EdgeInsets.only(bottom: 5.0),
                      padding: EdgeInsets.only(left: 40.0, right: 5.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFF707070).withOpacity(0.26))
                      ),
                      child:  new DropdownButtonHideUnderline(
                        child: new DropdownButton<String>
                          (items: _currencies.map((String dropDownStringItem){
                          return DropdownMenuItem<String>(
                            value:dropDownStringItem,
                            child: Text(dropDownStringItem, textAlign: TextAlign.center, style: hintStyleblackPR(),),
                          );
                        }).toList(),
                          onChanged: (String newvalue){
                            setState(() {
                              this.currentitem = newvalue;
                            });
                          },
                          value: currentitem,
                          style: hintStyleblackPR(),
                          icon: Icon(Icons.keyboard_arrow_down, size: 18.0,),
//                              isDense: true,
//                            isExpanded: true,

                        ) ,),
                    )),
                  ],
                )
                ),
                Flexible(flex:4, fit:FlexFit.tight,child:
               Column(
                 children: <Widget>[
                   Container(
                     padding: EdgeInsets.all(4.0),
                     margin: EdgeInsets.only(right: 20.0, bottom: 18.0),
                     alignment: Alignment.center,
                     decoration: BoxDecoration(
                         border: Border.all(color: Color(0xFFBBC2C3))
                     ),
                     child: Text('Not Available', style: hintStylesmallbtextPM(),),
                   ),
                   Container(
                     padding: EdgeInsets.all(4.0),
                     margin: EdgeInsets.only(right: 20.0, bottom: 0.0),
                     alignment: Alignment.center,
                     decoration: BoxDecoration(
                         border: Border.all(color: Color(0xFFBBC2C3))
                     ),
                     child: Text('View Similar', style: hintStylesmallbtextPM(),),
                   )
                 ],
               )
                ),


              ],
            )
          ],
        ),
      ),
    );
  }
}
