import 'package:flutter/material.dart';
import 'package:online_grocery/screens/pages/contact%20us/contactUS.dart';
import 'package:online_grocery/styles/styles.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';
// import '../../pages/contact_us/contact_us.dart';

class about_us extends StatefulWidget {
  static String tag = "about_us";

  @override
  _about_usState createState() => _about_usState();
}

final List<String> imgList = [
  // "assets/tech1.jpeg",
  "lib/assets/tech2.jpeg",
  "lib/assets/tech3.jpeg",
  "lib/assets/tech4.jpeg",
];

class _about_usState extends State<about_us> {
  ScrollController _scrollController = new ScrollController();

  _launchURLCall() async {
    const url = "tel:08050032994";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURL() async {
    const url =
        "https://www.google.co.in/maps/place/Pietech+Solution/@12.9168047,77.5878204,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae150bfacfb5e7:0x9f531c067f045d19!8m2!3d12.9167995!4d77.5900091";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    Widget _buildTile(BuildContext context, int index) {
      return new Container(
        child: new CarouselSlider(
          items: imgList.map((img) {
            return new Container(
              child: new Stack(
                children: <Widget>[
                  new Image(
                    image: new AssetImage(img),
                    fit: BoxFit.cover,
                    width: screenWidth,
                  ),
                  new Positioned(
                    top: 130.0,
                    child: new Container(
                      color: Colors.white70,
                      height: 50.0,
                      width: screenWidth,
                      padding: new EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                      alignment: FractionalOffset.centerLeft,
                      child: new Text(
                        "WE PROVIDE BETTER SERVICE. WE BELIEVE IN SATISFACTION OF OUR CLIENT",
                        // style: priceDescriptionText(),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }).toList(),
//          animationCurve: Curves.fastOutSlowIn,
          // autoPlayDuration: Duration(milliseconds: 3000),
          autoPlay: true,
          viewportFraction: 1.0,
          aspectRatio: 2.0,
        ),
      );
    }

    Widget aboutUs = new NestedScrollView(
      controller: _scrollController,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          new SliverList(
            delegate: new SliverChildBuilderDelegate(
              _buildTile,
              childCount: 1,
            ),
          ),
        ];
      },
      body: SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
              child: new Text(
                "About Us",
                // // style: titleStyleLight(),
                textAlign: TextAlign.left,
              ),
            ),
            new Container(
              padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
              child: new Text(
                "Pietech Solution is a global information technology, consulting and outsourcing company was"
                "founded by a highly motivated groups of marketing Team, software Developer. Pietech is not a"
                "typical outsourcing company. Talented, driven and principled people who are passionate about IT"
                "services, came together because they wanted to do something special. Special work for our"
                "clients is the front line, revolutionizing the way the industry works is a gradual effect"
                "improving Industry through services is our long term goal.",
                // // style: hintStyleDark(),
                textAlign: TextAlign.justify,
              ),
            ),
            new Container(
              padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
              child: new Text(
                "Get in Touch",
                // // style: titleStyleLight(),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
              child: Card(
                child: new Column(
                  children: <Widget>[
                    new Divider(),
                    new ListTile(
                      onTap: () {
                        _launchURLCall();
                      },
                      leading: new Icon(Icons.phone),
                      title: new Text(
                        "(+91) 8050032994",
                        // // style: hintStyleDark(),
                      ),
                    ),
                    new Divider(),
                    new ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => ContactUs(),
                          ),
                        );
                      },
                      leading: new Icon(Icons.mail),
                      title: new Text(
                        "Email Us",
                        // // style: hintStyleDark(),
                      ),
                    ),
                    new Divider(),
                    new ListTile(
                      onTap: () {
                        _launchURL();
                      },
                      leading: new Icon(Icons.my_location),
                      title: new Text(
                        "Locate Us",
                        // // style: hintStyleDark(),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );

    return new Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: colorblack,
        title: Text(
          'Help & About Us',
          style: hintStyleboldwhitePSB(),
        ),
      ),
      body: new Center(child: aboutUs),
    );
  }
}

class CompositeSubscription {
  Set<StreamSubscription> _subscriptions = new Set();

  void cancel() {
    for (var n in this._subscriptions) {
      n.cancel();
    }
    this._subscriptions = new Set();
  }

  void add(StreamSubscription subscription) {
    this._subscriptions.add(subscription);
  }

  void addAll(Iterable<StreamSubscription> subs) {
    _subscriptions.addAll(subs);
  }

  bool remove(StreamSubscription subscription) {
    return this._subscriptions.remove(subscription);
  }

  bool contains(StreamSubscription subscription) {
    return this._subscriptions.contains(subscription);
  }

  List<StreamSubscription> toList() {
    return this._subscriptions.toList();
  }
}
