import 'package:flutter/material.dart';
import 'package:online_grocery/styles/styles.dart';


class DeliveryLocation extends StatefulWidget {
  @override
  _DeliveryLocationState createState() => _DeliveryLocationState();
}

class _DeliveryLocationState extends State<DeliveryLocation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: colorblack,
          iconTheme: IconThemeData(color: Colors.white),
          title: new Text(
            'Add Delivery Address',
            style: hintStyleboldwhitePSB(),
          ),
        ),
        body: new SingleChildScrollView(
            child: Container(
          margin:
              EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 10.0)),
              new Row(
                children: <Widget>[
                  new Text(
                    'First Name',
                    style: hintStyleblackPR(),
                  ),
                  new Text(
                    '*',
                    style: TextStyle(color: red),
                  ),
                ],
              ),
              inputTextbox(context),
              new Row(
                children: <Widget>[
                  new Text(
                    'Last Name',
                    style: hintStyleblackPR(),
                  ),
                  new Text(
                    '*',
                    style: TextStyle(color: red),
                  ),
                ],
              ),
              inputTextbox(context),
              new Row(
                children: <Widget>[
                  new Text(
                    'Email',
                    style: hintStyleblackPR(),
                  ),
                  new Text(
                    '*',
                    style: TextStyle(color: red),
                  ),
                ],
              ),
              inputTextbox(context),
              new Row(
                children: <Widget>[
                  new Text(
                    'Phone Number',
                    style: hintStyleblackPR(),
                  ),
                  new Text(
                    '*',
                    style: TextStyle(color: red),
                  ),
                ],
              ),
              inputTextbox(context),
              new Row(
                children: <Widget>[
                  new Text(
                    'Address',
                    style: hintStyleblackPR(),
                  ),
                  new Text(
                    '*',
                    style: TextStyle(color: red),
                  ),
                ],
              ),
              addressbox(context),
              new Row(
                children: <Widget>[
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'CANCEL',
                      style: hintStyleblackPR(),
                    ),
                  )),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'SAVE',
                      textAlign: TextAlign.end,
                      style: hintStyleredPR(),
                    ),
                  ))
                ],
              )
            ],
          ),
        )));
  }

  static Widget inputTextbox(context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFF707070).withOpacity(0.27)),
      ),
      child: TextFormField(
          decoration: new InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            border: InputBorder.none,
          ),
          keyboardType: TextInputType.text,
          style: hintStyleblackPR()),
    );
  }

  static Widget addressbox(context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFF707070).withOpacity(0.27)),
      ),
      child: TextFormField(
          decoration: new InputDecoration(
            contentPadding: EdgeInsets.all(12.0),
            border: InputBorder.none,
          ),
          keyboardType: TextInputType.multiline,
          maxLines: 4,
          style: hintStyleblackPR()),
    );
  }
}
