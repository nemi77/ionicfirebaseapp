import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {EmailComposer} from '@ionic-native/email-composer';
import {Camera, CameraOptions} from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
  providers: [EmailComposer, Camera]
***REDACTED***
