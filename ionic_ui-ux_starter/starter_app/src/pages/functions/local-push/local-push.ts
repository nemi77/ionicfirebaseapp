import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';


@IonicPage()
@Component({
  selector: 'page-local-push',
  templateUrl: 'local-push.html',
  providers: [LocalNotifications]
***REDACTED***
