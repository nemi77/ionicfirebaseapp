import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-social-sharing',
  templateUrl: 'social-sharing.html',
  providers: [SocialSharing]
})
***REDACTED***
