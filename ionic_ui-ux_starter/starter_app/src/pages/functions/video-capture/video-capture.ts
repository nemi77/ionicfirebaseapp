import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError } from '@ionic-native/media-capture';
import { Camera } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-video-capture',
  templateUrl: 'video-capture.html',
  providers: [MediaCapture, Camera]
***REDACTED***
