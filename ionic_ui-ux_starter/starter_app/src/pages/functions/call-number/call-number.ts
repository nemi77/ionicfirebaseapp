import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {CallNumber} from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-call-number',
  templateUrl: 'call-number.html',
  providers: [CallNumber]
})
***REDACTED***
