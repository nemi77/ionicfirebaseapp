import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';
import { AgmCoreModule } from '@agm/core';
import { StyledMap } from './styled-map.directive';
import { DirectionsMapDirective } from './direction.directive';

@NgModule({
  declarations: [
    MapPage,
***REDACTED***
