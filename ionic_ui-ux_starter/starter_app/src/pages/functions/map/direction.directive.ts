import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
import { Directive, Input } from '@angular/core';
declare let google: any;

@Directive({
  selector: 'agm-map-directions'
})
export class DirectionsMapDirective {
  @Input() origin;
  @Input() destination;
***REDACTED***
