import { Directive } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
@Directive({
  selector: 'styled-map'
})

export class StyledMap {

  constructor(private _wrapper: GoogleMapsAPIWrapper) {
    this._wrapper.getNativeMap().then((m) => {
***REDACTED***
