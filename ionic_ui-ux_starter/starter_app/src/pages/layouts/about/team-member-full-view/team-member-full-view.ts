import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AboutService } from '../about.service';

@IonicPage()
@Component({
  selector: 'page-team-member-full-view',
  templateUrl: 'team-member-full-view.html',
    providers:[AboutService]
})
***REDACTED***
