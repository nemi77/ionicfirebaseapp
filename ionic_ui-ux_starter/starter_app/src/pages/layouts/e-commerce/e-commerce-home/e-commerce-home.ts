import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {EcommerceService} from '../ecommerce.service';

@IonicPage()
@Component({
  selector: 'page-e-commerce-home',
  templateUrl: 'e-commerce-home.html',
  providers:[EcommerceService]
})
***REDACTED***
