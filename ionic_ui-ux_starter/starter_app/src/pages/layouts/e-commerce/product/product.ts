import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {EcommerceService} from '../ecommerce.service';

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
  providers:[EcommerceService]
***REDACTED***
