import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NotificationService} from '../notification.service';

@IonicPage()
@Component({
  selector: 'page-notifications-details',
  templateUrl: 'notifications-details.html',
  providers:[NotificationService]
})
***REDACTED***
