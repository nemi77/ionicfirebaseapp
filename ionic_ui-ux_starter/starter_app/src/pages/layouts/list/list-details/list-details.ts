import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ListService} from '../list.service';

@IonicPage()
@Component({
  selector: 'page-list-details',
  templateUrl: 'list-details.html',
  providers:[ListService]
})
***REDACTED***
