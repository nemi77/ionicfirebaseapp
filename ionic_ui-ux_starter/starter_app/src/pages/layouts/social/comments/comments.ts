import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SocialService} from '../social.service';


@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
  providers:[SocialService]
***REDACTED***
