import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { EventService } from '../event.service';

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
  providers: [EventService]
})
***REDACTED***
