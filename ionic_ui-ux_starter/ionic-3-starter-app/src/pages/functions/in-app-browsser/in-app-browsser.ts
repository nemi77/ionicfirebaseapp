import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {InAppBrowser} from '@ionic-native/in-app-browser';


@IonicPage()
@Component({
  selector: 'page-in-app-browsser',
  templateUrl: 'in-app-browsser.html',
  providers: [InAppBrowser]
***REDACTED***
