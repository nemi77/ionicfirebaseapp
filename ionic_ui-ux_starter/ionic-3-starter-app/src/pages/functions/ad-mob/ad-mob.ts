import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@IonicPage()
@Component({
  selector: 'page-ad-mob',
  templateUrl: 'ad-mob.html',
  providers: [AdMobFree]
})
***REDACTED***
