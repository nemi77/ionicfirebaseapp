import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PushService } from './push.service';

@IonicPage()
@Component({
  selector: 'page-push',
  templateUrl: 'push.html',
  providers: [PushService]
})
***REDACTED***
