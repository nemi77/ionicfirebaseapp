import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {AppRate} from '@ionic-native/app-rate';

@IonicPage()
@Component({
  selector: 'page-app-rate',
  templateUrl: 'app-rate.html',
  providers: [AppRate]
})
***REDACTED***
