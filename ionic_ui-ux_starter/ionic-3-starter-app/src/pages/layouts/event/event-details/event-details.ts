import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {EventService} from '../event.service';


@IonicPage()
@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html',
  providers:[EventService]
***REDACTED***
