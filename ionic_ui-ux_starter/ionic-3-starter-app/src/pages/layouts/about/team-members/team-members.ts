import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {AboutService} from '../about.service';

@IonicPage()
@Component({
  selector: 'page-team-members',
  templateUrl: 'team-members.html',
  providers:[AboutService]
***REDACTED***
