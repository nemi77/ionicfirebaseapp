import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import {NewsService} from '../news.service';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
  providers:[SocialSharing,NewsService]
***REDACTED***
