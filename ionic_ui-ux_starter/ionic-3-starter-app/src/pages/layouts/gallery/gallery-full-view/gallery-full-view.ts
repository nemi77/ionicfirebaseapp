import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {GalleryService} from '../gallery.service';

@IonicPage()
@Component({
  selector: 'page-gallery-full-view',
  templateUrl: 'gallery-full-view.html',
  providers:[GalleryService]
})
***REDACTED***
