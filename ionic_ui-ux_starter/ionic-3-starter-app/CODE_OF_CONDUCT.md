# Citizen Code of Conduct

## 1. Purpose

A primary goal of Ionic3 Starterapp is to be inclusive to the largest number of contributors, with the most varied and diverse backgrounds possible. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion (or lack thereof).

This code of conduct outlines our expectations for all those who participate in our community, as well as the consequences for unacceptable behavior.

We invite all those who participate in Ionic3 Starterapp to help us create safe and positive experiences for everyone.

***REDACTED***
