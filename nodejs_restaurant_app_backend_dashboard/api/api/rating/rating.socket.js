/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import RatingEvents from './rating.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
