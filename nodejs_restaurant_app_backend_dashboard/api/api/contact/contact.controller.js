/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/contacts              ->  index
 * POST    /api/contacts              ->  create
 * GET     /api/contacts/:id          ->  show
 * PUT     /api/contacts/:id          ->  upsert
 * PATCH   /api/contacts/:id          ->  patch
 * DELETE  /api/contacts/:id          ->  destroy
 */

***REDACTED***
