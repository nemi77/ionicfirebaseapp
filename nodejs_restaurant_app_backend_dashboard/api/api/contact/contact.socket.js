/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ContactEvents from './contact.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
