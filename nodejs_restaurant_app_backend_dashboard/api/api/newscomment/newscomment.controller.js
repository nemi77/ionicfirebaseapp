/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/newscomments              ->  index
 * POST    /api/newscomments              ->  create
 * GET     /api/newscomments/:id          ->  show
 * PUT     /api/newscomments/:id          ->  upsert
 * PATCH   /api/newscomments/:id          ->  patch
 * DELETE  /api/newscomments/:id          ->  destroy
 */

***REDACTED***
