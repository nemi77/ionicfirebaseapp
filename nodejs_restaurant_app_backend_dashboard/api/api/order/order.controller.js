/**earning
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/orders              ->  index userOrders
 * POST    /api/orders              ->  create loyaltyPoints
 * GET     /api/orders/:id          ->  show
 * PUT     /api/orders/:id          ->  upsert
 * PATCH   /api/orders/:id          ->  patch
 * DELETE  /api/orders/:id          ->  destroy
 */

***REDACTED***
