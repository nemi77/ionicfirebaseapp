/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/testimonials              ->  index
 * POST    /api/testimonials              ->  create
 * GET     /api/testimonials/:id          ->  show
 * PUT     /api/testimonials/:id          ->  upsert
 * PATCH   /api/testimonials/:id          ->  patch
 * DELETE  /api/testimonials/:id          ->  destroy
 */

***REDACTED***
