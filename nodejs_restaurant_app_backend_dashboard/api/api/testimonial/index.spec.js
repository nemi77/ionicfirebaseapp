'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var testimonialCtrlStub = {
  index: 'testimonialCtrl.index',
  show: 'testimonialCtrl.show',
  create: 'testimonialCtrl.create',
***REDACTED***
