'use strict';

const express = require('express');
const router = express.Router();
const controller = require('./banner.controller');

router.get('/getBanner', controller.getBanners);

router.get('/webBanner', controller.webBanners);

***REDACTED***
