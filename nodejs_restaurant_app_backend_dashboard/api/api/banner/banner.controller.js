'use strict';

import Banner from "./banner.model";

export function getBanners(req, res) {
    Banner.find({ status: true }).populate('productId', 'title description categoryTitle discount')
        .then((obj, err) => {
            if (err) throw err;
            if (!obj) {
                res.status(500).send('No Object found');
***REDACTED***
