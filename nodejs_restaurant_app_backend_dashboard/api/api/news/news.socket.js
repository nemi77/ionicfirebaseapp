/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import NewsEvents from './news.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
