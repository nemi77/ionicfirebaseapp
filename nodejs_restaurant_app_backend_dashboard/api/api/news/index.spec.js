'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var newsCtrlStub = {
  index: 'newsCtrl.index',
  show: 'newsCtrl.show',
  create: 'newsCtrl.create',
  upsert: 'newsCtrl.upsert',
  patch: 'newsCtrl.patch',
***REDACTED***
