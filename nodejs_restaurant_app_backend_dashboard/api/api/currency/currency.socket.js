/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CurrencyEvents from './currency.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
