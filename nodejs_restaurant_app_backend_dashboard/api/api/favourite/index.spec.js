'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var favouriteCtrlStub = {
  index: 'favouriteCtrl.index',
  show: 'favouriteCtrl.show',
  create: 'favouriteCtrl.create',
  upsert: 'favouriteCtrl.upsert',
  patch: 'favouriteCtrl.patch',
***REDACTED***
