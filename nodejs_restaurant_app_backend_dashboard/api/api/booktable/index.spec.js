'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var booktableCtrlStub = {
  index: 'booktableCtrl.index',
  show: 'booktableCtrl.show',
  create: 'booktableCtrl.create',
***REDACTED***
