/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import BooktableEvents from './booktable.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
