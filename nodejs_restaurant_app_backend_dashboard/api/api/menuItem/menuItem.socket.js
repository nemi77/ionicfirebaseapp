/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import MenuItemEvents from './menuItem.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
