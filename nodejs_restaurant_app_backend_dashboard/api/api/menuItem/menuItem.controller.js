/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/menuItems              ->  index
 * POST    /api/menuItems              ->  create
 * GET     /api/menuItems/:id          ->  show
 * PUT     /api/menuItems/:id          ->  upsert
 * PATCH   /api/menuItems/:id          ->  patch
 * DELETE  /api/menuItems/:id          ->  destroy
 */

***REDACTED***
