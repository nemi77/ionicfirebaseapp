'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var settingCtrlStub = {
  index: 'settingCtrl.index',
  show: 'settingCtrl.show',
  create: 'settingCtrl.create',
  upsert: 'settingCtrl.upsert',
  patch: 'settingCtrl.patch',
***REDACTED***
