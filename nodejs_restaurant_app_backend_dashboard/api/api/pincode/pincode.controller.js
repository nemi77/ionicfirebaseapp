/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pincodes              ->  index
 * POST    /api/pincodes              ->  create
 * GET     /api/pincodes/:id          ->  show
 * PUT     /api/pincodes/:id          ->  upsert
 * PATCH   /api/pincodes/:id          ->  patch
 * DELETE  /api/pincodes/:id          ->  destroy
 */

***REDACTED***
