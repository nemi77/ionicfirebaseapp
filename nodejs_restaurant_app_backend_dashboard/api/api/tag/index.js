'use strict';

var express = require('express');
var controller = require('./tag.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.hasRole('admin'),controller.index);
router.get('/:id',auth.hasRole('admin'), controller.show);
***REDACTED***
