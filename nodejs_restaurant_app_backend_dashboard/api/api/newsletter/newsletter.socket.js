/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import NewsletterEvents from './newsletter.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
