/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/newsletters              ->  index
 * POST    /api/newsletters              ->  create
 * GET     /api/newsletters/:id          ->  show
 * PUT     /api/newsletters/:id          ->  upsert
 * PATCH   /api/newsletters/:id          ->  patch
 * DELETE  /api/newsletters/:id          ->  destroy
 */

***REDACTED***
