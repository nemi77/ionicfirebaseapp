/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CarddetailEvents from './carddetail.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
