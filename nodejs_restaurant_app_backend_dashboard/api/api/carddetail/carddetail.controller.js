/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/carddetails              ->  index
 * POST    /api/carddetails              ->  create
 * GET     /api/carddetails/:id          ->  show
 * PUT     /api/carddetails/:id          ->  upsert
 * PATCH   /api/carddetails/:id          ->  patch
 * DELETE  /api/carddetails/:id          ->  destroy
 */

***REDACTED***
