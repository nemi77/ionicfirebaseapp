/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import WishlistEvents from './wishlist.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
