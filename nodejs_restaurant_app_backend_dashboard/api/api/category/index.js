'use strict';

var express = require('express');
var controller = require('./category.controller');
import * as auth from '../../auth/auth.service';
var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', auth.hasRole('admin'),controller.create);
***REDACTED***
