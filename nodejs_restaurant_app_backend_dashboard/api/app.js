import express from 'express';
import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import config from './config/environment';
import setSeedDb from './config/seed';
import registerRoutes from './routes';
const ip = require('ip');
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
***REDACTED***
