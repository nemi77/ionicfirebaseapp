const addSubtractDate = require("add-subtract-date");
module.exports={
  graphDataModification(date,data){
    let arr=[];
    arr.push({date:date.getDate(),month:date.getMonth()+1,year:date.getFullYear(),total:0})
    let date1=addSubtractDate.add(date, -1, 'day');
    arr.push({date:date1.getDate(),month:date1.getMonth()+1,year:date1.getFullYear(),total:0})
    let date2=addSubtractDate.add(date, -1, 'day');
    arr.push({date:date2.getDate(),month:date2.getMonth()+1,year:date2.getFullYear(),total:0})
    let date3=addSubtractDate.add(date, -1, 'day');
***REDACTED***
