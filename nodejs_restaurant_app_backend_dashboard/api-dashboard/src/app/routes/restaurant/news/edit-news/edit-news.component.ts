import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';
import { NewsService } from '../news.service';
import { cloudinarUpload } from '../../../../cloudinary.config';

@Component({
  selector: 'app-edit-news',
***REDACTED***
