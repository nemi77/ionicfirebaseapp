import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DeliveryOptionService } from './delivery-options.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-delivery-option',
  templateUrl: './delivery-options.component.html',
  styleUrls: ['./delivery-options.component.scss'],
***REDACTED***
