import { Component, OnInit } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SettingServices } from './settings.service';
import { cloudinarUpload } from '../../../cloudinary.config';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
***REDACTED***
