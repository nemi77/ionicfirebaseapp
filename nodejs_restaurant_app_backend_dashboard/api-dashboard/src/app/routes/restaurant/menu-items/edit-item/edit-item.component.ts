import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';
import { cloudinarUpload } from '../../../../cloudinary.config';
import { MenuItemsService } from '../menu-items.service';

@Component({
  selector: 'app-edit-item',
***REDACTED***
