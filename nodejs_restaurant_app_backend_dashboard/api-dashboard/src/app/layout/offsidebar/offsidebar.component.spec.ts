/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {OffsidebarComponent} from './offsidebar.component';
import {TranslateService, TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {HttpModule, Http} from '@angular/http';

import {SettingsService} from '../../core/settings/settings.service';
import {ThemesService} from '../../core/themes/themes.service';
import {TranslatorService} from '../../core/translator/translator.service';
***REDACTED***
