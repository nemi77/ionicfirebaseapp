import * as ChatStoreAction from './chat-store.action';
import {ChatListModel, MessageListModel} from '../routes/restaurant/chat/chat';

export interface ChatStoreState {
  chatInfo: ChatUsersState;
}

export interface ChatUsersState {
  usersList: Array<ChatListModel>;
  userInfo: ChatListModel;
***REDACTED***
