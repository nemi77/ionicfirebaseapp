import {Action} from '@ngrx/store';
import {ChatListModel, MessageListModel} from '../routes/restaurant/chat/chat';

export const SET_CHAT_USERS_LIST = 'SET_CHAT_USERS_LIST';
export const SET_USER_INFO = 'SET_USER_INFO';
export const SET_NEW_INCOMING_MESSAGE = 'SET_NEW_INCOMING_MESSAGE';

export class SetChatUserListAction implements Action {
  readonly type = SET_CHAT_USERS_LIST;

***REDACTED***
