'use strict';
import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {ConstantService} from './constant.service';
import {BehaviorSubject} from 'rxjs';
import Socket = SocketIOClient.Socket;
import {Store} from '@ngrx/store';
import {ChatStoreState} from './chat-store/chat-store.reducer';
import * as ChatStoreAction from './chat-store/chat-store.action';
import {ChatListModel, MessageListModel} from './routes/restaurant/chat/chat';
***REDACTED***
