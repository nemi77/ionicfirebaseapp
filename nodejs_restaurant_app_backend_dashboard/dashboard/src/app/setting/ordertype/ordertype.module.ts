import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { ToastrModule } from 'ngx-toastr';
import { LaddaModule } from 'angular2-ladda';


***REDACTED***
