import {Component, ViewEncapsulation, OnInit, ElementRef, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ChatService} from '../chat.service';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {userlist, chatData, showChat, ChatListModel, MessageListModel} from '../chat';
import {ChatComponent} from '../chat.component';
import {Store} from '@ngrx/store';
import {SocketSharedService} from '../../../../SocketShare.service';
import {ChatStoreState} from '../../../../chat-store/chat-store.reducer';
***REDACTED***
