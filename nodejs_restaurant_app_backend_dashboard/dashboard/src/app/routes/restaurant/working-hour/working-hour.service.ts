import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from 'src/app/constant.service';

@Injectable({
  providedIn: 'root'
})
export class WorkingHourService {

  constructor(public http: HttpClient, public constantService: ConstantService) { }
***REDACTED***
