import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/core/settings/settings.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { WorkingHourService } from './working-hour.service';

@Component({
  selector: 'app-working-hour',
  templateUrl: './working-hour.component.html',
  styleUrls: ['./working-hour.component.scss']
***REDACTED***
