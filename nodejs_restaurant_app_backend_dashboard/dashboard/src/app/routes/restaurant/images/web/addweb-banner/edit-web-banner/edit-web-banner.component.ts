import { Component, OnInit } from '@angular/core';
import { CloudinaryUploader, CloudinaryOptions } from 'ng2-cloudinary';
import { cloudinarUpload } from 'src/app/cloudinary.config';
import { SettingServices } from 'src/app/routes/restaurant/settings/currency-settings.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-web-banner',
  templateUrl: './edit-web-banner.component.html',
***REDACTED***
