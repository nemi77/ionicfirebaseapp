import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';
import { cloudinarUpload } from '../../../../cloudinary.config';
import { SettingServices } from '../../settings/currency-settings.service';

@Component({
  selector: 'app-web',
  templateUrl: './web.component.html',
  styleUrls: ['./web.component.scss'],
***REDACTED***
