import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';
import { cloudinarUpload } from '../../../../cloudinary.config';
import { SettingServices } from '../../settings/currency-settings.service';
import { MenuItemsService } from '../../menu-items/menu-items.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-application',
***REDACTED***
