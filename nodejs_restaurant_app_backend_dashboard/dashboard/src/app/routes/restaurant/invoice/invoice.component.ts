import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { InvoiceService } from './invoice.service';

@Component({
  selector: 'app-invoice',
***REDACTED***
