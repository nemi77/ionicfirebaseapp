import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TagsService } from '../tags.service';

@Component({
  selector: 'app-add-tags',
  templateUrl: './add-tags.component.html',
***REDACTED***
