import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SharedModule} from '../../shared/shared.module';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {RecoverComponent} from './recover/recover.component';
import {Error404Component} from './error404/error404.component';
import {LaddaModule} from 'angular2-ladda';

***REDACTED***
