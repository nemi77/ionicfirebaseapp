/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/upcomings              ->  index
 * POST    /api/upcomings              ->  create
 * GET     /api/upcomings/:id          ->  show
 * PUT     /api/upcomings/:id          ->  upsert
 * PATCH   /api/upcomings/:id          ->  patch
 * DELETE  /api/upcomings/:id          ->  destroy
 */

***REDACTED***
