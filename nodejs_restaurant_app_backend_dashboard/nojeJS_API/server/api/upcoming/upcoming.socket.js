/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import UpcomingEvents from './upcoming.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
