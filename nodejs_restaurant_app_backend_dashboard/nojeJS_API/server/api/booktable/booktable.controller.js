/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/booktables              ->  index
 * POST    /api/booktables              ->  create
 * GET     /api/booktables/:id          ->  show
 * PUT     /api/booktables/:id          ->  upsert
 * PATCH   /api/booktables/:id          ->  patch
 * DELETE  /api/booktables/:id          ->  destroy
 */

***REDACTED***
