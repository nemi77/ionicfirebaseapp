'use strict';

var express = require('express');
var controller = require('./booktable.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();
//get a list of book tables
router.get('/user', auth.isAuthenticated(),controller.index);

//get a list of book tables of a user
***REDACTED***
