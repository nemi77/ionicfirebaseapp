'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var businessCtrlStub = {
  index: 'businessCtrl.index',
  show: 'businessCtrl.show',
  create: 'businessCtrl.create',
  upsert: 'businessCtrl.upsert',
  patch: 'businessCtrl.patch',
***REDACTED***
