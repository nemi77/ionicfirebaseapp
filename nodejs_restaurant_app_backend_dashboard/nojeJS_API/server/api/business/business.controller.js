/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/businesses              ->  index
 * POST    /api/businesses              ->  create
 * GET     /api/businesses/:id          ->  show
 * PUT     /api/businesses/:id          ->  upsert
 * PATCH   /api/businesses/:id          ->  patch
 * DELETE  /api/businesses/:id          ->  destroy
 */

***REDACTED***
