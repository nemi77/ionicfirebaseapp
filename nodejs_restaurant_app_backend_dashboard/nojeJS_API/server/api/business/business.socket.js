/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import BusinessEvents from './business.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
