'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var menuItemCtrlStub = {
  index: 'menuItemCtrl.index',
  show: 'menuItemCtrl.show',
  create: 'menuItemCtrl.create',
  upsert: 'menuItemCtrl.upsert',
  patch: 'menuItemCtrl.patch',
***REDACTED***
