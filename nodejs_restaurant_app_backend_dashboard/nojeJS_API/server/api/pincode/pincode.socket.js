/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import PincodeEvents from './pincode.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
