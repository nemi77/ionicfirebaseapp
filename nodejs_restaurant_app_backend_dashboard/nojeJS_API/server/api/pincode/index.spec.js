'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var pincodeCtrlStub = {
  index: 'pincodeCtrl.index',
  show: 'pincodeCtrl.show',
  create: 'pincodeCtrl.create',
***REDACTED***
