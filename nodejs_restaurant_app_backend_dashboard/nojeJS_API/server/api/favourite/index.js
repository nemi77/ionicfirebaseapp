'use strict';

var express = require('express');
var controller = require('./favourite.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router(); 
router.get('/', auth.hasRole('admin'), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
***REDACTED***
