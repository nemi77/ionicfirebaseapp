/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import AddressEvents from './address.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
