'use strict';

var express = require('express');
var controller = require('./contact.controller');

var router = express.Router();

router.get('/:name', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
***REDACTED***
