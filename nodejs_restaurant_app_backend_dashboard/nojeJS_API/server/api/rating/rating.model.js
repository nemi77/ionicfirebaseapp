'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './rating.events';
import {Schema} from 'mongoose';

var RatingSchema = new mongoose.Schema({
  user:{
      type: Schema.ObjectId,
      ref: 'User'
***REDACTED***
