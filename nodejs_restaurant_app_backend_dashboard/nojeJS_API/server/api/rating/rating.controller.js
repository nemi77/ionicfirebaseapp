/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/ratings              ->  index
 * POST    /api/ratings              ->  create
 * GET     /api/ratings/:id          ->  show
 * PUT     /api/ratings/:id          ->  upsert
 * PATCH   /api/ratings/:id          ->  patch
 * DELETE  /api/ratings/:id          ->  destroy
 */

***REDACTED***
