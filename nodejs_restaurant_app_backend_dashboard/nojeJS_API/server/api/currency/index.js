'use strict';

var express = require('express');
var controller = require('./currency.controller');
var auth 		= require('../../auth/auth.service');
var router = express.Router();

router.get('/', controller.index);
router.get('/:id',auth.hasRole('admin'), controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
***REDACTED***
