/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /currencies              ->  index
 * POST    /currencies              ->  create
 * GET     /currencies/:id          ->  show
 * PUT     /currencies/:id          ->  upsert
 * PATCH   /currencies/:id          ->  patch
 * DELETE  /currencies/:id          ->  destroy
 */

***REDACTED***
