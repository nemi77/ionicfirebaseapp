'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var tagCtrlStub = {
  index: 'tagCtrl.index',
  show: 'tagCtrl.show',
  create: 'tagCtrl.create',
  update: 'tagCtrl.update',
  destroy: 'tagCtrl.destroy'
***REDACTED***
