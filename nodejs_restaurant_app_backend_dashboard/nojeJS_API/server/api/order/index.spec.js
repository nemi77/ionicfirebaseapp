'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var orderCtrlStub = {
  index: 'orderCtrl.index',
  show: 'orderCtrl.show',
  create: 'orderCtrl.create',
  upsert: 'orderCtrl.upsert',
  patch: 'orderCtrl.patch',
***REDACTED***
