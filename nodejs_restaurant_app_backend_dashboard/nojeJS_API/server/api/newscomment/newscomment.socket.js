/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import NewscommentEvents from './newscomment.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
