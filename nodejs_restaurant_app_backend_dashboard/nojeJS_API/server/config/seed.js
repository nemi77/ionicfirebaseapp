/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Tag from '../api/tag/tag.model';
import Setting from '../api/setting/setting.model';
import Booktable from '../api/booktable/booktable.model';
***REDACTED***
