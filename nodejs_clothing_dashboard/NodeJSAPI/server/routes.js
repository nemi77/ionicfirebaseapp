/**
 * Main application routes
 */
'use strict';
import errors from './components/errors';
import path from 'path';
export default function(app) {
  // Insert routes below
  app.use('/api/currencies', require('./api/currency'));
  app.use('/api/subscriptions', require('./api/subscription'));
***REDACTED***
