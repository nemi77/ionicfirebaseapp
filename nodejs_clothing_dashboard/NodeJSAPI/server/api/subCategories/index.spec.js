'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var subCategoriesCtrlStub = {
  index: 'subCategoriesCtrl.index',
  show: 'subCategoriesCtrl.show',
  create: 'subCategoriesCtrl.create',
***REDACTED***
