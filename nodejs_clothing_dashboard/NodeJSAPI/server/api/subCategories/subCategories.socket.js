/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import SubCategoriesEvents from './subCategories.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
