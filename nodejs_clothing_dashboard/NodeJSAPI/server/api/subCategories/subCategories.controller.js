/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subCategoriess              ->  index
 * POST    /api/subCategoriess              ->  create
 * GET     /api/subCategoriess/:id          ->  show
 * PUT     /api/subCategoriess/:id          ->  upsert
 * PATCH   /api/subCategoriess/:id          ->  patch
 * DELETE  /api/subCategoriess/:id          ->  destroy
 */

***REDACTED***
