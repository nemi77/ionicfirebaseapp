/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import PaymentmethodEvents from './paymentmethod.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
