'use strict';

var express = require('express');
var controller = require('./paymentmethod.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
***REDACTED***
