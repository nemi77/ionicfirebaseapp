/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/paymentmethods              ->  index
 * POST    /api/paymentmethods              ->  create
 * GET     /api/paymentmethods/:id          ->  show
 * PUT     /api/paymentmethods/:id          ->  upsert
 * PATCH   /api/paymentmethods/:id          ->  patch
 * DELETE  /api/paymentmethods/:id          ->  destroy
 */

***REDACTED***
