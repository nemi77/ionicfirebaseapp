'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './notification.events';
import {Schema} from 'mongoose';
var NotificationSchema = new mongoose.Schema({
  order: {
    type: Schema.ObjectId,
    ref: 'Order'
  },
***REDACTED***
