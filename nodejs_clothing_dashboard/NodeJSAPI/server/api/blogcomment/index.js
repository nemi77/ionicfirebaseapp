'use strict';

var express = require('express');
var controller = require('./blogcomment.controller');
import * as auth from '../../auth/auth.service';
var router = express.Router();

router.get('/', controller.index);

router.get('/:id', controller.show);
***REDACTED***
