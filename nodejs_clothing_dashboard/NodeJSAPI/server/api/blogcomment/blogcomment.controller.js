/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/blogcomments              ->  index
 * POST    /api/blogcomments              ->  create
 * GET     /api/blogcomments/:id          ->  show
 * PUT     /api/blogcomments/:id          ->  upsert
 * PATCH   /api/blogcomments/:id          ->  patch
 * DELETE  /api/blogcomments/:id          ->  destroy
 */

***REDACTED***
