/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import BlogcommentEvents from './blogcomment.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
