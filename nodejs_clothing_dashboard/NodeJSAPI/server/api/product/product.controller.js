/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products              ->  index getByFilter
 * POST    /api/products              ->  create
 * GET     /api/products/:id          ->  show
 * PUT     /api/products/:id          ->  upsert
 * PATCH   /api/products/:id          ->  patch
 * DELETE  /api/products/:id          ->  destroy
 */

***REDACTED***
