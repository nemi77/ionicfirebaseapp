'use strict';

var express = require('express');
var controller = require('./carddetail.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

router.get('/', auth.isAuthenticated(),controller.index);

router.get('/:id', auth.isAuthenticated(),controller.show);
***REDACTED***
