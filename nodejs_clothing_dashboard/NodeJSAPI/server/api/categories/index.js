'use strict';

var express = require('express'); 
var controller = require('./categories.controller');
import * as auth from '../../auth/auth.service';
var router = express.Router();
//get all categories
router.get('/', controller.index);
//pagination
router.post('/pagination/data', controller.pagination);
***REDACTED***
