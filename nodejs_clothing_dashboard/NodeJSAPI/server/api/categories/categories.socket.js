/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CategoriesEvents from './categories.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
