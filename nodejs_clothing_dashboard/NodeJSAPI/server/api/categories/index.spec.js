'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var categoriesCtrlStub = {
  index: 'categoriesCtrl.index',
  show: 'categoriesCtrl.show',
  create: 'categoriesCtrl.create',
***REDACTED***
