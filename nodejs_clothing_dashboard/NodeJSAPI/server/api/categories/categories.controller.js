/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/categoriess              ->  index
 * POST    /api/categoriess              ->  create
 * GET     /api/categoriess/:id          ->  show
 * PUT     /api/categoriess/:id          ->  upsert
 * PATCH   /api/categoriess/:id          ->  patch
 * DELETE  /api/categoriess/:id          ->  destroy
 */

***REDACTED***
