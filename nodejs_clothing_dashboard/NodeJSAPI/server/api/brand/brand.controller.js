/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/brands              ->  index
 * POST    /api/brands              ->  create
 * GET     /api/brands/:id          ->  show
 * PUT     /api/brands/:id          ->  upsert
 * PATCH   /api/brands/:id          ->  patch
 * DELETE  /api/brands/:id          ->  destroy
 */

***REDACTED***
