/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import BrandEvents from './brand.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
