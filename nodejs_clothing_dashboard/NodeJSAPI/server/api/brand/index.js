'use strict';

var express = require('express');
var controller = require('./brand.controller');
import * as auth from '../../auth/auth.service';
var router = express.Router();

router.get('/', controller.index);
//
router.get('/product/count', controller.getBrandProducts);
***REDACTED***
