/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import SubscriptionEvents from './subscription.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
