'use strict';

var express = require('express');
var controller = require('./cart.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();
//get a list of carts of a user
router.get('/user', auth.isAuthenticated(),controller.index);
//get a single cart
router.get('/:id',  auth.isAuthenticated(),controller.show);
***REDACTED***
