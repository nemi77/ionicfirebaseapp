/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/carts              ->  index
 * POST    /api/carts              ->  create
 * GET     /api/carts/:id          ->  show
 * PUT     /api/carts/:id          ->  upsert
 * PATCH   /api/carts/:id          ->  patch
 * DELETE  /api/carts/:id          ->  destroy
 */

***REDACTED***
