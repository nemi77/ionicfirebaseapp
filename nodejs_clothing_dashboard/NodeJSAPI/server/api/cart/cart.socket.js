/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import CartEvents from './cart.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
