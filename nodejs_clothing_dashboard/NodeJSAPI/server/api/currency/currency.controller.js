/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/currencies              ->  index
 * POST    /api/currencies              ->  create
 * GET     /api/currencies/:id          ->  show
 * PUT     /api/currencies/:id          ->  upsert
 * PATCH   /api/currencies/:id          ->  patch
 * DELETE  /api/currencies/:id          ->  destroy
 */

***REDACTED***
