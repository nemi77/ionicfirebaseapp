/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/addresss              ->  index
 * POST    /api/addresss              ->  create
 * GET     /api/addresss/:id          ->  show
 * PUT     /api/addresss/:id          ->  upsert
 * PATCH   /api/addresss/:id          ->  patch
 * DELETE  /api/addresss/:id          ->  destroy
 */

***REDACTED***
