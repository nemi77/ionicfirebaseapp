'use strict';

var express = require('express');
var controller = require('./coupon.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();
//get a list of copons
router.get('/', controller.index);
//get a single coupon
router.get('/:id', controller.show);
***REDACTED***
