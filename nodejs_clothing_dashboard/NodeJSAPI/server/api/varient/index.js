'use strict';

var express = require('express');
var controller = require('./varient.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

router.get('/by/product/:id', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
***REDACTED***
