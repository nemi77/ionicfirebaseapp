/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/varients              ->  index
 * POST    /api/varients              ->  create
 * GET     /api/varients/:id          ->  show
 * PUT     /api/varients/:id          ->  upsert
 * PATCH   /api/varients/:id          ->  patch
 * DELETE  /api/varients/:id          ->  destroy
 */

***REDACTED***
