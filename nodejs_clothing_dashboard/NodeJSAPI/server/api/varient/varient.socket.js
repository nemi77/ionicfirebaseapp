/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import VarientEvents from './varient.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
