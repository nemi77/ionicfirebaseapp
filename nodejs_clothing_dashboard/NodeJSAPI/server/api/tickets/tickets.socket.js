/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import TicketsEvents from './tickets.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
