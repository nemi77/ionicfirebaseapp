'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var ticketsCtrlStub = {
  index: 'ticketsCtrl.index',
  show: 'ticketsCtrl.show',
  create: 'ticketsCtrl.create',
***REDACTED***
