/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /tickets              ->  index
 * POST    /tickets              ->  create
 * GET     /tickets/:id          ->  show
 * PUT     /tickets/:id          ->  upsert
 * PATCH   /tickets/:id          ->  patch
 * DELETE  /tickets/:id          ->  destroy
 */

***REDACTED***
