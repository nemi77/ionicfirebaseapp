'use strict';

var express = require('express');   //.alltickets
var controller = require('./tickets.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
***REDACTED***
