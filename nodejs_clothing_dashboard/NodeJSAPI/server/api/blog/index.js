'use strict';

var express = require('express');
var controller = require('./blog.controller');

var router = express.Router();
//get a list of blogs
router.get('/', controller.index);
//get a list of blogs pagination
router.get('/pagination/data/:page/:limit', controller.pagination);
***REDACTED***
