/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import BlogEvents from './blog.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
