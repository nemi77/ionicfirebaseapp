'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var blogCtrlStub = {
  index: 'blogCtrl.index',
  show: 'blogCtrl.show',
  create: 'blogCtrl.create',
***REDACTED***
