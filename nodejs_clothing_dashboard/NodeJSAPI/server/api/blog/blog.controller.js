/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/blogs              ->  index
 * POST    /api/blogs              ->  create
 * GET     /api/blogs/:id          ->  show
 * PUT     /api/blogs/:id          ->  upsert
 * PATCH   /api/blogs/:id          ->  patch
 * DELETE  /api/blogs/:id          ->  destroy
 */

***REDACTED***
