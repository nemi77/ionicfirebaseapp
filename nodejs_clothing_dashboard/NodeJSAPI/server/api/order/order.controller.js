
/*
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/Orders              ->  index
 * POST    /api/Orders              ->  create
 * GET     /api/Orders/:id          ->  show
 * PUT     /api/Orders/:id          ->  upsert
 * PATCH   /api/Orders/:id          ->  patch
 * DELETE  /api/Orders/:id          ->  destroy
 */
***REDACTED***
