 'use strict';

var express = require('express'); 
var controller = require('./order.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

router.get('/', auth.isAuthenticated(),controller.index);
//pagination for orders
router.post('/pagination/data', controller.pagination);
***REDACTED***
