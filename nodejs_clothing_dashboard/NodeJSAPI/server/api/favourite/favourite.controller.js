/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/favourites              ->  index
 * POST    /api/favourites              ->  create
 * GET     /api/favourites/:id          ->  show
 * PUT     /api/favourites/:id          ->  upsert
 * PATCH   /api/favourites/:id          ->  patch
 * DELETE  /api/favourites/:id          ->  destroy
 */

***REDACTED***
