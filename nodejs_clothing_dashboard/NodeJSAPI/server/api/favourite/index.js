'use strict';

var express = require('express');
var controller = require('./favourite.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();
router.get('/:id',auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(),controller.create);
//delete a favourite
router.post('/delete',  auth.isAuthenticated(),controller.destroy);
***REDACTED***
