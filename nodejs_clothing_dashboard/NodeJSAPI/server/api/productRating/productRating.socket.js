/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ProductRatingEvents from './productRating.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
