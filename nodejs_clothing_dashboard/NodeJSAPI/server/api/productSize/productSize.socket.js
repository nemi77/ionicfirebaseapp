/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ProductSizeEvents from './productSize.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
