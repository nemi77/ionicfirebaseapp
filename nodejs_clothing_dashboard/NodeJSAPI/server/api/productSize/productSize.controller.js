/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/productSizes              ->  index
 * POST    /api/productSizes              ->  create
 * GET     /api/productSizes/:id          ->  show
 * PUT     /api/productSizes/:id          ->  upsert
 * PATCH   /api/productSizes/:id          ->  patch
 * DELETE  /api/productSizes/:id          ->  destroy
 */

***REDACTED***
