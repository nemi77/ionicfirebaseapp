/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/offers              ->  index
 * POST    /api/offers              ->  create
 * GET     /api/offers/:id          ->  show
 * PUT     /api/offers/:id          ->  upsert
 * PATCH   /api/offers/:id          ->  patch
 * DELETE  /api/offers/:id          ->  destroy
 */

***REDACTED***
