/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import OfferEvents from './offer.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
