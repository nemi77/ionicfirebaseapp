/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import ProductColorsEvents from './productColors.events';

// Model events to emit
var events = ['save', 'remove'];
***REDACTED***
