import { Component } from '@angular/core';
import { SocketSharedService } from './SocketShare.service';
import { Router } from '@angular/router';
import { LoginService } from './pages/extra-pages/sign-in/sign-in.service';

@Component({
  moduleId: module.id,
  selector: 'app',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['../assets/sass/style.scss'],
***REDACTED***
