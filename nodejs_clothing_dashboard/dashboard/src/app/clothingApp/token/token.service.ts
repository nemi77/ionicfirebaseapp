import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class TokenService {
	constructor(private crud: CrudService) { }
	getTickets() {
		return this.crud.get('tickets');
	}

	getSingleTicket(id: string) {
***REDACTED***
