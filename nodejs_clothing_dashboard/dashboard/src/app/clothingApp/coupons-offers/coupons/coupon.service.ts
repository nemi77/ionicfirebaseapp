import { Injectable } from '@angular/core';
import { CrudService } from '../../services/crud.service';
@Injectable()
export class CouponService {
    constructor(private crud: CrudService) { }
    getCoupons() {
        return this.crud.get('coupons');
    }

    deleteCoupon(id: string) {
***REDACTED***
