import { Injectable } from '@angular/core';
import { CrudService } from '../../services/crud.service';
@Injectable()
export class LoyaltyProgramService {
	constructor(private crud:CrudService) { }

	getSettings(){
		return this.crud.get('settings');
	}

***REDACTED***
