import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../../layouts/shared-service';
import { SizeService } from './size.service';
import { ToastrService } from 'toastr-ng2';

@Component({
	selector: 'app-size',
	templateUrl: './size.component.html',
	styleUrls: ['./size.component.scss'],
***REDACTED***
