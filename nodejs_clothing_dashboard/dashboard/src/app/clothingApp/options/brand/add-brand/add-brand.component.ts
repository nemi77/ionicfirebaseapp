import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from '../../../../layouts/shared-service';
import { BrandService } from '../brand.service';
import { ToastrService } from 'toastr-ng2';

@Component({
	selector: 'app-add-brand',
	templateUrl: './add-brand.component.html',
***REDACTED***
