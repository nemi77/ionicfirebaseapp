import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../../layouts/shared-service';
import {ColorService} from '../color.service';
import {ToastrService} from 'toastr-ng2';

@Component({
    selector: 'app-update-color',
    templateUrl: './update-color.component.html',
***REDACTED***
