import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from '../../../../layouts/shared-service';
import { CouponService } from '../coupon.service';
import { ToastrService } from 'toastr-ng2';
@Component({
    selector: 'app-add-coupon',
    templateUrl: './add-coupon.component.html',
    styleUrls: ['./add-coupon.component.scss'],
***REDACTED***
