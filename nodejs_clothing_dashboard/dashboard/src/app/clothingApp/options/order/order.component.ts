import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../../layouts/shared-service';
import { OrderService } from './order.service';
import { ToastrService } from 'toastr-ng2';
import { CrudService } from '../../services/crud.service';

@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
***REDACTED***
