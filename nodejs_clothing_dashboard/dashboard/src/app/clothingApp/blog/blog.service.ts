import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class BlogsService {
	constructor(private crud:CrudService){ }
	getBlogs(){
		return this.crud.get('blogs');
	}

	getBlogById(id:string){
***REDACTED***
