import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CloudinaryUploader, CloudinaryOptions } from 'ng2-cloudinary';
import { ToastrService } from 'toastr-ng2';
import { BlogsService } from '../blog.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [BlogsService]
***REDACTED***
