import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { SharedService } from '../../../layouts/shared-service';
import { ProductsService } from '../product.service';
import { ToastrService } from 'toastr-ng2';
import { CKEditorModule } from 'ng2-ckeditor';

@Component({
***REDACTED***
