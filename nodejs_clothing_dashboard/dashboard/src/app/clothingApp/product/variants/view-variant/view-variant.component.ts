import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VariantService } from '../variant.service';
import { SharedService } from '../../../../layouts/shared-service';

@Component({
	selector: 'app-view-variant',
	templateUrl: './view-variant.component.html',
	styleUrls: ['./view-variant.component.css'],
	providers: [VariantService]
***REDACTED***
