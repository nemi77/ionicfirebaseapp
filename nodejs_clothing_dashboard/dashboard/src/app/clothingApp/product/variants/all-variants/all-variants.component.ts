import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VariantService } from '../variant.service';
import { ToastrService } from 'toastr-ng2';
import { SharedService } from '../../../../layouts/shared-service';

@Component({
	selector: 'app-all-variants',
	templateUrl: './all-variants.component.html',
	styleUrls: ['./all-variants.component.css'],
***REDACTED***
