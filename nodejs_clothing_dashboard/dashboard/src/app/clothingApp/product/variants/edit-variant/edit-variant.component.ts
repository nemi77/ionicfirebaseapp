import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../product.service';
import { VariantService } from '../variant.service';
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { SharedService } from '../../../../layouts/shared-service';
import { ToastrService } from 'toastr-ng2';

@Component({
***REDACTED***
