import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../../layouts/shared-service';
import { ProductsService } from '../product.service';

@Component({
	selector: 'app-view-products',
	templateUrl: './view-products.component.html',
	styleUrls: ['./view-products.component.scss'],
	providers: [ProductsService]
***REDACTED***
