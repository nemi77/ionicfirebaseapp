import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../../layouts/shared-service';
import { CategoriesService } from '../categories.service';
import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'app-update-categories',
***REDACTED***
