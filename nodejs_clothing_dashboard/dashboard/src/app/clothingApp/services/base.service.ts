import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class CrudBaseService {
	protected data: any = { api: null };
	protected record: any = { api: null };
	private observable: Observable<any>;
***REDACTED***
