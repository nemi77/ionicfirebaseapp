import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentMethodsService } from './payment-methods.service';
import { ToastrService } from 'toastr-ng2';
@Component({
	selector: 'app-payment-methods',
	templateUrl: './payment-methods.component.html',
	styleUrls: ['./payment-methods.component.css'],
	providers: [PaymentMethodsService]
})
***REDACTED***
