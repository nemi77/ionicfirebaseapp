import { Injectable } from '@angular/core';
import { CrudService } from '../services/crud.service';
@Injectable()
export class PaymentMethodsService {
	constructor(private crud: CrudService) { }
	getPayments() {
		return this.crud.get('paymentmethods');
	}

	savePayments(body: any) {
***REDACTED***
