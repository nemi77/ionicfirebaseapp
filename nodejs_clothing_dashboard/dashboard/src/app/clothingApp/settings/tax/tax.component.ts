import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaxService } from './tax.service';
import { ToastrService } from 'toastr-ng2';
@Component({
	selector: 'app-tax',
	templateUrl: './tax.component.html',
	styleUrls: ['./tax.component.css'],
	providers: [TaxService]
})
***REDACTED***
