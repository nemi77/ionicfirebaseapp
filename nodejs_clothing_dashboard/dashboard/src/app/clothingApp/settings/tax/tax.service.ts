import { Injectable } from '@angular/core';
import { CrudService } from '../../services/crud.service';
@Injectable()
export class TaxService {
	constructor(private crud: CrudService) { }

	getTaxData() {
		return this.crud.get('settings');
	}

***REDACTED***
