import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SharedService } from '../../layouts/shared-service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SubCategoriesService } from './sub-categories.service';

@Component({
	selector: 'app-sub-categories',
	templateUrl: './sub-categories.component.html',
	styleUrls: ['./sub-categories.component.scss'],
***REDACTED***
