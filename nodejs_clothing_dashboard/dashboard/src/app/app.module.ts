import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SelectModule} from 'ng2-select';
import {MaterialModule} from '@angular/material';
import {DataTableModule} from 'angular2-datatable';
import {ChartsModule} from 'ng2-charts';
***REDACTED***
