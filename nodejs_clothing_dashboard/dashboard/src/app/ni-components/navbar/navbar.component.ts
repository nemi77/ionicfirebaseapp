import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { SocketSharedService }from '../../SocketShare.service';
import { NavBarService } from './navbar.service';
@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss'],
  providers: [NavBarService],
***REDACTED***
