import { Component, OnInit } from '@angular/core';

import { MainMenuItem } from './main-menu-item';
import { MainMenuService } from './main-menu.service';

@Component({
  moduleId: module.id,
  selector: 'main-menu',
  templateUrl: 'main-menu.component.html',
  styleUrls: ['main-menu.component.scss'],
***REDACTED***
