import { Component, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'card',
  templateUrl: 'card.component.html',
  styleUrls: ['card.component.scss']
})
export class A2CardComponent {
  @Input() title: string = '';
***REDACTED***
