import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DefaultLayoutComponent } from './layouts/default/default.component';
import { ExtraLayoutComponent } from './layouts/extra/extra.component';


import { PageDashboardComponent } from './pages/dashboard/dashboard.component';

import { PageNotFoundComponent } from './pages/not-found/not-found.component';
***REDACTED***
