import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CookieService } from 'ngx-cookie';
import { LoginService } from './sign-in.service';
import { ToastrService } from 'toastr-ng2';
import { SocketSharedService } from '../../../SocketShare.service';

@Component({
  selector: 'page-sign-in',
***REDACTED***
