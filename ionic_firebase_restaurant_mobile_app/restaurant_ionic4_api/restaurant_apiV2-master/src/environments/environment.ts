export const environment = {
  production: false,
  cloudinaryConfig: {
    cloudName: 'pietechsolutions',
    uploadPreset: 't0iey0lk'
  },
  languages: [
    {
      "language": "ENGLISH",
      "value": "en"
***REDACTED***
