import { ConstService } from './../../services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CheckoutService {
  constructor(public http: HttpClient, public constService: ConstService) { }
***REDACTED***
