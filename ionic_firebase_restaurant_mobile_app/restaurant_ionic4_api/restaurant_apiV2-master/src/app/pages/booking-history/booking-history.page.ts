import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { BookingHistoryService } from './booking-history.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.page.html',
  styleUrls: ['./booking-history.page.scss'],
  providers: [BookingHistoryService]
***REDACTED***
