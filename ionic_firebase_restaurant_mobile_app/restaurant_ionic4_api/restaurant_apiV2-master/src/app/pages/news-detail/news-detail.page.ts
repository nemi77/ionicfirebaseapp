import { Service } from 'src/app/services/app.service';
import { Component, OnInit } from '@angular/core';
import { NewsDetailsService } from './news-detail.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
  providers: [NewsDetailsService]
***REDACTED***
