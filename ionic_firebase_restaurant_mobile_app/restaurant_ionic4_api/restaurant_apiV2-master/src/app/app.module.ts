import { OneSignal } from '@ionic-native/onesignal/ngx';
import { environment } from './../environments/environment.prod';
import { RatingModule } from 'ngx-rating';
import { MomentModule } from 'ngx-moment';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
***REDACTED***
