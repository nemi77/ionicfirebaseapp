import { FavouriteService } from './pages/favourite/favourite.service';
import { NewsService } from './pages/news/news.service';
import { environment } from './../environments/environment';
import { Service } from './services/app.service';
import { Component } from '@angular/core';
import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
***REDACTED***
