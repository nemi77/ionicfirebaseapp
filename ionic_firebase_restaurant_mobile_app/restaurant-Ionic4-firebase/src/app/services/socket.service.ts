import { Service } from './app.service';
import { ConstService } from './constant.service';
import { Injectable, NgZone } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
***REDACTED***
