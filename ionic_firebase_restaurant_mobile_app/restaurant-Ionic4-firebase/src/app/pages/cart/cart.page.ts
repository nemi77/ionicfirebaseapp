import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { CartService } from './cart.service';
import { NavController } from '@ionic/angular';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.page.html',
	styleUrls: ['./cart.page.scss'],
	providers: [CartService]
***REDACTED***
