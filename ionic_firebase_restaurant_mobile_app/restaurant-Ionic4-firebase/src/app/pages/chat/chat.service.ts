import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private afDB: AngularFireDatabase, private afAuth: AngularFireAuth) { }

***REDACTED***
