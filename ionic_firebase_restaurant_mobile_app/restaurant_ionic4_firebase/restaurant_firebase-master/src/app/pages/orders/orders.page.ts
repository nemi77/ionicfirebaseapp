import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { OrdersService } from './orders.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
  providers: [OrdersService]
***REDACTED***
