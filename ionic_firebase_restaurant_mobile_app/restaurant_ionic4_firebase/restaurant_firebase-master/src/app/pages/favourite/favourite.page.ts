import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { FavouriteService } from './favourite.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.page.html',
  styleUrls: ['./favourite.page.scss'],
  providers: [FavouriteService]
***REDACTED***
