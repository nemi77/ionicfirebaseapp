import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { CategoryService } from './category.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
  providers: [CategoryService]
***REDACTED***
