import { Component, OnInit } from '@angular/core';
import { NewsService } from './news.service';
import { NavController } from '@ionic/angular';
import { Service } from 'src/app/services/app.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
  providers: [NewsService]
***REDACTED***
