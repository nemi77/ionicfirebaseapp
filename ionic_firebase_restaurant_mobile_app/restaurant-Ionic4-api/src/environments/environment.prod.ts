export const environment = {
  production: true,
  cloudinaryConfig: {
    cloudName: 'pietechsolutions',
    uploadPreset: 't0iey0lk'
  },
  languages: [
    {
      "language": "ENGLISH",
      "value": "en"
***REDACTED***
