import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import { FileUploadModule } from 'ng2-file-upload';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { IonicModule } from '@ionic/angular';
import { SettingsPage } from './settings.page';

***REDACTED***
