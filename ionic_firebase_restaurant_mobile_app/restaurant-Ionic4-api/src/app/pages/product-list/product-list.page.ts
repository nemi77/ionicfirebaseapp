import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductListService } from './product-list.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
***REDACTED***
