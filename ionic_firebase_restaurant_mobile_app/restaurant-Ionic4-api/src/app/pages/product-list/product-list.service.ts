import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstService } from "../../services/constant.service";

@Injectable({
  providedIn: 'root'
})

export class ProductListService {
  constructor(public http: HttpClient, public constService: ConstService) { }
***REDACTED***
