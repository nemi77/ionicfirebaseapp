import { ConstService } from './../../services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailsService {
  constructor(public http: HttpClient, public constService: ConstService) { }

***REDACTED***
