import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstService } from "../../services/constant.service";

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(public http: HttpClient, public constService: ConstService) { }

***REDACTED***
