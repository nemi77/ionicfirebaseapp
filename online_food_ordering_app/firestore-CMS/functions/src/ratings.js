const functions = require("firebase-functions");
const admin = require("firebase-admin");

exports.reviewRating = functions.firestore
    .document("menuItems/{menuId}/reviews/{reviewId}")
    .onCreate((change, context) => {
        // const reviewId = context.params.reviewId;
        const ratData = change.data();
        const menuId = context.params.menuId;
        return admin.firestore().collection("menuItems").doc(menuId).get().then(doc => {
***REDACTED***
