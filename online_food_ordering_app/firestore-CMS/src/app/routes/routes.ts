import { NgModule } from "@angular/core";
import { LayoutComponent } from "../layout/layout.component";

import { LoginComponent } from "./pages/login/login.component";

import { RegisterComponent } from "./pages/register/register.component";
import { RecoverComponent } from "./pages/recover/recover.component";
import { Error404Component } from "./pages/error404/error404.component";

import { HomeComponent } from "./home/home/home.component";
***REDACTED***
