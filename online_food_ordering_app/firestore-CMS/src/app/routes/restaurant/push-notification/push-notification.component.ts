import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { PushNotificationService } from "./push-notification.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-push-notification",
  templateUrl: "./push-notification.component.html",
  styleUrls: ["./push-notification.component.scss"],
***REDACTED***
