// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

/**********************************************************
* This script provides syntax highlighting support for
* the Ntriples format.
* Ntriples format specification:
*     http://www.w3.org/TR/rdf-testcases/#ntriples
***********************************************************/

***REDACTED***
