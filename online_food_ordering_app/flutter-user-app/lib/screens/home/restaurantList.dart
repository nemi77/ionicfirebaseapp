import 'package:flutter/material.dart';
import 'package:multi_restaurant_firestore/screens/home/restaurantdetails.dart';
import 'package:multi_restaurant_firestore/services/localization.dart';
import 'package:multi_restaurant_firestore/styles/style.dart';
import 'package:async_loader/async_loader.dart';
import 'package:multi_restaurant_firestore/services/homeServices.dart';

class RestaurantList extends StatefulWidget {
  final String type;
  RestaurantList({Key key, this.type}) : super(key: key);
***REDACTED***
