import 'package:flutter/material.dart';

class CardSwipping extends StatefulWidget {
  const CardSwipping({Key key, this.isCurrentTab = false, this.image})
      : super(key: key);
  final bool isCurrentTab;
  final AssetImage image;

  @override
  _CardSwippingState createState() => _CardSwippingState();
***REDACTED***
