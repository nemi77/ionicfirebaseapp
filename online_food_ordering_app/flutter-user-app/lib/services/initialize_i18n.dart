import 'dart:convert';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'constant.dart' show languages;

Future<String> loadJsonFromAsset(language) async {
  print('aaaaaaaaaaaa $language');
  return await rootBundle.loadString('lib/assets/i18n/' + language + '.json');
}

***REDACTED***
