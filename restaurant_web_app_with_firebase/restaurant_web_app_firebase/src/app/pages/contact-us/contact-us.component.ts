import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from './contact.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  providers: [ContactService]
})
export class ContactUsComponent implements OnInit {
***REDACTED***
