import { Component, OnInit } from '@angular/core';
import { MenuListService } from './menu-list.service';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss'],
  providers: [MenuListService]
***REDACTED***
