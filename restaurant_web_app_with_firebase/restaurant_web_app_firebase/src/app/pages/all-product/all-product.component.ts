import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AllProductsService } from './all-products.service';
import swal from 'sweetalert2';

@Component({
    selector: 'app-all-product',
    templateUrl: './all-product.component.html',
    styleUrls: ['./all-product.component.scss'],
    providers: [AllProductsService]
***REDACTED***
