import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SingleProductService } from './single-product.service';
import swal from 'sweetalert2';
import { shareLink } from '../../firebase.config';
import { SeoService } from '../../seo.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
***REDACTED***
