import { Component, OnInit } from '@angular/core';
import { BookedTableService } from './booked-table.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-booked-table',
  templateUrl: './booked-table.component.html',
  styleUrls: ['./booked-table.component.scss'],
  providers: [ BookedTableService ]
})
export class BookedTableComponent implements OnInit {
***REDACTED***
