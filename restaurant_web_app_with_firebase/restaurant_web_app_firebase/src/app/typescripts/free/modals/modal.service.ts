import { ComponentRef, Injectable, TemplateRef, EventEmitter } from '@angular/core';

import { ComponentLoader } from '../utils/component-loader';
import { ComponentLoaderFactory } from '../utils/component-loader';
import { ModalBackdropComponent } from './modalBackdrop.component';
import { ModalContainerComponent } from './modalContainer.component';
import { MDBModalRef, ClassName, modalConfigDefaults, ModalOptions, TransitionDurations } from './modal.options';
import { msConfig } from './modalService.config';

@Injectable()
***REDACTED***
