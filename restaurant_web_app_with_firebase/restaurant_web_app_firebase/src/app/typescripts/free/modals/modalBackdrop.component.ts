import { Component, ElementRef, OnInit, Renderer, HostBinding } from '@angular/core';

import { ClassName } from './modal.options';
import { isBs3 } from '../utils/ng2-bootstrap-config';
import { Utils } from '../utils/utils.class';

export class ModalBackdropOptions {
  public animate = true;

  public constructor(options: ModalBackdropOptions) {
***REDACTED***
