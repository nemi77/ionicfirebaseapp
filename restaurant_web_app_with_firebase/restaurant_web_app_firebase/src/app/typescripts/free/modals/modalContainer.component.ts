import { Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer, HostBinding } from '@angular/core';
import { ClassName, DISMISS_REASONS, ModalOptions, TransitionDurations } from './modal.options';
import { isBs3 } from '../utils/ng2-bootstrap-config';
import { msConfig } from './modalService.config';

@Component({
  selector: 'mdb-modal-container',
  template: `
  <div [class]="'modal-dialog' + (config.class ? ' ' + config.class : '')" role="document">
  <div class="modal-content"><ng-content></ng-content></div>
***REDACTED***
