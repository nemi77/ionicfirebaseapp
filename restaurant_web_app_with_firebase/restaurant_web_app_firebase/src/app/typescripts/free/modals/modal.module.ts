import { NgModule, ModuleWithProviders, NO_ERRORS_SCHEMA } from '@angular/core';

import { ModalBackdropComponent } from './modalBackdrop.component';
import { ModalDirective } from './modal.directive';
import { PositioningService } from '../utils/positioning';
import { ComponentLoaderFactory } from '../utils/component-loader';
import { ModalContainerComponent } from './modalContainer.component';
import { MDBModalService } from './modal.service';

@NgModule({
***REDACTED***
