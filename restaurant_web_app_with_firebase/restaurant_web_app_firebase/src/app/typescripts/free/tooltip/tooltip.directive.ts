import {
  Directive, Input, TemplateRef, ViewContainerRef, Output, EventEmitter,
  Renderer, ElementRef, OnInit, OnDestroy
} from '@angular/core';
import { TooltipContainerComponent } from './tooltip.component';
import { TooltipConfig } from './tooltip.service';
import { ComponentLoaderFactory, ComponentLoader } from '../utils/component-loader';
import { OnChange } from '../utils/decorators';

@Directive({
***REDACTED***
