import { AfterViewInit, Component, ChangeDetectionStrategy, HostBinding  } from '@angular/core';
import { TooltipConfig } from './tooltip.service';
import { isBs3 } from '../utils/ng2-bootstrap-config';

@Component({
  selector: 'mdb-tooltip-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line
  host: {
    '[class]': '"tooltip-fadeIn tooltip in tooltip-" + placement'
***REDACTED***
