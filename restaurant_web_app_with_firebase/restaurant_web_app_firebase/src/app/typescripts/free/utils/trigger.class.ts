/**
 * @copyright Valor Software
 * @copyright Angular ng-bootstrap team
 */

 export class Trigger {
   public open: string;
   public close?: string;

   public constructor(open: string, close?: string) {
***REDACTED***
