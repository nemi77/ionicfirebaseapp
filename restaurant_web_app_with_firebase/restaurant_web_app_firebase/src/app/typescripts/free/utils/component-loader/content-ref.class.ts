/**
 * @copyright Valor Software
 * @copyright Angular ng-bootstrap team
 */

 import { ComponentRef, ViewRef } from '@angular/core';

 export class ContentRef {
   public nodes: any[];
   public viewRef?: ViewRef;
***REDACTED***
