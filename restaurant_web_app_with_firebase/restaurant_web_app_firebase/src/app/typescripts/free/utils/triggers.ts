/**
 * @copyright Valor Software
 * @copyright Angular ng-bootstrap team
 */
 import { Renderer } from '@angular/core';
 import { Trigger } from './trigger.class';

 const DEFAULT_ALIASES = {
   hover: ['mouseover', 'mouseout'],
   focus: ['focusin', 'focusout']
***REDACTED***
