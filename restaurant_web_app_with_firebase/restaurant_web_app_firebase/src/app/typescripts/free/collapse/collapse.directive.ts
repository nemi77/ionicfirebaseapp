// todo: add animations when https://github.com/angular/angular/issues/9947 solved
import {
  Directive, ElementRef, EventEmitter, Input, OnInit, Output,
  Renderer
} from '@angular/core';

@Directive({
  selector: '[mdbCollapse]',
  exportAs: 'bs-collapse',
  /* tslint:disable-next-line */
***REDACTED***
