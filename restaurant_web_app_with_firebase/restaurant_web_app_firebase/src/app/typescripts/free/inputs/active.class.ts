import {Directive, ElementRef,  Input, HostListener, Renderer, AfterViewInit} from '@angular/core';

@Directive({
  selector: '[mdbActive]'
})

export class ActiveDirective implements AfterViewInit {

  @Input() public mdbActive: ActiveDirective;
  public el: ElementRef = null;
***REDACTED***
