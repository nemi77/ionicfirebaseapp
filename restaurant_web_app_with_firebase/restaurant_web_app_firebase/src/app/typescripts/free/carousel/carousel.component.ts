import { Component, Input, OnDestroy, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';

  import { isBs3, LinkedList } from '../utils';
  import { SlideComponent } from './slide.component';
  import { CarouselConfig } from './carousel.config';

  export enum Direction {UNKNOWN, NEXT, PREV}

/**
* Base element to create carousel
***REDACTED***
