import { Component, HostBinding, OnDestroy, Input, OnInit, ElementRef } from '@angular/core';

import { CarouselComponent } from './carousel.component';

@Component({
  selector: 'mdb-slide',
  template: `
  <ng-content></ng-content>
  `
})
***REDACTED***
