// free
import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { DeepModule } from './inputs/';
import { ButtonsModule } from './buttons';
import { RippleModule } from './ripple';
import { ActiveModule } from './inputs';
import { NavbarModule } from './navbars';
import { BsDropdownModule } from './dropdown';
import { CarouselModule } from './carousel/';
import { MDBChartsModule } from './charts/';
***REDACTED***
