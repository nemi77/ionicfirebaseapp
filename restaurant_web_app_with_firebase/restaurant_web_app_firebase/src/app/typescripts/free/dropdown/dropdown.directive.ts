import { Directive, ElementRef, EmbeddedViewRef, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output, Renderer,
  ViewContainerRef } from '@angular/core';
  import { Subscription } from 'rxjs/Subscription';
  import 'rxjs/add/operator/filter';
  import { ComponentLoader, ComponentLoaderFactory } from '../utils/component-loader/index';

  import { BsDropdownConfig } from './dropdown.config';
  import { BsDropdownContainerComponent } from './dropdown-container.component';
  import { BsDropdownState } from './dropdown.state';
  import { BsComponentRef } from '../utils/component-loader/bs-component-ref.class';
***REDACTED***
