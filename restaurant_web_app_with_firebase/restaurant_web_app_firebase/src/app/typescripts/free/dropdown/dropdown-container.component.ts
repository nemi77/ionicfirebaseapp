import { ChangeDetectionStrategy, Component, OnDestroy, HostBinding } from '@angular/core';
import { BsDropdownState } from './dropdown.state';

@Component({
  selector: 'mdb-dropdown-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
  <div [class.dropup]="direction === 'up'"
  [class.dropdown]="direction === 'down'"
  [class.show]="isOpen"
***REDACTED***
