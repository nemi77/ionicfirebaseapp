import { EventEmitter, Injectable } from '@angular/core';
import { BsComponentRef } from '../utils/component-loader/bs-component-ref.class';

@Injectable()
export class BsDropdownState {
  direction: 'down' | 'up' = 'down';
  autoClose: boolean;
  isOpenChange = new EventEmitter<boolean>();
  isDisabledChange = new EventEmitter<boolean>();
  toggleClick = new EventEmitter<boolean>();
***REDACTED***
