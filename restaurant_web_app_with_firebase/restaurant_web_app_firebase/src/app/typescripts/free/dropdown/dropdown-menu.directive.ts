import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { BsDropdownState } from './dropdown.state';

@Directive({
  selector: '[mdbDropdownMenu],[dropdownMenu]',
  exportAs: 'bs-dropdown-menu'
})
export class BsDropdownMenuDirective {
  constructor(_state: BsDropdownState,
    _viewContainer: ViewContainerRef,
***REDACTED***
