import { Directive, ElementRef, HostBinding, HostListener, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { BsDropdownState } from './dropdown.state';

@Directive({
  selector: '[mdbDropdownToggle],[dropdownToggle]',
  exportAs: 'bs-dropdown-toggle'
})
export class BsDropdownToggleDirective implements OnDestroy {
***REDACTED***
