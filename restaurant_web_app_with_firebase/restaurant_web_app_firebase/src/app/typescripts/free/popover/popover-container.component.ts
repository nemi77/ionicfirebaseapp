import { ChangeDetectionStrategy, Input, Component, HostBinding, OnInit } from '@angular/core';
import { PopoverConfig } from './popover.config';
import { isBs3 } from '../utils/ng2-bootstrap-config';

@Component({
 selector: 'mdb-popover-container',
 changeDetection: ChangeDetectionStrategy.OnPush,
 template: `
 <h3 class="popover-header" *ngIf="title">{{title}}</h3>
 <div class="popover-body">
***REDACTED***
