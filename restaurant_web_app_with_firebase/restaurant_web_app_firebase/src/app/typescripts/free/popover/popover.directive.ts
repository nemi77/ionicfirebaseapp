import { Directive, Input, Output, EventEmitter, OnInit, OnDestroy, Renderer, ElementRef, TemplateRef,
  ViewContainerRef } from '@angular/core';
  import { PopoverConfig } from './popover.config';
  import { ComponentLoaderFactory, ComponentLoader } from '../utils/component-loader';
  import { PopoverContainerComponent } from './popover-container.component';

/**
 * A lightweight, extensible directive for fancy popover creation.
 */
 @Directive({selector: '[mdbPopover]', exportAs: 'bs-mdbPopover'})
***REDACTED***
