'use strict';
import { AfterViewChecked, Component, Input, Output, EventEmitter, OnInit, ViewChild,
 forwardRef, AfterViewInit, ElementRef } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

import { MdbCompleterDirective } from '../directives/completer.directive';
import { CompleterData } from '../services/completer-data.service';
import { CompleterService } from '../services/completer.service';
import { CompleterItem } from './completer-item.component';
import { MAX_CHARS, MIN_SEARCH_LENGTH, PAUSE, TEXT_SEARCHING, TEXT_NO_RESULTS } from '../globals';
***REDACTED***
