'use strict';
import {Component, Input, OnInit} from '@angular/core';

export interface MatchPart {
  isMatch: boolean;
  text: string;
}

@Component({
  selector: 'mdb-completer-list-item',
***REDACTED***
