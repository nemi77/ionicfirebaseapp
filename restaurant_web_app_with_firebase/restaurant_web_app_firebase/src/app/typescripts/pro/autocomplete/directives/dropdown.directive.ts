import { AfterViewInit, Directive, ElementRef, Host, HostListener, OnDestroy, OnInit } from '@angular/core';

import { CompleterItem } from '../components/completer-item.component';
import { MdbCompleterDirective, CompleterDropdown } from './completer.directive';


export interface CtrRowElement {
  setHighlighted(selected: boolean): void;
  getNativeElement(): any;
  getDataItem(): CompleterItem;
***REDACTED***
