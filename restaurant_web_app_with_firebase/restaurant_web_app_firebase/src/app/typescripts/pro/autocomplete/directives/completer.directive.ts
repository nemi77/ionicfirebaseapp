import { Directive, EventEmitter, Output } from '@angular/core';

import { CompleterItem } from '../components/completer-item.component';

export interface CompleterList {
  search(term: string): void;
  open(): void;
  isOpen(open: boolean): void;
  clear(): void;
}
***REDACTED***
