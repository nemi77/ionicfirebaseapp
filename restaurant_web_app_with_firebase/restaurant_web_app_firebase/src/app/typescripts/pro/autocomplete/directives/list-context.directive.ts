import 'rxjs/add/observable/timer';
import { ChangeDetectorRef, Directive, Host, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


import { MdbCompleterDirective, CompleterList } from './completer.directive';
import { CompleterData } from '../services/completer-data.service';
import { CompleterItem } from '../components/completer-item.component';
import { MIN_SEARCH_LENGTH, PAUSE, CLEAR_TIMEOUT, isNil } from '../globals';
***REDACTED***
