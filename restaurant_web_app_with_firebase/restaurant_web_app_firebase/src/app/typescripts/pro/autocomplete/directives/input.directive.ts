import { Directive, ElementRef, EventEmitter, Host, HostListener, Input, Output } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { CompleterItem } from '../components/completer-item.component';
import { MdbCompleterDirective } from './completer.directive';
import { isNil } from '../globals';


***REDACTED***
