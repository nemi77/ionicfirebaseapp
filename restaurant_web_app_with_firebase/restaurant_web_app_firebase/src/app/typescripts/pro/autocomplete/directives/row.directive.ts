import { Directive, ElementRef, Host, HostListener, Input, Renderer, OnInit } from '@angular/core';

import { CompleterItem } from '../components/completer-item.component';
import { MdbDropdownDirective, CtrRowElement, CtrRowItem } from './dropdown.directive';

@Directive({
  selector: '[mdbRow]',
})
export class MdbRowDirective implements CtrRowElement, OnInit {

***REDACTED***
