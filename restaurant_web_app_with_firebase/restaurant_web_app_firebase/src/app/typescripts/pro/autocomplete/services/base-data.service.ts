import { Subject } from 'rxjs/Subject';

import { CompleterItem } from '../components/completer-item.component';
import { CompleterData } from './completer-data.service';
import { isNil } from '../globals';

export abstract class CompleterBaseData extends Subject<CompleterItem[]> implements CompleterData {


  protected _searchFields: string;
***REDACTED***
