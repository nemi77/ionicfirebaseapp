import {Http} from '@angular/http';

import {LocalData} from './local-data.service';
import {RemoteData} from './remote-data.service';


export function localDataFactory () {
  return () => {
    return new LocalData();
  };
***REDACTED***
