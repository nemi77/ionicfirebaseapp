import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { CompleterBaseData } from './base-data.service';
import { CompleterItem } from '../components/completer-item.component';

export class RemoteData extends CompleterBaseData {
  private _remoteUrl: string;
***REDACTED***
