import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CompleterBaseData } from './base-data.service';
import { CompleterItem } from '../components/completer-item.component';

@Injectable()
export class LocalData extends CompleterBaseData {

  private _data: any[];
***REDACTED***
