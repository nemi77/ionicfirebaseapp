export abstract class EasingLogic {
  /**
  * Examples may be found at https://github.com/gdsmith/jquery.easing/blob/master/jquery.easing.js
  * or http://gizma.com/easing/
  * @param t current time
  * @param b beginning value
  * @param c change In value
  * @param d duration
  */
  public abstract ease(t: number, b: number, c: number, d: number): number;
***REDACTED***
