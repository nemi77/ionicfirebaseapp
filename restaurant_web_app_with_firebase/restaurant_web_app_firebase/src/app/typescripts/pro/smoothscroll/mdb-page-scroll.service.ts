import {Injectable, isDevMode} from '@angular/core';

import {PageScrollConfig} from './mdb-page-scroll.config';
import {PageScrollInstance, InterruptReporter} from './mdb-page-scroll.instance';
import {PageScrollUtilService as Util} from './mdb-page-scroll-util.service';

@Injectable()
export class PageScrollService {

  private static instanceCounter = 0;
***REDACTED***
