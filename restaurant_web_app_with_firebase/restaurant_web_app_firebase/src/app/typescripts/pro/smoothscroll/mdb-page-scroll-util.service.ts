/**
 * Created by sebastianfuss on 02.09.16.
 */

export class PageScrollUtilService {

  /**
   * Util method to check whether a given variable is either undefined or null
   * @param variable
   * @returns {boolean} true the variable is undefined or null
***REDACTED***
