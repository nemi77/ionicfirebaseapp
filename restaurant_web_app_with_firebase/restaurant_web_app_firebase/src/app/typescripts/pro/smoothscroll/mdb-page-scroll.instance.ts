/**
* Created by sebastianfuss on 29.08.16.
*/

import {EventEmitter} from '@angular/core';

import {EasingLogic, PageScrollConfig, PageScrollTarget, PageScrollingViews} from './mdb-page-scroll.config';
import {PageScrollUtilService as Util} from './mdb-page-scroll-util.service';

/**
***REDACTED***
