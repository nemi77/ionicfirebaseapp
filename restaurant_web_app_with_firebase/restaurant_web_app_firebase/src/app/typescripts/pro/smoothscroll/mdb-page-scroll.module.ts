/**
* Created by sebastianfuss on 03.09.16.
*/

import {CommonModule} from '@angular/common';
import {NgModule, ModuleWithProviders} from '@angular/core';

import {PageScrollService} from './mdb-page-scroll.service';
import {PageScrollDirective} from './mdb-page-scroll.directive';

***REDACTED***
