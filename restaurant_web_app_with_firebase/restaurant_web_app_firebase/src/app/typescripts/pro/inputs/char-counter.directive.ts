import { OnInit, Directive, ElementRef, Renderer, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[mdbCharCounter]'
})


export class CharCounterDirective implements OnInit {

  @Input() public length = 20;
***REDACTED***
