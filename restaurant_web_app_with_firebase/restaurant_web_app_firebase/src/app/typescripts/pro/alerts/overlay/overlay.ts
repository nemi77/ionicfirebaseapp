import { ComponentFactoryResolver, Injectable, ApplicationRef } from '@angular/core';
import { DomPortalHost } from '../portal/dom-portal-host';
import { OverlayRef } from './overlay-ref';

import { OverlayContainer } from './overlay-container';
import { ToastContainerDirective } from '../toast/toast.directive';


/**
 * Service to create Overlays. Overlays are dynamically added pieces of floating UI, meant to be
***REDACTED***
