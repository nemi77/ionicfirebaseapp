import {ComponentRef} from '@angular/core';
import {BasePortalHost, ComponentPortal} from '../portal/portal';

/**
 * Reference to an overlay that has been created with the Overlay service.
 * Used to manipulate or dispose of said overlay.
 */
export class OverlayRef {
  constructor(
      private _portalHost: BasePortalHost) { }
***REDACTED***
