import {
  ViewContainerRef,
  ComponentRef,
  Injector
} from '@angular/core';

export interface ComponentType<T> {
  new (...args: any[]): T;
}

***REDACTED***
