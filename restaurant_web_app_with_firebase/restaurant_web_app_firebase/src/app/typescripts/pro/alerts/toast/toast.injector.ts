import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { OverlayRef } from '../overlay/overlay-ref';
import { ToastPackage } from './toast.config';

/**
 * Reference to a toast opened via the Toast service.
 */
***REDACTED***
