import { Injectable } from '@angular/core';
import { IMyLocales, IMyOptions } from '../interfaces/index';

@Injectable()
export class LocaleService {
  private locales: IMyLocales = {
    'en': {
      dayLabelsFull: {
        su: 'Sunday',
        mo: 'Monday',
***REDACTED***
