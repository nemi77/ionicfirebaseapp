import { IMyDayLabels } from './dayLabels.interface';
import { IMyMonthLabels } from './monthLabels.interface';
import { IMyDate } from './date.interface';
import { IMyDateRange } from './dateRange.interface';
import { IMyMarkedDates } from './markedDates.interface';
import { IMyMarkedDate } from './markedDate.interface';

export interface IMyOptions {
  dayLabelsFull?: IMyDayLabels;
  dayLabels?: IMyDayLabels;
***REDACTED***
