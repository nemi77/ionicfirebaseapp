import { Directive, ElementRef, Renderer, Input, HostListener } from '@angular/core';
import { IMyInputAutoFill } from '../interfaces/inputAutofill.interface';

enum KeyCode {backspace = 8, delete = 46}

@Directive({
  selector: '[mdbInputAutoFill]'
})

export class InputAutoFillDirective {
***REDACTED***
