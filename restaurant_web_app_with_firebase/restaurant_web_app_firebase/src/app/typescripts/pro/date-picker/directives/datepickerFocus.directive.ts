import { Directive, ElementRef, Renderer, AfterViewInit, Input } from '@angular/core';

@Directive({
  selector: '[mdbDpFocus]'
})

export class FocusDirective implements AfterViewInit {
  @Input() value: string;

  constructor(private el: ElementRef, private renderer: Renderer) {}
***REDACTED***
