import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { Ng2CompleterModule } from './autocomplete';
import { CardsModule } from './cards/';
import { MDBUploaderModule } from './file-input/';
import { MaterialChipsModule } from './tags/';
import { ProgressBars } from './progressbars/';
import { TabsModule } from './tabs-pills/';
import { SelectModule } from './material-select/';
import { MDBDatePickerModule } from './date-picker/';
***REDACTED***
