import { Component, ElementRef, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';

declare var EasyPieChart: any;

@Component({
  selector: 'mdb-easy-pie-chart',
  template: '<div>Loading</div>'
})
export class EasyPieChartComponent implements OnInit, OnChanges {
  @Input('percent') percent: any;
***REDACTED***
