
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mdb-simple-chart',
  templateUrl: './chart-simple.component.html',
  styles: []
})
export class SimpleChartComponent implements OnInit {
  @Input('percent') percent: any;
***REDACTED***
