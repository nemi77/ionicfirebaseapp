import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SqueezeBoxComponent} from './components/squeezebox';
import {SBItemComponent} from './components/sb-item';
import {SBItemHeadComponent} from './components/sb-item.head';
import {SBItemBodyComponent} from './components/sb-item.body';

export const SQUEEZEBOX_COMPONENTS = [SqueezeBoxComponent, SBItemComponent, SBItemHeadComponent, SBItemBodyComponent];

***REDACTED***
