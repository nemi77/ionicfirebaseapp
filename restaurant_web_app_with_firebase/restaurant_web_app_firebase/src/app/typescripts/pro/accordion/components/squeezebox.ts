import {Component, Input, ContentChildren, QueryList, forwardRef} from '@angular/core';
import {SBItemComponent} from './sb-item';
import {sbConfig} from './sb.config';

@Component({
  exportAs: 'squeezebox',
  selector: 'mdb-squeezebox',
  templateUrl: 'squeezebox.html'
})
export class SqueezeBoxComponent {
***REDACTED***
