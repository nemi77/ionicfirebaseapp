import {Component, ElementRef, Renderer, ViewChild} from '@angular/core';

@Component({
  exportAs: 'sbItemBody',
  selector: 'mdb-item-body',
  templateUrl: 'sb-item.body.html'
})
export class SBItemBodyComponent {

  public height = '0';
***REDACTED***
