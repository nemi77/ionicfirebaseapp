import {Component, ContentChild, Input, AfterViewInit} from '@angular/core';
import {SBItemBodyComponent} from './sb-item.body';
import {sbConfig} from './sb.config';

@Component({
  exportAs: 'sbItem',
  selector: 'mdb-item',
  templateUrl: 'sb-item.html'
})
export class SBItemComponent implements AfterViewInit {
***REDACTED***
