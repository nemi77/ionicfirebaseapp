import { Directive, ElementRef, HostListener } from '@angular/core';
@Directive({
  selector: '[mdbRippleRadius]'
})
export class RippleDirective {
  el: ElementRef;

  constructor(el: ElementRef) {
    this.el = el;
  }
***REDACTED***
