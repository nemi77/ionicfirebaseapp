import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[mdbNgTransclude]'
})
export class NgTranscludeDirective {
  public viewRef: ViewContainerRef;

  protected _viewRef: ViewContainerRef;
  protected _ngTransclude: TemplateRef<any>;
***REDACTED***
