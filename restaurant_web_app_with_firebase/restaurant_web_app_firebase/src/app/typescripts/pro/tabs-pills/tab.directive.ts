import { Directive, EventEmitter, HostBinding, Input, Output, TemplateRef, ElementRef, OnInit } from '@angular/core';
import { TabsetComponent } from './tabset.component';

@Directive({selector: 'mdb-tab, [mdbTab]'})
export class TabDirective implements OnInit {
  /** tab header text */
  @Input() public heading: string;
  /** if true tab can not be activated */
  @Input() public disabled: boolean;
  /** if true tab can be removable, additional button will appear */
***REDACTED***
