import { Component, Output, EventEmitter, ViewChildren, HostBinding, Input, OnDestroy, OnInit, ElementRef } from '@angular/core';

import { TabDirective } from './tab.directive';
import { TabsetConfig } from './tabset.config';

import { RippleDirective } from './ripple-effect.component';
// todo: add active event to tab
// todo: fix? mixing static and dynamic tabs position tabs in order of creation
@Component({
  selector: 'mdb-tabset',
***REDACTED***
