import { trigger, state, style, transition, animate } from '@angular/core';

// SideNav
export const slideIn: any =  trigger('slideIn', [
  state('inactive', style({opacity: 0, transform: 'translateX(-300%)'})),
  state('active',   style({opacity: 1, transform: 'translateX(0)'})),
  transition('inactive => active', animate('500ms ease')),
  transition('active => inactive', animate('500ms ease')),
]);

***REDACTED***
