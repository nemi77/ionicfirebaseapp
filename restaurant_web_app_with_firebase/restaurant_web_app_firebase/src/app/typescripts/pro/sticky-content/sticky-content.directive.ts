'use strict';

import { Directive, ElementRef, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { computedStyle } from './computed.style';

@Directive({
  selector: '[mdbSticky]'
})

export class MdbStickyDirective implements OnDestroy, AfterViewInit {
***REDACTED***
