import { Component, Input, ElementRef,  AfterViewInit } from '@angular/core';

@Component({
    selector: 'mdb-spinner',
    templateUrl: 'progress-spinner.component.html'
})
export class ProgressSpinnerComponent implements AfterViewInit {
    el: ElementRef;
    addClass: String = 'spinner-blue-only';
    @Input() spinnerType = '';
***REDACTED***
