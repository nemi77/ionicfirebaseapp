import { Component, Input } from '@angular/core';
import { ProgressbarConfigComponent } from './progressbar.config.component';

@Component({
  selector: 'mdb-progressbar',
  templateUrl: './progressbar.component.html'
})
export class ProgressbarComponent {
    /** if `true` changing value of progress bar will be animated (note: not supported by Bootstrap 4) */
  @Input() public animate: boolean;
***REDACTED***
