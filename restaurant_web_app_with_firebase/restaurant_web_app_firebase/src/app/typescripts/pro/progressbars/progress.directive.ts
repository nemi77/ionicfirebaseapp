import { Directive, HostBinding, Input } from '@angular/core';

import { BarComponent } from './bar.component';

// todo: progress element conflict with bootstrap.css
// todo: need hack: replace host element with div
@Directive({selector: 'mdbProgress, [mdbProgress]'})
export class ProgressDirective  {
    /** if `true` changing value of progress bar will be animated (note: not supported by Bootstrap 4) */
  @Input() public animate: boolean;
***REDACTED***
