import {Option} from './option';
import {IOption} from './option-interface';
import {Diacritics} from './diacritics';

export class OptionList {

  private _options: Array<Option>;

  /* Consider using these for performance improvement. */
  // private _selection: Array<Option>;
***REDACTED***
