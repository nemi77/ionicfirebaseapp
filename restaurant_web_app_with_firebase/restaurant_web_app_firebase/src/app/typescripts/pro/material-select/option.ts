import {IOption} from './option-interface';

export class Option {

  wrappedOption: IOption;

  disabled: boolean;
  highlighted: boolean;
  selected: boolean;
  shown: boolean;
***REDACTED***
