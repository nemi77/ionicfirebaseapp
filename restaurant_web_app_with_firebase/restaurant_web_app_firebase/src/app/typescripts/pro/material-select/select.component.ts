import {Component, Input, OnChanges, OnInit, Output, EventEmitter, ExistingProvider, ViewChild,
        ViewEncapsulation, forwardRef, ElementRef, HostListener} from '@angular/core';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/forms';
import {SelectDropdownComponent} from './select-dropdown.component';
import {IOption} from './option-interface';
import {Option} from './option';
import {OptionList} from './option-list';

export const SELECT_VALUE_ACCESSOR: ExistingProvider = {
  provide: NG_VALUE_ACCESSOR,
***REDACTED***
