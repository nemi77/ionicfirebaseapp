import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { OrdersService } from '../orders/orders.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-table-booking',
  templateUrl: './table-booking.component.html',
***REDACTED***
