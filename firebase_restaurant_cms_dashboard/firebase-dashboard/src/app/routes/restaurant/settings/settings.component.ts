import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { cloudinarUpload } from '../../../firebase.config';

@Component({
***REDACTED***
