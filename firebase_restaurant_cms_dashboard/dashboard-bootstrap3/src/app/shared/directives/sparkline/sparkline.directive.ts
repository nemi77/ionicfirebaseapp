import { OnInit, OnDestroy, Directive, Input, ElementRef } from '@angular/core';
declare var $: any;

@Directive({
    selector: '[sparkline]'
})
export class SparklineDirective implements OnInit, OnDestroy {

    @Input() sparkline;

***REDACTED***
