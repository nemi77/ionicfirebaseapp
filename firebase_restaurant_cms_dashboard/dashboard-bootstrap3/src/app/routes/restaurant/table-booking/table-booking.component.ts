import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/Operator/map';
import { ToastrService } from 'ngx-toastr';
import { OrdersService } from '../orders/orders.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-table-booking',
  templateUrl: './table-booking.component.html',
***REDACTED***
