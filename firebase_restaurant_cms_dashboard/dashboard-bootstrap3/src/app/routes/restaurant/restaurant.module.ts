import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
//import {ColorPickerModule, ColorPickerService} from 'angular2-color-picker/lib';
import {CustomFormsModule} from 'ng2-validation';
import {SharedModule} from '../../shared/shared.module';

import {CategoriesComponent} from './categories/categories.component';
import {AddCategoriesComponent} from './categories/add-categories/add-categories.component';
import {MenuItemsComponent} from './menu-items/menu-items.component';
import {AddItemComponent} from './menu-items/add-item/add-item.component';
***REDACTED***
