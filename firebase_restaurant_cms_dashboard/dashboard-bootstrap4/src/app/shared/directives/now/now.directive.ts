import { OnInit, OnDestroy, Directive, Input, ElementRef } from '@angular/core';
import * as moment from 'moment';

@Directive({
    selector: '[now]'
})
export class NowDirective implements OnInit, OnDestroy {

    @Input() format;
    intervalId;
***REDACTED***
