import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { cloudinarUpload } from '../../../firebase.config';

@Component({
  selector: 'app-settings',
***REDACTED***
