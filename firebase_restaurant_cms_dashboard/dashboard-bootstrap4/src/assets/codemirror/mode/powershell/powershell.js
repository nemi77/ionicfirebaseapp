// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  'use strict';
  if (typeof exports == 'object' && typeof module == 'object') // CommonJS
    mod(require('codemirror'));
  else if (typeof define == 'function' && define.amd) // AMD
    define(['codemirror'], mod);
  else // Plain browser env
***REDACTED***
