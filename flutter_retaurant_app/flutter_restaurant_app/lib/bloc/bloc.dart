import 'dart:async';
import 'validator.dart';

class Bloc extends Object with Validators {
  final _email = StreamController<String>();
  final _password = StreamController<String>();


  //Add data to stream
  Stream<String> get email => _email.stream.transform(validateEmail);
***REDACTED***
