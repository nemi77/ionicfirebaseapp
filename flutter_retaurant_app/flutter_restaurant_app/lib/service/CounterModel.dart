import 'package:flutter/cupertino.dart';
import 'package:restaurant/common/common.dart';

class CounterModel with ChangeNotifier {
  int cartCounter = 0;

  getCounter() async {
    await Common.getCart().then((onValue) {
      if (onValue != null) {
        cartCounter = onValue['cart'].length;
***REDACTED***
