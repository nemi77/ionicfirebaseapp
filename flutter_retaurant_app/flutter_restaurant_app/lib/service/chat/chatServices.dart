import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../common/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

postChat(txt, sender, receiver, image) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String authToken = prefs.getString('token');
  var msgData = {
    'message': txt,
***REDACTED***
