import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../../common/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

fetchOfferItems() async {
  return await http.get(baseUrl + 'api/menuitems/offer/available/', headers: {
    'Content-Type': 'application/json',
  });
***REDACTED***
