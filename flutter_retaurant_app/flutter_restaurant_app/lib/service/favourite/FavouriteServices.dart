import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../common/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

addFavouriteItem(userReaction, menuItem) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var body = {
    'userReaction': userReaction,
    'menuItem': menuItem,
***REDACTED***
