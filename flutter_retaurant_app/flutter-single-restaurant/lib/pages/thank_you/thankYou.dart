import 'package:flutter/material.dart';
import '../home/restaurantHome.dart';
import '../../styles/styles.dart';
import 'package:restaurant/service/localizations.dart' show MyLocalizations;
import 'dart:ui';

class ThankYou extends StatefulWidget {
  static String tag = 'thank-you';
  final Map<String, Map<String, String>> localizedValues;
  final String locale;
***REDACTED***
