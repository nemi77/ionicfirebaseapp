import 'package:flutter/material.dart';
import '../../styles/styles.dart';
import '../../service/contact/contactService.dart';
import '../../pages/home/restaurantHome.dart';
import 'package:restaurant/service/localizations.dart' show MyLocalizations;

class Contact extends StatefulWidget {
  static String tag = "contact";
  Contact({Key key, this.locale, this.localizedValues}) : super(key: key);
  final Map<String, Map<String, String>> localizedValues;
***REDACTED***
