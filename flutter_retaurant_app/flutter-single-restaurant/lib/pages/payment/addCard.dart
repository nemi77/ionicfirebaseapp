import 'package:flutter/material.dart';
import '../../styles/styles.dart';

class AddCard extends StatefulWidget {
  AddCard({Key key, this.locale, this.localizedValues}) : super(key: key);
  final Map<String, Map<String, String>> localizedValues;
  final String locale;
  @override
  _AddCardState createState() => _AddCardState();
}
***REDACTED***
