import 'package:flutter/material.dart';
import '../../styles/styles.dart';
import 'login.dart';
import '../../service/auth/auth.dart';
import 'package:restaurant/service/localizations.dart' show MyLocalizations;

class ResetPassword extends StatefulWidget {
  static String tag = 'reset-password';
  ResetPassword({Key key, this.locale, this.localizedValues}) : super(key: key);
  final Map<String, Map<String, String>> localizedValues;
***REDACTED***
