import 'package:flutter/material.dart';

// final primary = const Color(0xFF4bc08a);
final primary = const Color(0xFFFF3131);
final primaryLight = const Color(0xFF85c1a6);
final primaryLighter = const Color(0xFFedefaad);
final danger = const Color(0xFFcb202d);
final dark = const Color(0xFF040C0E);
final darker = const Color(0xFF483E3E);
final darkLight = const Color(0xFF132226);
***REDACTED***
