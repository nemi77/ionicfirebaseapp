import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Common {
  // save token on storage
  static Future<bool> setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('token', token);
  }

***REDACTED***
