import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../common/constants.dart';

final JsonDecoder decoder = new JsonDecoder();

fetchOfferItems() async {
  return await http.get(baseUrl + 'api/menuitems/offer/available/', headers: {
    'Content-Type': 'application/json',
  });
***REDACTED***
