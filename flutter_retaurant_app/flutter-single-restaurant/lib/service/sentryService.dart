import 'package:sentry/sentry.dart';

final SentryClient sentry = new SentryClient(
    dsn: "https://11d769cd158d48e18b576b98ad98d0bd@sentry.io/1778433");

class SentryError {
  Future<Null> reportError(dynamic error, dynamic stackTrace) async {
    print('Caught error: $error');

    print('Reporting to Sentry.io...');
***REDACTED***
