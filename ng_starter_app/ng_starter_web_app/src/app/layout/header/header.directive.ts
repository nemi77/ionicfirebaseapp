import { Directive, HostListener } from '@angular/core';
@Directive({
    selector:'[sidebarToggleDirective]'
})
export class HeaderDirective {
    @HostListener('click',['$event']) toggle(event:any){
      event.preventDefault();
        document.querySelector('body').classList.toggle('sidebar-open');
        document.querySelector('.navbar-fixed').classList.toggle('sidebar-open');
    }
***REDACTED***
