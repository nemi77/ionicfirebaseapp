
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AccountModule } from './account/account.module';
import { BlogModule } from './blog/blog.module';
import { ShopModule } from './shop/shop.module';
import { OtherPagesModule } from './other-pages/otherPages.module';
import { CheckoutModule } from './checkout/checkout.module';
***REDACTED***
