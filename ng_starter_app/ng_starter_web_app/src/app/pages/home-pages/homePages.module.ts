
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OwlModule } from 'ng2-owl-carousel';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from './home/home.component';
import { HomeCollectionComponent } from './home-collection/home-collection.component';
import { HomeFeaturedComponent } from './home-featured/home-featured.component';
***REDACTED***
