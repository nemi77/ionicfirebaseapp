import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import {ShopService} from '../shop.services';
import {Router, ActivatedRoute} from '@angular/router';
import * as fromCart from '../../store/cart.reducer';
import * as CartActions from '../../store/cart.action';
@Component({
  selector: 'app-shop-list-ls',
  templateUrl: './shop-list-ls.component.html',
***REDACTED***
