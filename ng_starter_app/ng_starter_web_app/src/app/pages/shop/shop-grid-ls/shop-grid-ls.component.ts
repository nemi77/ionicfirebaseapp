import { Component, OnInit } from '@angular/core';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromCart from '../../store/cart.reducer';
import * as CartActions from '../../store/cart.action';
import { CartService } from '../cart/cart.service';
import {ShopService} from '../shop.services';

@Component({
***REDACTED***
