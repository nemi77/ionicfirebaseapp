import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';
import {Router, ActivatedRoute} from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromCart from '../../store/cart.reducer';
import * as CartActions from '../../store/cart.action'; 
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
***REDACTED***
