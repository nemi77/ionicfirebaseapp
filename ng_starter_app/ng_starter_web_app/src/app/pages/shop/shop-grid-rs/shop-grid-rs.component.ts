import { Component, OnInit } from '@angular/core';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import {ShopService} from '../shop.services';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-shop-grid-rs',
  templateUrl: './shop-grid-rs.component.html',
  styleUrls: ['./shop-grid-rs.component.scss'],
  providers: [NgbAccordionConfig,ShopService]
***REDACTED***
