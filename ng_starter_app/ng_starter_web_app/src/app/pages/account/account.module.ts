
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AccountAddressComponent } from './account-address/account-address.component';
import { AccountLoginComponent } from './account-login/account-login.component';
import { AccountOrdersComponent } from './account-orders/account-orders.component';
import { AccountPasswordRecoveryComponent } from './account-password-recovery/account-password-recovery.component';
***REDACTED***
