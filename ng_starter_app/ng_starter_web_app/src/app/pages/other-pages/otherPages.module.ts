
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AboutComponent } from './about/about.component';
import { Page404Component } from './page-404/page-404.component';
import { OrderTrackingComponent } from './order-tracking/order-tracking.component';
import { SearchResultsComponent } from './search-results/search-results.component';
***REDACTED***
