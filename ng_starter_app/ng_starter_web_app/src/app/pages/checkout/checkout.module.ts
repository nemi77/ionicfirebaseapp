
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlModule } from 'ng2-owl-carousel';

import { CheckoutAddressComponent } from './checkout-address/checkout-address.component';
import { CheckoutCompleteComponent } from './checkout-complete/checkout-complete.component';
import { CheckoutPaymentComponent } from './checkout-payment/checkout-payment.component';
***REDACTED***
