import { Injectable } from '@angular/core';

 @Injectable()
 export class SidebarService {
   activeClass: boolean = false;
   role: string = 'test';
   toggleSidebar() {
     this.activeClass=!this.activeClass;
     return this.activeClass;
   }
***REDACTED***
