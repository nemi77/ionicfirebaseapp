import 'package:flutter/material.dart';
import 'package:todo_app/style/style.dart';
import 'package:todo_app/auth/register.dart';
import 'package:todo_app/auth/reset_password.dart';
import 'package:todo_app/auth/login.dart';
import 'package:todo_app/pages/drawer/landing.dart';
import 'package:todo_app/pages/home/home.dart';
import 'package:todo_app/pages/categories/categories.dart';
import 'package:todo_app/pages/categories/category_expanded.dart';
import 'package:todo_app/pages/add_task/add_task.dart';
***REDACTED***
