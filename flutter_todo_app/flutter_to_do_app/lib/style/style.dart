import 'package:flutter/material.dart';

final primary = const Color(0xFF77BB23);
final secondary = const Color(0xFFFFB70F);
final tertiary = const Color(0xFFFF5752);
final danger = const Color(0xFFcb202d);
final dark = const Color(0xFF040C0E);
final darker = const Color(0xFF483E3E);
final darkLight = const Color(0xFF132226);
final darkLighter = const Color(0xFF525B56);
***REDACTED***
