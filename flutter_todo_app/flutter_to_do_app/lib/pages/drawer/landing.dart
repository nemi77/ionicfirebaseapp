import 'package:flutter/material.dart';
import 'package:todo_app/style/style.dart';
import 'package:todo_app/pages/drawer/drawer.dart';
import 'package:intl/intl.dart';
import 'package:todo_app/pages/home/home.dart';
import 'package:todo_app/pages/categories/categories.dart';
import 'package:todo_app/pages/add_task/add_task.dart';

class Landing extends StatefulWidget {
  static String tag = "landing";
***REDACTED***
