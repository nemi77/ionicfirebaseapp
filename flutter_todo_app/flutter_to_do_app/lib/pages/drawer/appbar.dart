import 'package:flutter/material.dart';
import 'package:todo_app/style/style.dart';
import 'package:intl/intl.dart';

class MyAppBar extends AppBar {
  MyAppBar({Key key, Widget title})
      : super(key: key, title: title, actions: <Widget>[
        IconButton(
          icon: Icon(Icons.more_vert, color: Colors.white,),
          onPressed: () {},
***REDACTED***
