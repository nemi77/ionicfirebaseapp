import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LocationPage } from "./location";
import { AgmCoreModule } from "@agm/core";
import { PipesModule } from "../../app/pipes.module";

@NgModule({
  declarations: [LocationPage],
  imports: [
    IonicPageModule.forChild(LocationPage),
***REDACTED***
