import { Component } from "@angular/core";
import { IonicPage, NavController, ToastController } from "ionic-angular";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFireDatabase } from "@angular/fire/database";
import { map } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-favourite",
  templateUrl: "favourite.html"
***REDACTED***
