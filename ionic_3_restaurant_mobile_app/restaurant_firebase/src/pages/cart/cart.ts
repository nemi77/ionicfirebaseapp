import { Component } from "@angular/core";
import { NavController, IonicPage, ToastController } from "ionic-angular";
import { AlertController, LoadingController } from "ionic-angular";
import { AngularFireDatabase, AngularFireObject } from "@angular/fire/database";

@IonicPage()
@Component({
  selector: "page-cart",
  templateUrl: "cart.html"
})
***REDACTED***
