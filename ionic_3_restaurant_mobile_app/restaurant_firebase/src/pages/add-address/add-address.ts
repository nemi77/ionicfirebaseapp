import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFireDatabase } from "@angular/fire/database";
import { AngularFireAuth } from "@angular/fire/auth";
import { NgForm } from "@angular/forms";

@IonicPage()
@Component({
  selector: "page-add-address",
  templateUrl: "add-address.html"
***REDACTED***
