import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AddressService } from './address.service';

@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
  providers: [AddressService]
})
***REDACTED***
