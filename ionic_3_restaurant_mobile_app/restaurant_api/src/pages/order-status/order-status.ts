import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { OrderStatusService } from './order-status.service';

@IonicPage()
@Component({
  selector: 'page-order-status',
  templateUrl: 'order-status.html',
  providers: [OrderStatusService]
})
***REDACTED***
