import { Component } from '@angular/core';
import { NavController, Events, NavParams, IonicPage, LoadingController, Platform, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { GooglePlus } from '@ionic-native/google-plus';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { LoginService } from './login.service';
import { UserService } from '../../providers/user-service';
import { SocketService } from '../../providers/socket-service';

***REDACTED***
