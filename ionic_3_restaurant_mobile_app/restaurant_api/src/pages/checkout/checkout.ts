import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { CheckoutService } from './checkout.service';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Stripe } from '@ionic-native/stripe';
import { UserService } from '../../providers/user-service';


const payPalEnvironmentSandbox = 'AcgkbqWGamMa09V5xrhVC8bNP0ec9c37DEcT0rXuh7hqaZ6EyHdGyY4FCwQC-fII-s5p8FL0RL8rWPRB';
***REDACTED***
