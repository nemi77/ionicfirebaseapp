import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, IonicPage } from 'ionic-angular';
import { Service } from '../../app/service';
import { Storage } from '@ionic/storage';
import { ProductDetailsService } from './product-details.service';

@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
***REDACTED***
