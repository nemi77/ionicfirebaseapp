import { Injectable, NgZone } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { ConstService } from './const-service';
import { UserService } from './user-service';

@Injectable()
export class SocketService {
    socket: any;
    zone: any;
***REDACTED***
