import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RatingService } from './rating.service';


@IonicPage()
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html',
  providers: [RatingService]
***REDACTED***
