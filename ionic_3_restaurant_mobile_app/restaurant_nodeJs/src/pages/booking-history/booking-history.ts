import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { BookingHistoryService } from './booking-history.service';
import { UserService } from '../../providers/user-service';

@IonicPage()
@Component({
  selector: 'page-booking-history',
  templateUrl: 'booking-history.html',
  providers: [BookingHistoryService]
***REDACTED***
