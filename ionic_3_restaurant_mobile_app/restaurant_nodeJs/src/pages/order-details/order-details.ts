import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, ToastController } from 'ionic-angular';
import { Service } from '../../app/service';
import { OrderDetailsService } from './order-details.service';

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
  providers: [OrderDetailsService]
***REDACTED***
