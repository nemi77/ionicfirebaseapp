import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, IonicPage, LoadingController, ToastController } from 'ionic-angular';
import { OfferService } from './offer.service';


@IonicPage()
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
  providers: [OfferService]
***REDACTED***
