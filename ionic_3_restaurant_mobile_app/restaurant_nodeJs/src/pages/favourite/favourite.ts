import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, ToastController } from 'ionic-angular';
import { UserService } from '../../providers/user-service';
import { FavouriteService } from './favourite.service';


@IonicPage()
@Component({
  selector: 'page-favourite',
  templateUrl: 'favourite.html',
***REDACTED***
