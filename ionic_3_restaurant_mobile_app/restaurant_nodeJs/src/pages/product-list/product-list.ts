import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { Service } from '../../app/service';
import { ProductListService } from './product-list.service';

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
  providers: [Service, ProductListService]
***REDACTED***
