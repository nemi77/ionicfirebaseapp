import { Component } from "@angular/core";
import { IonicPage, AlertController, NavController, PopoverController, ToastController } from "ionic-angular";
import { CartService } from './cart.service';

@IonicPage()
@Component({
	selector: 'page-cart',
	templateUrl: 'cart.html',
	providers: [CartService]
})
***REDACTED***
