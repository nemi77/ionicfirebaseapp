import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController } from 'ionic-angular';
import { NewsService } from './news.service';


@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
  providers: [NewsService]
***REDACTED***
