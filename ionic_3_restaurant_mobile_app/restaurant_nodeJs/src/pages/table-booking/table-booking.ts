import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { BookTableService } from './table-booking.service';


@IonicPage()
@Component({
  selector: 'page-table-booking',
  templateUrl: 'table-booking.html',
  providers: [BookTableService]
***REDACTED***
