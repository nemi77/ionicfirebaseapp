import { Component } from '@angular/core';
import { NavController, IonicPage, LoadingController, ToastController } from 'ionic-angular';
import { CategoryService } from './category.service';


@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
  providers: [CategoryService]
***REDACTED***
