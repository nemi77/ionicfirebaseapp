import 'package:flutter/material.dart';
import 'package:kools/styles/styles.dart' as prefix0;
import '../../styles/styles.dart';
import '../auth/authentication.dart';
import 'package:kools/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import '../../initialize_i18n.dart' show initializeI18n;
import '../../constant.dart' show languages;
import '../../localizations.dart' show MyLocalizations, MyLocalizationsDelegate;
***REDACTED***
