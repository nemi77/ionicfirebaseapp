import 'package:flutter/material.dart';
import '../../styles/styles.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../service/payment-service.dart';
import '../../service/sentry-service.dart';
import '../../initialize_i18n.dart' show initializeI18n;
import '../../constant.dart' show languages;
import '../../localizations.dart' show MyLocalizations, MyLocalizationsDelegate;

SentryError sentryError = new SentryError();
***REDACTED***
