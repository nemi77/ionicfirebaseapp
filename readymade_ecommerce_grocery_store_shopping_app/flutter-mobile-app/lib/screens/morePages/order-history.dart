import 'package:flutter/material.dart';
import 'package:kools/screens/morePages/cancelled-order.dart';
import 'package:kools/screens/morePages/completed-order.dart';
import 'package:kools/screens/morePages/upcoming-order.dart';
import '../../styles/styles.dart';
import '../../initialize_i18n.dart' show initializeI18n;
import '../../constant.dart' show languages;
import '../../localizations.dart'
    show MyLocalizations, MyLocalizationsDelegate;

***REDACTED***
