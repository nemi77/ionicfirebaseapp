import 'package:flutter/material.dart';
import 'package:kools/screens/pages/productDetails.dart';
import 'package:kools/service/product-service.dart';
import '../../service/sentry-service.dart';
import '../../styles/styles.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import '../../initialize_i18n.dart' show initializeI18n;
import '../../constant.dart' show languages;
import '../../localizations.dart' show MyLocalizations, MyLocalizationsDelegate;

***REDACTED***
