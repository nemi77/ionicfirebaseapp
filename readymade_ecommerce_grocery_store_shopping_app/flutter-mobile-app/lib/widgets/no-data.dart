import 'package:flutter/material.dart';

class NoData extends StatefulWidget {
  final String message;
  final Widget icon;
  final double verticalPadding;

  NoData({Key key, this.message, this.icon, this.verticalPadding})
      : super(key: key);

***REDACTED***
