export interface ProductModel {
    title: string;
    description: string;
    imageUrl: string;
    categoryId: string;
    imageId: string;
    type: string;
    variant: Array<VariantModel>;
    subCategoryId?: string;
    filePath?: string;
    sku: string;
};

export interface VariantModel {
    enable: boolean;
    productStock: number;
    unit: string;
    price: number;
}
