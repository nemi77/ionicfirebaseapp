import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';
import { SettingService } from "../settings/setting.service";
import { UtilService } from "../../service/util.service";
import { OrderModel } from "./orders.model";

@Component({
	selector: 'app-orders',
	templateUrl: './orders.component.html',
	styleUrls: ['./orders.component.scss']
})
export class OrderComponent implements OnInit {
	public isLoading = false;
	public orders: Array<OrderModel> = [];
	public page: number = 1;
	public limit: number = 25;
	public total: number = 0;
	public currencySymbol: string = null;
	public orderId: string = null;
	public selectedStatus: string = 'All';

	constructor(
		private utilService: UtilService,
		private orderService: OrderService,
		private settingsService: SettingService
	) {
		this.getAllOrder();
	}

	ngOnInit() {
	}

	// Get all order
	private getAllOrder(): void {
		this.isLoading = true;
		this.orderService.getAll(this.page, this.limit, this.selectedStatus).subscribe((res: any) => {
			this.isLoading = false;
			this.orders = res.response_data || [];
			this.total = res.total || 0;
		}, error => this.isLoading = false);
	}

	// Pagination
	public pageChange(page: number): void {
		this.page = page;
		this.getAllOrder();
	}

	// Update status
	public updateStatus(event, Id): void {
		const update: any = { orderId: Id, status: event.target.value };
		this.isLoading = true;
		this.orderService.updateStatus(Id, update).subscribe((res: any) => {
			this.isLoading = false;
			this.utilService.successMessage(res.response_data);
			this.getAllOrder();
		}, error => this.isLoading = false);
	}

	// Filter order by status
	public filterByStatus(): void {
		this.page = 1;
		this.getAllOrder();
	}
}
