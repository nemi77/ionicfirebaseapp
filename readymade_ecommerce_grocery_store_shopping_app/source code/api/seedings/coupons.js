module.exports = [
   {
      "_id" : "5f2440e89d95c700390caacd",
      "status" : true,
      "description" : " Flat 50% off on your Groceries ",
      "couponCode" : "FLAT50",
      "offerValue" : 20,
      "startDate" : 1596220200000,
      "expiryDate" : 1596479400000,
      "couponType" : "PERCENTAGE"
   },
   {
      "_id" : "5f2441979d95c700390caace",
      "status" : true,
      "description" : " Flat 30 % off on your personal cares ",
      "couponCode" : "FESTIVALSALE",
      "offerValue" : 30,
      "startDate" : 1596405600000,
      "expiryDate" : 1597010400000,
      "couponType" : "PERCENTAGE"
   },
   {
      "_id" : "5f2441e39d95c700390caacf",
      "status" : true,
      "description" : " Flat 50% off on your Household",
      "couponCode" : "SALE 50",
      "offerValue" : 50,
      "startDate" : 1596220200000,
      "expiryDate" : 1596479400000,
      "couponType" : "PERCENTAGE"
   }
]