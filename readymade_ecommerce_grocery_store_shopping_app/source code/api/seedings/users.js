module.exports = [
    {
        "_id" : "5f24242faea52d34bb1308bd",
        "location" : {
            "latitude" : 12.8718,
            "longitude" : 77.6022
        },
        "status" : true,
        "email" : "admin@ionicfirebaseapp.com",
        "password" : "$2b$10$tJoJoew.JM7yx9OngEXOfe7I1hOqLdU72qJ4s4xkKQ1qBXRoLLcbO",
        "role" : "ADMIN",
        "lastName" : "Solution",
        "firstName" : "Pietech",
        "emailVerified" : true,
        "mobileNumber" : "9835041459",
        "language" : "en",
        "salt" : "$2b$10$tJoJoew.JM7yx9OngEXOfe",
        "walletAmount" : 0,
        "playerId" : null
    }, 
    {
        "_id" : "5f24242faea52d34bb1308be",
        "location" : {
            "latitude" : 0,
            "longitude" : 0
        },
        "status" : true,
        "firstName" : "Fire",
        "lastName" : "bases",
        "email" : "user@ionicfirebaseapp.com",
        "password" : "$2b$10$ZUD80ZnthJ38r03jxd8xoOBVTpj2vQD6xBN4jccQh25zwugQ6Hxk2",
        "role" : "USER",
        "mobileNumber" : "0710394088",
        "salt" : "$2b$10$ZUD80ZnthJ38r03jxd8xoO",
        "emailVerified" : true,
        "playerId" : null,
        "language" : "en",
        "walletAmount" : 0
    },
    {
        "_id" : "5f25b0df87dea20039c8287f",
        "mobileNumberVerified" : false,
        "emailVerified" : true,
        "status" : true,
        "firstName" : "Ntish",
        "lastName" : "Kumar",
        "email" : "nitishk1301@gmail.com",
        "password" : "$2b$10$XJ.EaCgzFOGgZZE4yAEE2uS2AMJTX8qK99e/qi7jIIHmaE2PTrKRi",
        "mobileNumber" : "9876543210",
        "role" : "DELIVERY_BOY",
        "salt" : "$2b$10$XJ.EaCgzFOGgZZE4yAEE2u",
        "emailVerificationId" : "c0a7cb20-d422-11ea-9346-e798bcadf9b9",
        "emailVerificationExpiry" : 1596312831442,
        "playerId" : null
    }    
]