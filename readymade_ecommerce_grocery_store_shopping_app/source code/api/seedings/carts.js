module.exports = [
   {
      "_id" : "5f25b06a87dea20039c8286d",
      "productIds" : [
         "5f2506ffdcb97d00397ac13e",
         "5f2505d7dcb97d00397ac133"
      ],
      "couponAmount" : 0,
      "walletAmount" : 0,
      "isOrderLinked" : true,
      "subTotal" : 120,
      "tax" : 24,
      "grandTotal" : 144,
      "deliveryCharges" : 0,
      "userId" : "5f24242faea52d34bb1308be",
      "products" : [
         {
            "_id" : "5f25b06a87dea20039c8286e",
            "productId" : "5f2506ffdcb97d00397ac13e",
            "productName" : "Amul Cheese",
            "unit" : "1 Kg",
            "price" : 70,
            "quantity" : 1,
            "productTotal" : 70,
            "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262133761_original_waldemar-brandt-kPqaqug998Y-unsplash_2x_SMGqBFdXE.png",
            "filePath" : "/1596262133761_original_waldemar-brandt-kPqaqug998Y-unsplash_2x_SMGqBFdXE.png",
            "dealAmount" : 0,
            "dealTotalAmount" : 0,
            "isDealAvailable" : false
         },
         {
            "_id" : "5f25b07e87dea20039c82872",
            "productId" : "5f2505d7dcb97d00397ac133",
            "productName" : "Donughts",
            "unit" : "1 Kg",
            "price" : 50,
            "quantity" : 1,
            "productTotal" : 50,
            "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261832824_original_patrick-fore-ZmH0g1ievTg-unsplash_2x_iXXS9yAOY.png",
            "filePath" : "/1596261832824_original_patrick-fore-ZmH0g1ievTg-unsplash_2x_iXXS9yAOY.png",
            "dealAmount" : 0,
            "dealTotalAmount" : 0,
            "isDealAvailable" : false
         }
      ],
      "taxInfo" : {
         "taxName" : "GST",
         "amount" : 20
      },
      "deliveryAddress" : "5f2424979d95c700390caa97"
   }
]