module.exports = [
   {
      "_id" : "5f2434db9d95c700390caaa4",
      "isDealAvailable" : true,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Apples",
      "description" : "Fresh and healthy",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596208230933_original_1589463352346_original_shelley-pauls-I58f47LRQYM-unsplash_2x_y6MHUE3yK_Yh5U7bA59.jpeg",
      "categoryId" : "5f242e139d95c700390caaa0",
      "imageId" : "5f243468a0af626666fb5c83",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2434db9d95c700390caaa5",
            "productStock" : 100,
            "unit" : "1 Kg",
            "price" : 99
         },
         {
            "enable" : true,
            "_id" : "5f2434db9d95c700390caaa6",
            "productStock" : 150,
            "unit" : "5 Kg",
            "price" : 399
         }
      ],
      "subCategoryId" : "5f2433669d95c700390caaa3",
      "filePath" : "/1596208230933_original_1589463352346_original_shelley-pauls-I58f47LRQYM-unsplash_2x_y6MHUE3yK_Yh5U7bA59.jpeg",
      "sku" : null,
      "categoryName" : "Fruits",
      "subCategoryName" : "Berries",
      "dealId" : "5f2440499d95c700390caacb",
      "dealPercent" : 60,
      "dealType" : "CATEGORY"
   },
   {
      "_id" : "5f2437b09d95c700390caaaf",
      "isDealAvailable" : true,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Jack Daniels",
      "description" : "Liquor (also hard liquor, hard alcohol, spirit, or distilled drink) is an alcoholic drink produced by distillation of grains, fruit, or vegetables that have already gone through alcoholic fermentation.",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596209020744_original_1589462995065_original_photo-1520091276903-2d35a24fab56_2x_6ePhimMY__Hck55lhgi.jpeg",
      "categoryId" : "5f242ca99d95c700390caa9f",
      "imageId" : "5f24377e1005831c01beb926",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2437b09d95c700390caab0",
            "productStock" : 100,
            "unit" : "750 Ml",
            "price" : 1499
         },
         {
            "enable" : true,
            "_id" : "5f2437b09d95c700390caab1",
            "productStock" : 120,
            "unit" : "900 Ml",
            "price" : 1699
         }
      ],
      "subCategoryId" : "5f24371c9d95c700390caaab",
      "filePath" : "/1596209020744_original_1589462995065_original_photo-1520091276903-2d35a24fab56_2x_6ePhimMY__Hck55lhgi.jpeg",
      "sku" : null,
      "categoryName" : "Liquor",
      "subCategoryName" : "Tequila & Mezcal",
      "dealId" : "5f243fad9d95c700390caac8",
      "dealPercent" : 30,
      "dealType" : "PRODUCT"
   },
   {
      "_id" : "5f2436469d95c700390caaa8",
      "isDealAvailable" : true,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Orange",
      "description" : "Oranges are round orange-coloured fruit that grow on a tree which can reach 10 metres Rich in vitamin C",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596208653336_original_1589462640345_original_theme-inn-yi1YB_FubH8-unsplash_2x_6_fkYqhVR_c3PdokERP.jpeg",
      "categoryId" : "5f242e139d95c700390caaa0",
      "imageId" : "5f24360ea0af626666fb5ed3",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2436469d95c700390caaa9",
            "productStock" : 120,
            "unit" : "1 Kg",
            "price" : 89
         },
         {
            "enable" : true,
            "_id" : "5f2436469d95c700390caaaa",
            "productStock" : 140,
            "unit" : "3 Kg",
            "price" : 219
         }
      ],
      "subCategoryId" : "5f24359f9d95c700390caaa7",
      "filePath" : "/1596208653336_original_1589462640345_original_theme-inn-yi1YB_FubH8-unsplash_2x_6_fkYqhVR_c3PdokERP.jpeg",
      "sku" : null,
      "categoryName" : "Fruits",
      "subCategoryName" : "Citrus fruits ",
      "dealId" : "5f2440499d95c700390caacb",
      "dealPercent" : 60,
      "dealType" : "CATEGORY"
   },
   {
      "_id" : "5f250353dcb97d00397ac117",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Cone Icecream",
      "description" : "Tasty and yummy ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261171007_original_brooke-lark-8beTH4VkhLI-unsplash_2x_28sH0ZGLz.png",
      "categoryId" : "5f25026edcb97d00397ac113",
      "imageId" : "5f2503351005831c01bf66f6",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250353dcb97d00397ac118",
            "productStock" : 20,
            "unit" : "1 Piece",
            "price" : 50
         },
         {
            "enable" : true,
            "_id" : "5f250353dcb97d00397ac119",
            "productStock" : 10,
            "unit" : "5 Piece",
            "price" : 250
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261171007_original_brooke-lark-8beTH4VkhLI-unsplash_2x_28sH0ZGLz.png",
      "sku" : "Piece",
      "categoryName" : "Amul Icecream",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250397dcb97d00397ac11b",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Spices",
      "description" : "All types of Indian spices available ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261251742_original_andra-ion-qsPHXC4T2cY-unsplash_2x_suVp3f7R0.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f2503861005831c01bf674e",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250397dcb97d00397ac11c",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 75
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261251742_original_andra-ion-qsPHXC4T2cY-unsplash_2x_suVp3f7R0.png",
      "sku" : "Kilogram",
      "categoryName" : "Uncle Bens ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250404dcb97d00397ac11d",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Cakes",
      "description" : "Tasty and yummy",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261362081_original_brooke-lark-pGM4sjt_BdQ-unsplash_2x_9GULK59yq.png",
      "categoryId" : "5f250773dcb97d00397ac141",
      "imageId" : "5f2503f4a0af626666fc0afa",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250404dcb97d00397ac11e",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 60
         },
         {
            "enable" : true,
            "_id" : "5f250404dcb97d00397ac11f",
            "productStock" : 20,
            "unit" : "2 Kg",
            "price" : 120
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261362081_original_brooke-lark-pGM4sjt_BdQ-unsplash_2x_9GULK59yq.png",
      "sku" : "Kg",
      "categoryName" : "Bakery ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250438dcb97d00397ac120",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Deodrant",
      "description" : "Fragrance",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261413741_original_deo_4hmODLIIy.jpg",
      "categoryId" : "5f25028edcb97d00397ac114",
      "imageId" : "5f250426a0af626666fc0b20",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250438dcb97d00397ac121",
            "productStock" : 20,
            "unit" : "1 Piece",
            "price" : 250
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261413741_original_deo_4hmODLIIy.jpg",
      "sku" : "Piece",
      "categoryName" : "Cosmetics ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f25046bdcb97d00397ac122",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Carrot ",
      "description" : "Root vegetables ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261468003_original_harshal-s-hirve-yNB8niq1qCk-unsplash_2x_d7e_9kUSi.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f25045e1005831c01bf6886",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f25046bdcb97d00397ac123",
            "productStock" : 20,
            "unit" : "1 Kg",
            "price" : 40
         },
         {
            "enable" : true,
            "_id" : "5f25046bdcb97d00397ac124",
            "productStock" : 10,
            "unit" : "2 Kg",
            "price" : 80
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261468003_original_harshal-s-hirve-yNB8niq1qCk-unsplash_2x_d7e_9kUSi.png",
      "sku" : "Kg",
      "categoryName" : "Groceries",
      "subCategoryName" : null
   },
   {
      "_id" : "5f2504a1dcb97d00397ac126",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Indian Spices",
      "description" : "Spicy ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261529845_original_hue12-photography-8rTwokBwz1w-unsplash_2x_Ww5InZziW.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f25049c1005831c01bf6898",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2504a1dcb97d00397ac127",
            "productStock" : 10,
            "unit" : "1 Kg ",
            "price" : 80
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261529845_original_hue12-photography-8rTwokBwz1w-unsplash_2x_Ww5InZziW.png",
      "sku" : "Kg",
      "categoryName" : "Groceries",
      "subCategoryName" : null
   },
   {
      "_id" : "5f2504d5dcb97d00397ac128",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Flavoured Icecream ",
      "description" : "Tasty ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261582934_original_lama-roscu-Wpg3Qm0zaGk-unsplash_2x_Wm1STkqor.png",
      "categoryId" : "5f25026edcb97d00397ac113",
      "imageId" : "5f2504d1a0af626666fc0b9e",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2504d5dcb97d00397ac129",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 90
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261582934_original_lama-roscu-Wpg3Qm0zaGk-unsplash_2x_Wm1STkqor.png",
      "sku" : "Kg",
      "categoryName" : "Amul Icecream",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250502dcb97d00397ac12a",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Tomatoes",
      "description" : "Root vegetables ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261628052_original_lars-blankers-6Z7Ss9jlEL0-unsplash_2x_cCILN_Z77.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f2504fea0af626666fc0bce",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250502dcb97d00397ac12b",
            "productStock" : 20,
            "unit" : "1 Kg",
            "price" : 45
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261628052_original_lars-blankers-6Z7Ss9jlEL0-unsplash_2x_cCILN_Z77.png",
      "sku" : "Kg",
      "categoryName" : "Groceries",
      "subCategoryName" : null
   },
   {
      "_id" : "5f25052cdcb97d00397ac12c",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Lays",
      "description" : "Crunchy and yummy",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261668785_original_lays__1__9Bi2z807i.jpg",
      "categoryId" : "5f250773dcb97d00397ac141",
      "imageId" : "5f250526a0af626666fc0c10",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f25052cdcb97d00397ac12d",
            "productStock" : 50,
            "unit" : "1 Piece",
            "price" : 20
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261668785_original_lays__1__9Bi2z807i.jpg",
      "sku" : "Piece",
      "categoryName" : "Bakery ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250560dcb97d00397ac12e",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Household Matress",
      "description" : "Household ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261707727_original_mattress_CQuZbLEO9.jpg",
      "categoryId" : "5f250238dcb97d00397ac112",
      "imageId" : "5f25054d1005831c01bf691c",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250560dcb97d00397ac12f",
            "productStock" : 10,
            "unit" : "1 Piece",
            "price" : 35000
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261707727_original_mattress_CQuZbLEO9.jpg",
      "sku" : "Piece",
      "categoryName" : "Household",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250591dcb97d00397ac130",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Nescafe Coffee",
      "description" : "Tasty ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261763035_original_nescafe-kofe-kruzhka-kofeinye-ziorna-sovok-otrazhenie-fon_2x_fjgA6pvVK.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f2505851005831c01bf6964",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250591dcb97d00397ac131",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 250
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261763035_original_nescafe-kofe-kruzhka-kofeinye-ziorna-sovok-otrazhenie-fon_2x_fjgA6pvVK.png",
      "sku" : "Kg",
      "categoryName" : "Groceries",
      "subCategoryName" : null
   },
   {
      "_id" : "5f2505d7dcb97d00397ac133",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Donughts",
      "description" : "Tasty and Yummy",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261832824_original_patrick-fore-ZmH0g1ievTg-unsplash_2x_iXXS9yAOY.png",
      "categoryId" : "5f250773dcb97d00397ac141",
      "imageId" : "5f2505cb1005831c01bf69ba",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2505d7dcb97d00397ac134",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 50
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261832824_original_patrick-fore-ZmH0g1ievTg-unsplash_2x_iXXS9yAOY.png",
      "sku" : "Kg",
      "categoryName" : "Bakery ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f25063cdcb97d00397ac135",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Whitewalker",
      "description" : "Liquor",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261897093_original_photo-1520091276903-2d35a24fab56_2x_UyuDewhc9.png",
      "categoryId" : "5f242ca99d95c700390caa9f",
      "imageId" : "5f25060aa0af626666fc0cfa",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f25063cdcb97d00397ac136",
            "productStock" : 10,
            "unit" : "1 Litre",
            "price" : 950
         }
      ],
      "subCategoryId" : "5f24371c9d95c700390caaab",
      "filePath" : "/1596261897093_original_photo-1520091276903-2d35a24fab56_2x_UyuDewhc9.png",
      "sku" : "Litre",
      "categoryName" : "Liquor",
      "subCategoryName" : "Tequila & Mezcal"
   },
   {
      "_id" : "5f250669dcb97d00397ac137",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Pillow",
      "description" : "Household things ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596261980161_original_pillow_9X7aTBfyH.jpg",
      "categoryId" : "5f250238dcb97d00397ac112",
      "imageId" : "5f25065da0af626666fc0d78",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250669dcb97d00397ac138",
            "productStock" : 10,
            "unit" : "1 Piece",
            "price" : 500
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596261980161_original_pillow_9X7aTBfyH.jpg",
      "sku" : "Piece",
      "categoryName" : "Household",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250699dcb97d00397ac13a",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Shampoo",
      "description" : "Hair care ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262028821_original_shampoo_hWYyaYHTl.jpg",
      "categoryId" : "5f25028edcb97d00397ac114",
      "imageId" : "5f25068ea0af626666fc0dbc",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250699dcb97d00397ac13b",
            "productStock" : 10,
            "unit" : "1 Botte",
            "price" : 120
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596262028821_original_shampoo_hWYyaYHTl.jpg",
      "sku" : "Bottle",
      "categoryName" : "Cosmetics ",
      "subCategoryName" : null
   },
   {
      "_id" : "5f2506ccdcb97d00397ac13c",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Tata Tea",
      "description" : "Tasty ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262086522_original_tata-Tea_2x_n8_ntYlWj.png",
      "categoryId" : "5f250210dcb97d00397ac111",
      "imageId" : "5f2506c81005831c01bf6b02",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2506ccdcb97d00397ac13d",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 60
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596262086522_original_tata-Tea_2x_n8_ntYlWj.png",
      "sku" : "Kg",
      "categoryName" : "Groceries",
      "subCategoryName" : null
   },
   {
      "_id" : "5f2506ffdcb97d00397ac13e",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Amul Cheese",
      "description" : "Dairy products",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262133761_original_waldemar-brandt-kPqaqug998Y-unsplash_2x_SMGqBFdXE.png",
      "categoryId" : "5f2502a6dcb97d00397ac115",
      "imageId" : "5f2506f8a0af626666fc0e68",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2506ffdcb97d00397ac13f",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 70
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596262133761_original_waldemar-brandt-kPqaqug998Y-unsplash_2x_SMGqBFdXE.png",
      "sku" : "Kg",
      "categoryName" : "Dairy",
      "subCategoryName" : null
   },
   {
      "_id" : "5f250983dcb97d00397ac150",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Rice Packet",
      "description" : "Groceries ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262772811_original_fruits-_strawberry_NrxQmb4tOu.png",
      "categoryId" : "5f242e139d95c700390caaa0",
      "imageId" : "5f2509761005831c01bf6df0",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250983dcb97d00397ac151",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 40
         }
      ],
      "subCategoryId" : "5f2433669d95c700390caaa3",
      "filePath" : "/1596262772811_original_fruits-_strawberry_NrxQmb4tOu.png",
      "sku" : "Kg",
      "categoryName" : "Fruits",
      "subCategoryName" : "Berries"
   },
   {
      "_id" : "5f2509aedcb97d00397ac152",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Grapes",
      "description" : "Berries ",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262802634_original_fruits-grapes_qbIqr_Zfe.png",
      "categoryId" : "5f242e139d95c700390caaa0",
      "imageId" : "5f250994a0af626666fc10ea",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f2509aedcb97d00397ac153",
            "productStock" : 10,
            "unit" : "1 kg",
            "price" : 80
         }
      ],
      "subCategoryId" : "5f2433669d95c700390caaa3",
      "filePath" : "/1596262802634_original_fruits-grapes_qbIqr_Zfe.png",
      "sku" : "Kg",
      "categoryName" : "Fruits",
      "subCategoryName" : "Berries"
   },
   {
      "_id" : "5f250a4ddcb97d00397ac155",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Chocolate Cake",
      "description" : "Tasty and yummy",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262968572_original_cup_cakes_2x_k2J2NdGRX.png",
      "categoryId" : "5f250773dcb97d00397ac141",
      "imageId" : "5f250a3b1005831c01bf6f5c",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250a4ddcb97d00397ac156",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 100
         }
      ],
      "subCategoryId" : "5f25089bdcb97d00397ac14b",
      "filePath" : "/1596262968572_original_cup_cakes_2x_k2J2NdGRX.png",
      "sku" : "Kg",
      "categoryName" : "Bakery ",
      "subCategoryName" : "Cakes"
   },
   {
      "_id" : "5f250ad4dcb97d00397ac159",
      "isDealAvailable" : false,
      "status" : true,
      "averageRating" : 0,
      "totalRating" : 0,
      "noOfUsersRated" : 0,
      "title" : "Nescafe Cofee powder",
      "description" : "Beverages",
      "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596263108987_original_beverages_eQlaid22L.png",
      "categoryId" : "5f250aa0dcb97d00397ac157",
      "imageId" : "5f250ac6a0af626666fc121e",
      "type" : " ",
      "variant" : [
         {
            "enable" : true,
            "_id" : "5f250ad4dcb97d00397ac15a",
            "productStock" : 10,
            "unit" : "1 Kg",
            "price" : 140
         }
      ],
      "subCategoryId" : null,
      "filePath" : "/1596263108987_original_beverages_eQlaid22L.png",
      "sku" : "Kg",
      "categoryName" : "Beverages",
      "subCategoryName" : null
   }
]