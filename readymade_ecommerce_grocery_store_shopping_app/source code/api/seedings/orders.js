module.exports = [
    {
        "_id" : "5f25b0b187dea20039c8287a",
        "isOrderAssigned" : true,
        "isAcceptedByDeliveryBoy" : true,
        "isWalletUsed" : false,
        "subTotal" : 120,
        "tax" : 24,
        "product" : {
            "title" : "Amul Cheese",
            "imageUrl" : "https://ik.imagekit.io/kplhvthqbi/tr:dpr-auto,tr:w-auto/1596262133761_original_waldemar-brandt-kPqaqug998Y-unsplash_2x_SMGqBFdXE.png"
        },
        "totalProduct" : 2,
        "grandTotal" : 144,
        "deliveryCharges" : 0,
        "couponAmount" : 0,
        "transactionDetails" : {
            "transactionStatus" : null,
            "receiptUrl" : null,
            "transactionId" : null,
            "currency" : null
        },
        "address" : {
            "address" : "262, Muthurayya Swamy Layout, Hulimavu, Bengaluru, Karnataka 560076, India",
            "flatNo" : "23",
            "postalCode" : "560076",
            "addressType" : "HOME",
            "apartmentName" : "Milevska",
            "landmark" : "Near book store",
            "location" : {
                "latitude" : 12.872626277861134,
                "longitude" : 77.6011914894104
            }
        },
        "user" : {
            "firstName" : "Fire",
            "lastName" : "bases",
            "mobileNumber" : "0710394088",
            "email" : "user@ionicfirebaseapp.com"
        },
        "userId" : "5f24242faea52d34bb1308be",
        "paymentType" : "COD",
        "orderStatus" : "DELIVERED",
        "cartId" : "5f25b06a87dea20039c8286d",
        "orderID" : 10001,
        "deliveryDate" : "Sun 2-AUG",
        "deliveryTime" : "9:00 AM to 12:00 PM",
        "usedWalletAmount" : 0,
        "amountRefunded" : 0,
        "currencyCode" : "USD",
        "currencySymbol" : "$",
        "invoiceToken" : "a57b01a0-d422-11ea-9346-e798bcadf9b9",
        "orderFrom" : "WEB_APP",
        "rejectedByDeliveryBoy" : [ ],
        "assignedToId" : "5f25b0df87dea20039c8287f",
        "assignedToName" : "Ntish Kumar"
    }
]
