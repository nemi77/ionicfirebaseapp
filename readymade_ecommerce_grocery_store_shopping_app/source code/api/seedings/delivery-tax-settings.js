module.exports = [
   {
      "_id": "5e85abc2286b3633ddd36608",
      "deliveryCoverage": 8,
      "minimumOrderAmount": 200,
      "minimumOrderAmountToPlaceOrder": 100,
      "taxName": "GST",
      "deliveryType": "fixed",
      "taxType": "included",
      "taxAmount": 20,
      "fixedDeliveryCharges": 8.5,
      "deliveryChargePerKm": 10,
      "paymentMethod": "Both",
      "location": {
         "latitude": 12.8718,
         "longitude": 77.6022
      },
      "store": "5e8f02a81c9d44000078b0f6",
      "__v": 0
   }
]