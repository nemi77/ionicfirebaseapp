import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { WalletDTO, WalletSaveDTO, WalletTransactionType } from './wallet.model';

@Injectable()
export class WalletService {
	constructor(
		@InjectModel('Wallet') private readonly walletModel: Model<any>
	) { }

	public async walletHistory(userId: string): Promise<Array<WalletDTO>> {
		let wallets = await this.walletModel.find({ userId: userId });
		return wallets;
	}

	public async cancelOrder(walletData: WalletSaveDTO): Promise<WalletDTO> {
		walletData.isCredited = true;
		walletData.transactionType = WalletTransactionType.ORDER_CANCELLED;
		let wallet = await this.walletModel.create(walletData);
		return wallet;
	}

	public async madeOrder(walletData: WalletSaveDTO): Promise<WalletDTO> {
		walletData.isCredited = false;
		walletData.transactionType = WalletTransactionType.ORDER_PAYMENT;
		let wallet = await this.walletModel.create(walletData);
		return wallet;
	}
}