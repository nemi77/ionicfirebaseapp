import { Controller, UseGuards, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { CommonResponseModel, ResponseErrorMessage } from '../utils/app.model';
import { WalletService } from './wallet.service';
import { UtilService } from '../utils/util.service';
import { GetUser } from '../utils/jwt.strategy';

@Controller('wallets')
@ApiUseTags('Wallets')
export class WalletController {
	constructor(
		private walletService: WalletService,
		private utilService: UtilService
	) {
	}

	@Get('/history')
	@ApiOperation({ title: 'Get wallet transcation history' })
	@ApiResponse({ status: 200, description: 'Return list wallet transcation' })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async walletHistory(@GetUser() user: UsersDTO): Promise<CommonResponseModel> {
		this.utilService.validateUserRole(user);
		try {
			const wallets = await this.walletService.walletHistory(user._id);
			return this.utilService.successResponseData(wallets);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}
}