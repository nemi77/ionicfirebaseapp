import { Controller, Post, Body, Get, Delete, UseGuards, Param, Put, Res, UseInterceptors, UploadedFile, Query } from '@nestjs/common';
import {
	UserCreateDTO,
	LoginDTO,
	UsersDTO,
	ChangePasswordDTO,
	ForgotPasswordDTO,
	ResetPasswordDTO,
	LanguageUpdateDTO,
	UsersUpdateDTO, UserStatusDTO, AdminDeliveryDTO, ResponseLogin, ResponseMe, ResponseAdminUserList, ResponseAdminDeliveryList
} from './users.model';
import { UserService } from './users.service';
import { ApiBearerAuth, ApiConsumes, ApiImplicitFile, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UtilService } from '../utils/util.service';
import { UserRoles, ResponseMessage, UploadImageDTO, UploadImageResponseDTO, AdminSettings, CommonResponseModel, ResponseErrorMessage, ResponseBadRequestMessage, ResponseSuccessMessage, AdminQuery } from '../utils/app.model';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { UploadService } from '../utils/upload.service';
import { AuthService } from '../utils/auth.service';
import { EmailService } from '../utils/email.service';
import { GetUser } from '../utils/jwt.strategy';

@Controller('users')
@ApiUseTags('Users')
export class UserController {
	constructor(
		private userService: UserService,
		private authService: AuthService,
		private utilService: UtilService,
		private emailService: EmailService,
		private uploadService: UploadService
	) {
	}

	/* ################################################### NO AUTH ################################## */
	@Post('/register')
	@ApiOperation({ title: 'Register user' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async registerNewUser(@Body() userData: UserCreateDTO): Promise<CommonResponseModel> {
		try {
			const checkUser = await this.userService.findUserByEmailOrMobile(userData.email, userData.mobileNumber);
			if (checkUser && checkUser.email == userData.email) this.utilService.badRequest(ResponseMessage.USER_EMAIL_ALREADY_EXIST);
			if (checkUser && checkUser.mobileNumber == userData.mobileNumber) this.utilService.badRequest(ResponseMessage.USER_MOBILE_ALREADY_EXIST);

			userData.role = UserRoles.USER;
			const user = await this.userService.createUser(userData);

			if (user) {
				const emailRes = await this.emailService.sendEmailForVerification(user.firstName, user.email, user.emailVerificationId);
				if (emailRes) return this.utilService.successResponseMsg(ResponseMessage.USER_EMAIL_VERIFY_SENT);
				else this.utilService.badRequest(ResponseMessage.USER_EMAIL_VERIFY_NOT_SENT);
			}
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Post('/login')
	@ApiOperation({ title: 'Log in user' })
	@ApiResponse({ status: 200, description: 'Return user info', type: ResponseLogin })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async validateUser(@Body() credential: LoginDTO): Promise<CommonResponseModel> {
		try {
			const user = await this.userService.getUserByEmail(credential.email);
			if (!user) this.utilService.badRequest(ResponseMessage.USER_EMAIL_NOT_FOUND);

			if (!user.status) this.utilService.badRequest(ResponseMessage.USER_ACCOUNT_BLOCKED);
			if (!user.emailVerified) return this.utilService.resetContentResponseMsg(ResponseMessage.USER_EMAIL_NOT_VERIFIED);

			const isValid = await this.authService.verifyPassword(credential.password, user.password);
			if (!isValid) this.utilService.badRequest(ResponseMessage.USER_EMAIL_OR_PASSWORD_INVALID);

			await this.userService.updatePlayerId(user._id, credential.playerId);
			const token = await this.authService.generateAccessToken(user._id, user.role);
			return this.utilService.successResponseData({ token: token, role: user.role, id: user._id, language: user.language });
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Post('/forgot-password')
	@ApiOperation({ title: 'Forgot password' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async forgotPassword(@Body() emailData: ForgotPasswordDTO): Promise<CommonResponseModel> {
		try {
			const checkEmail = await this.userService.getUserByEmail(emailData.email);
			if (!checkEmail) this.utilService.badRequest(ResponseMessage.USER_EMAIL_INVALID);

			const otp = Math.floor(9000 * Math.random()) + 1000;
			const user = await this.userService.updateOTP(checkEmail._id, otp);

			const emailRes = await this.emailService.sendEmailForForgotPassword(user.firstName, user.email, otp);
			if (emailRes) return this.utilService.successResponseMsg(ResponseMessage.USER_FORGOT_PASSWORD_OTP_SENT_EMAIL);
			else this.utilService.badRequest(ResponseMessage.USER_FORGOT_PASSWORD_OTP_NOT_SENT_EMAIL);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Get('/resend-verify-email')
	@ApiOperation({ title: 'Resend email verification to email' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async resendVerifyUserEmail(@Query('email') email: string) {
		try {
			const user = await this.userService.getUserByEmail(email);
			if (!user) this.utilService.badRequest(ResponseMessage.USER_EMAIL_NOT_FOUND);

			const resend = await this.userService.regenerateVerificationCode(user._id);
			if (resend) {
				const emailRes = await this.emailService.sendEmailForVerification(user.firstName, user.email, resend.emailVerificationId);
				if (emailRes) return this.utilService.successResponseMsg(ResponseMessage.USER_EMAIL_VERIFY_SENT);
				else this.utilService.badRequest(ResponseMessage.USER_EMAIL_VERIFY_NOT_SENT);
			}
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Get('/verify-email')
	@ApiOperation({ title: 'Verify email verification to email' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async verifyUserEmail(@Query('verificationId') verificationId: string, @Query('email') email: string, @Res() res) {
		try {
			const user = await this.userService.getUserByEmail(email);
			if (!user) {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_EMAIL_NOT_FOUND);
				return res.send(message);
			}
			if (user.emailVerified) {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_EMAIL_VERIFIED_ALREADY);
				return res.send(message);
			}
			if (user.emailVerificationId !== verificationId) {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_EMAIL_VERIFICATION_CODE_INVALID);
				return res.send(message);
			}

			const currentTime = (new Date()).getTime()
			if (currentTime > user.emailVerificationExpiry) {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_EMAIL_VERIFY_EXPIRED);
				return res.send(message);
			}
			const isVerified = await this.userService.setEmailVerified(user._id);
			if (isVerified) {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_EMAIL_VERIFIED_SUCCESSFULLY);
				return res.send(message);
			}
			else {
				const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.SOMETHING_WENT_WRONG);
				return res.send(message);
			}
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Get('/verify-otp')
	@ApiOperation({ title: 'Verify OTP for reset password' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async verifyOtpEmail(@Query('otp') otp: number, @Query('email') email: string) {
		try {
			const user = await this.userService.getUserByEmail(email);
			if (!user) this.utilService.badRequest(ResponseMessage.USER_EMAIL_NOT_FOUND);
			if (user.otp != otp) this.utilService.badRequest(ResponseMessage.USER_FORGOT_PASSWORD_OTP_INVALID);

			const currentTime = (new Date()).getTime();
			if (currentTime > user.otpVerificationExpiry) this.utilService.badRequest(ResponseMessage.USER_FORGOT_PASSWORD_OTP_EXPIRED);

			const otpVerificationId = await this.utilService.getUUID();
			const isVerified = await this.userService.setOTPVerification(user._id, otpVerificationId);
			const message = await this.utilService.getTranslatedMessageByKey(ResponseMessage.USER_FORGOT_PASSWORD_OTP_VERIFIED);
			if (isVerified) return this.utilService.successResponseData({ verificationToken: otpVerificationId, message: message });
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Post('/reset-password')
	@ApiOperation({ title: 'Reset password' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async resetPassword(@Body() passwordData: ResetPasswordDTO): Promise<CommonResponseModel> {
		try {
			const user = await this.userService.getUserByEmail(passwordData.email);
			if (!user) this.utilService.badRequest(ResponseMessage.USER_EMAIL_NOT_FOUND);

			if (user.otpVerificationId !== passwordData.verificationToken) this.utilService.badRequest(ResponseMessage.USER_RESET_PASSWORD_INVALID_TOKEN);
			const { salt, hashedPassword } = await this.authService.hashPassword(passwordData.newPassword);

			const newPaswword = await this.userService.updatePassword(user._id, salt, hashedPassword);
			if (newPaswword) return this.utilService.successResponseMsg(ResponseMessage.USER_PASSWORD_CHANGED);
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Post('/change-password')
	@ApiOperation({ title: 'Change password' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async changePassword(@GetUser() user: UsersDTO, @Body() passwordData: ChangePasswordDTO): Promise<CommonResponseModel> {
		try {
			const userInfo = await this.userService.getUserById(user._id);
			const isPasswordMatch = await this.authService.verifyPassword(passwordData.currentPassword, userInfo.password);
			if (!isPasswordMatch) this.utilService.badRequest(ResponseMessage.USER_CURRENT_PASSWORD_INCORRECT);

			const { salt, hashedPassword } = await this.authService.hashPassword(passwordData.newPassword);
			const newPaswword = await this.userService.updatePassword(user._id, salt, hashedPassword);

			if (newPaswword) return this.utilService.successResponseMsg(ResponseMessage.USER_PASSWORD_CHANGED);
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	/* ################################################### USERS ################################## */
	@Get('/me')
	@ApiOperation({ title: 'Get logged-in user info' })
	@ApiResponse({ status: 200, description: 'Return user info', type: ResponseMe })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async GetUserInfo(@GetUser() user: UsersDTO): Promise<CommonResponseModel> {
		try {
			const me = await this.userService.getUserInfo(user._id);
			if (me) {
				me['walletAmount'] = me['walletAmount'] || 0;
				return this.utilService.successResponseData(me);
			}
			else return this.utilService.successResponseMsg(ResponseMessage.USER_PROFILE_NOT_FOUND);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Put('/update/profile')
	@ApiOperation({ title: 'Update profile' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async updateProfile(@GetUser() user: UsersDTO, @Body() userInfo: UsersUpdateDTO): Promise<CommonResponseModel> {
		try {
			if (user && user.mobileNumber != userInfo.mobileNumber) {
				const checkUser = await this.userService.findUserByMobile(userInfo.mobileNumber);
				if (checkUser) this.utilService.badRequest(ResponseMessage.USER_MOBILE_ALREADY_EXIST);
			}

			const response = await this.userService.updateMyInfo(user._id, userInfo);
			if (response) return this.utilService.successResponseMsg(ResponseMessage.USER_PROFILE_UPDATED);
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	/* ################################################### ADMIN ################################## */
	@Get('/admin/list')
	@ApiOperation({ title: 'Get all users' })
	@ApiImplicitQuery({ name: "page", description: "page", required: false, type: Number })
	@ApiImplicitQuery({ name: "limit", description: "limit", required: false, type: Number })
	@ApiResponse({ status: 200, description: 'Return list of user', type: ResponseAdminUserList })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async getAllUserList(@GetUser() user: UsersDTO, @Query() adminQuery: AdminQuery): Promise<CommonResponseModel> {
		this.utilService.validateAdminRole(user);
		try {
			let pagination = this.utilService.getAdminPagination(adminQuery);
			const users = await Promise.all([
				this.userService.getAllUser(pagination.page - 1, pagination.limit, pagination.q),
				this.userService.countAllUser(pagination.q)
			])
			return this.utilService.successResponseData(users[0], { total: users[1] });
		} catch (e) {
			this.utilService.errorResponse(e)
		}
	}

	@Put('/admin/status-update/:userId')
	@ApiOperation({ title: 'Update user status by userId' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async updateUserStatus(@GetUser() user: UsersDTO, @Param('userId') userId: string, @Body() userStatusData: UserStatusDTO): Promise<CommonResponseModel> {
		this.utilService.validateAdminRole(user);
		try {
			const userExist = await this.userService.getUserById(userId);
			if (!userExist) this.utilService.badRequest(ResponseMessage.USER_NOT_FOUND);

			const userStatus = await this.userService.updateUserStatus(userId, userStatusData);
			if (userStatus) return this.utilService.successResponseMsg(ResponseMessage.USER_STATUS_UPDATED);
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Put('/language/update')
	@ApiOperation({ title: 'Update user preferred language' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async updateLanguage(@GetUser() user: UsersDTO, @Body() updateData: LanguageUpdateDTO): Promise<CommonResponseModel> {
		this.utilService.validateUserRole(user);
		try {
			const resData = await this.userService.updateMyLanguage(user._id, updateData.language);
			if (resData) return this.utilService.successResponseMsg(ResponseMessage.USER_LANGUAGE_UPDATED);
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG)
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}

	@Post('/upload/image')
	@ApiOperation({ title: 'User profile image upload' })
	@ApiResponse({ status: 200, description: 'Return image detail', type: UploadImageResponseDTO })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseInterceptors(FileInterceptor('file'))
	@ApiConsumes('multipart/form-data')
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	@ApiImplicitFile({ name: 'file', required: true, description: 'Profile image upload' })
	public async userImageUpload(@GetUser() user: UsersDTO, @UploadedFile() file, @Body() image: UploadImageDTO): Promise<CommonResponseModel> {
		try {
			const uploadedImage = await this.uploadService.uploadImage(file, image.type) as UploadImageResponseDTO;
			if (uploadedImage.url) return this.utilService.successResponseData(uploadedImage);
			this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}


	@Delete('/delete/image')
	@ApiOperation({ title: 'Delete user profile image' })
	@ApiResponse({ status: 200, description: 'Return image detail', type: UploadImageResponseDTO })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async deleteImage(@GetUser() user: UsersDTO): Promise<CommonResponseModel> {
		try {
			const userInfo = await this.userService.getUserInfo(user._id);

			const deleteImage = await this.uploadService.deleteImage(userInfo.imageId)
			if (deleteImage) {
				let removeImageDetail = { imageUrl: null, imageId: null, filePath: null }

				const UpdatedUseer = await this.userService.updateMyInfo(user._id, removeImageDetail);
				if (UpdatedUseer) return this.utilService.successResponseMsg(ResponseMessage.USER_PROFILE_IMAGE_DELETED);
				else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
			}
			else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
		} catch (e) {
			this.utilService.errorResponse(e);
		}
	}
}
