import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CartDataModel, UserCartDTO } from './cart.model';
import { UtilService } from '../utils/util.service';
import { ResponseMessage } from '../utils/app.model';
import { DeliveryType } from '../settings/settings.model';

@Injectable()
export class CartService {
	constructor(
		@InjectModel('Cart') private readonly cartModel: Model<any>,
		@InjectModel('OrderProducts') private readonly orderProductModel: Model<any>,
		private utilService: UtilService,
	) {
	}

	public async getMyCart(userId: string): Promise<UserCartDTO> {
		const cart = await this.cartModel.findOne({ userId: userId, isOrderLinked: false }, '-_id -userId -createdAt -updatedAt -__v');
		return cart;
	}

	public async getCartByUserId(id: string): Promise<any> {
		const cartInfo = await this.cartModel.findOne({ userId: id, isOrderLinked: false });
		return cartInfo;
	}

	public async getCartById(id: string): Promise<UserCartDTO> {
		const cart = await this.cartModel.findOne({ _id: id }, '-_id -userId -createdAt -updatedAt -__v');
		return cart;
	}

	public async getCartByIdOnlyProducts(id: string): Promise<UserCartDTO> {
		const cart = await this.cartModel.findOne({ _id: id }, 'productIds');
		return cart;
	}

	public async addProductInOrdersForRating(productOrderData): Promise<any> {
		return await this.orderProductModel.updateOne({ userId: productOrderData.userId, productId: productOrderData.productId }, productOrderData, { upsert: true });
	}

	public async isUserBoughtProduct(userId: string, productId: string): Promise<number> {
		return await this.orderProductModel.findOne({ userId: userId, productId: productId });
	}

	public async findProductsById(userId: string, productIds): Promise<any> {
		return await this.orderProductModel.find({ userId: userId, productId: { $in: productIds } });
	}

	public async updateRating(userId: string, productId: string, rating: number): Promise<any> {
		return await this.orderProductModel.updateOne({ userId: userId, productId: productId }, { isRated: true, rating: rating });
	}

	public async checkOutOfStockOrLeft(product, cart): Promise<any> {
		let errArr;
		if (product && product.variant && product.variant.length) {
			const variant = product.variant.find(val => val.unit == cart["unit"]);
			if (variant) {
				if (variant.enable && variant.productStock < cart.quantity) {
					const resMsg = await this.utilService.getTranslatedMessageByKey(ResponseMessage.CART_ITEM_LEFT);
					errArr = `${product.title} - ${variant.unit} - ${variant.productStock} ${resMsg}`;
				}
			} else {
				const resMsg = await this.utilService.getTranslatedMessageByKey(ResponseMessage.CART_ITEM_OUT_OF_STOCK);
				errArr = `${product.title}  ${resMsg}`;
			}
		} else {
			const resMsg = await this.utilService.getTranslatedMessageByKey(ResponseMessage.CART_ITEM_OUT_OF_STOCK);
			errArr = `${product.title}  ${resMsg}`;
		}
		return errArr;
	}

	public async verifyCart(products, carts): Promise<any> {
		let cartArr = [], productArr = [];
		for (let cartItem of carts.products) {
			const productIndex = await products.findIndex(val => val._id.toString() == cartItem.productId.toString());
			if (productIndex !== -1) {
				if (products[productIndex].variant.length) {
					if (products[productIndex].status == false) {
						const resMsg = await this.utilService.getTranslatedMessageByKey(ResponseMessage.CART_ITEM_LEFT);
						cartArr.push(cartItem);
					} else {
						const varientIndex = await products[productIndex].variant.findIndex(val => val.unit == cartItem.unit);
						if (varientIndex !== -1) {
							if (products[productIndex].variant[varientIndex].enable && products[productIndex].variant[varientIndex].productStock < cartItem.quantity) {
								cartArr.push(cartItem)
							} else {
								products[productIndex].variant[varientIndex].productStock = products[productIndex].variant[varientIndex].productStock - cartItem.quantity;
								productArr.push(products[productIndex])
							}
						}
					}
				}
			}
		}
		return { cartArr, productArr }
	}

	public calculateProductPrice(product, cart) {
		let price = 0, unit, dealAmount = 0, productTotal = 0;
		const variant = product.variant.find(val => val.unit == cart["unit"]);
		if (variant) {
			price = variant['price'];
			unit = variant['unit'];
		}
		productTotal = Number(price) * Number(cart.quantity);
		if (product.isDealAvailable) {
			dealAmount = Number(product.dealPercent * price) / 100;
			productTotal = (price - dealAmount) * cart.quantity;
		}

		let cartInfo = {
			productId: product._id.toString(),
			productName: product.title,
			unit: unit,
			price: price,
			quantity: cart.quantity,
			productTotal: productTotal,
			imageUrl: product.imageUrl,
			filePath: product.filePath,
			dealAmount: dealAmount,
			dealPercent: product.dealPercent,
			dealTotalAmount: dealAmount * cart.quantity,
			isDealAvailable: product.isDealAvailable
		};
		return cartInfo;
	}

	public async calculateTotal(cartInfo, deliveryTax?, address?) {
		cartInfo.subTotal = 0;
		cartInfo.products.forEach(cart => { cartInfo.subTotal += Number((cart.productTotal).toFixed(2)); });

		if (deliveryTax) {
			cartInfo.tax = this.taxCalculation(cartInfo, deliveryTax);
			cartInfo.taxInfo = { taxName: deliveryTax.taxName, amount: deliveryTax.taxAmount };
		}

		if (address) {
			cartInfo.deliveryCharges = await this.calculateDeliveryCharge(deliveryTax, cartInfo.subTotal, address);
		}

		cartInfo.couponAmount = cartInfo.couponAmount || 0;
		cartInfo.walletAmount = cartInfo.walletAmount || 0;
		cartInfo.grandTotal = Number((cartInfo.subTotal + cartInfo.deliveryCharges + cartInfo.tax - cartInfo.couponAmount - cartInfo.walletAmount).toFixed(2));
		return cartInfo;
	}

	public async getCartDetail(cartId: string): Promise<UserCartDTO> {
		const cartInfo = await this.cartModel.findById(cartId);
		return cartInfo;
	}

	public async saveCart(cartInfo): Promise<UserCartDTO> {
		const cart = await this.cartModel.create(cartInfo);
		return cart;
	}

	public async updateCart(cartId, cartInfo): Promise<UserCartDTO> {
		const cart = await this.cartModel.findByIdAndUpdate(cartId, cartInfo, { new: true });
		return cart;
	}

	public async updateAddressInCart(cartId, cartInfo): Promise<UserCartDTO> {
		const cart = await this.cartModel.findByIdAndUpdate(cartId, cartInfo, { fields: { deliveryCharges: 1, grandTotal: 1, deliveryAddress: 1 }, new: true });
		return cart;
	}

	public taxCalculation(cart: CartDataModel, deliveryAndTaxSetting) {
		let tax = Number((cart.subTotal * deliveryAndTaxSetting.taxAmount / 100).toFixed(2));
		return tax;
	}

	public async deleteCart(cartId): Promise<UserCartDTO> {
		const cart = await this.cartModel.findByIdAndDelete(cartId);
		return cart;
	}

	public async cartOrderUnlink(cartId: string): Promise<Boolean> {
		await this.cartModel.findByIdAndUpdate(cartId, { isOrderLinked: true });
		return true;
	}

	public async calculateDeliveryCharge(deliveryTax, subTotal, address) {
		let deliveryCharges = 0;
		if (deliveryTax.deliveryType === DeliveryType.FLEXIBLE) {
			const storeLocation = { latitude: deliveryTax.location.latitude, longitude: deliveryTax.location.longitude };
			const userLocation = { latitude: address.location.latitude, longitude: address.location.longitude }
			const preciseDistance = this.utilService.calculateDistance(userLocation, storeLocation);
			deliveryCharges = Number((deliveryTax.deliveryChargePerKm * preciseDistance).toFixed(2));
			if (deliveryTax.minOrderAmountForFree && subTotal >= deliveryTax.minOrderAmountForFree) deliveryCharges = 0;
			deliveryCharges = Number(deliveryCharges);
		} else if (deliveryTax.deliveryType === DeliveryType.FIXED) {
			deliveryCharges = Number((deliveryTax.fixedDeliveryCharges).toFixed(2));
			if (deliveryTax.minOrderAmountForFree && subTotal >= deliveryTax.minOrderAmountForFree) deliveryCharges = 0;
		}
		return deliveryCharges;
	}
}