# Grocery Pro API

## RUN API locally
You need to create .env file in root folder and following enviroment variables:

```
MONGO_DB_URL_STAGING=
MONGO_DB_URL_PRODUCTION=

SECRET=

IMAGEKIT_URL=
IMAGEKIT_PRIVATE_KEY=
IMAGEKIT_PUBLIC_KEY=

SENDGRID_KEY=
SENDGRID_FROM=

ONE_SIGNAL_APP_ID_USER=
ONE_SIGNAL_SECRET_KEY_USER=

STRIPE_SECRET_KEY=

API_URL_STAGING=
API_URL_PRODUCTION=
```

## Reset Database with default data
It will delete all data from database and it will set default data.
Do not run it for production otherwise you will loose all data.

```npx ts-node  mongodb-seeding.ts```
