<p align="center"> 
  <a href="./README.md"> English </a> | 简体中文 | <a href="./README.ja-JP.md"> 日本語 </a>
</p>

<h1 align="center"> Vuestic Admin </h1>
  
<p align="center">
  免费与美妙Vue.js管理模板包括38以上个定制用户界面组件</br>
  由<a href="https://epicmax.co">Epicmax</a>开发。
  由<a href="https://www.xxsavitski.com">Vasili Savitski</a>设计
***REDACTED***
