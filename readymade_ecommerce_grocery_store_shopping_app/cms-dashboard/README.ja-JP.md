<p align="center"> 
  <a href="./README.md"> English </a> | <a href="./README.zh-CN.md"> 简体中文 </a> | 日本語
</p>

<h1 align="center"> Vuestic Admin </h1>
  
<p align="center">
  38+のカスタムUIコンポーネントと美しいデザインのVue.js無料管理画面テンプレート</br>
  開発者  <a href="https://epicmax.co">Epicmax</a>. 
  デザイン <a href="https://www.xxsavitski.com">Vasili Savitski</a>
***REDACTED***
