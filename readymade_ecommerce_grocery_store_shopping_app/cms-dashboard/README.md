<p align="center"> 
  English | <a href="./README.zh-CN.md"> 简体中文 </a> | <a href="./README.ja-JP.md"> 日本語 </a>
</p>

<h1 align="center"> Vuestic Admin </h1>
  
<p align="center">
  Free and beautiful Vue.js admin template with 44+ custom UI components.</br>
  Developed by  <a href="https://epicmax.co">Epicmax</a>. 
  Designed by <a href="https://www.xxsavitski.com">Vasili Savitski</a>
***REDACTED***
