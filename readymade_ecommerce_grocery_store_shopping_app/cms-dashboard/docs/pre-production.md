# Pre-production

## SEO

We have a boierplate prepared with some analytics ingrained. This includes:

* [Yandex Metrica](https://metrica.yandex.com/about)
* [Drift](https://www.drift.com/mobile-app/)

To get these running - just provide keys to respective apis. You're advised to copy `.env.example` with rename to `.env` then modify it.
***REDACTED***
