import Vue from 'vue'
import Vuex from 'vuex'
import VuexI18n from 'vuex-i18n' // load vuex i18n module
import app from './modules/app'
import axios from 'axios';
import * as getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
***REDACTED***
