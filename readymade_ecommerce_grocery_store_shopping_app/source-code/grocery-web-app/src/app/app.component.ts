import { Component } from '@angular/core';
import { CrudService } from './services/crud.service';
import { Store } from '@ngrx/store';
import { CartState } from './pages/store/cart.reducer';
import { HelperService } from './services/helper.service';
import { AuthGuardService } from './services/auth-guard.service';
import { TranslateService } from "@ngx-translate/core";
import { CartService } from "./pages/home/cart/cart.service";
import { SocketService } from "./services/socket.service";
import { CheckoutService } from "./pages/home/checkout/checkout.service";
***REDACTED***
