import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './pages/auth/registration/registration.component';
import { WelcomeComponent } from './pages/auth/welcome/welcome.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { ForgotPasswdComponent } from './pages/auth/forgot-passwd/forgot-passwd.component';
import { ResetPasswdComponent } from './pages/auth/reset-passwd/reset-passwd.component';
import { VerifyNumberComponent } from './pages/auth/verify-number/verify-number.component';
import { HomeComponent } from './pages/home/home/home.component';
import { MyStoreComponent } from './pages/home/my-store/my-store.component';
***REDACTED***
