import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CardModel, PlaceOrderModel } from '../../models/order.model';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart.reducer';
import { CartModel, OutOfStockData, PaymentMethodEnum } from '../../models/cart.model';
import { HelperService } from '../../../services/helper.service';
import { Router } from '@angular/router';
import { ActiveToast } from 'ngx-toastr';
import { environment } from "../../../../environments/environment";
***REDACTED***
