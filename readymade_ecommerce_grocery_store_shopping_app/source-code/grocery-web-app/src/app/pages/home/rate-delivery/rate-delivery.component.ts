import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';
import { Deliveryboy, OrderListModel, OrderModel, RateModel, RateModelDeliveryBoy } from '../../models/order.model';
import { OrdersService } from '../orders/orders.service';
import { RateDeliveryService } from './rate-delivery.service';

@Component({
  selector: 'app-rate-delivery',
  templateUrl: './rate-delivery.component.html',
***REDACTED***
