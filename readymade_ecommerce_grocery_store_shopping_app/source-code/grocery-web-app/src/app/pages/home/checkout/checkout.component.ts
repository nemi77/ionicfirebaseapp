import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbDateStruct, NgbCalendar, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart.reducer';
import { CartModel } from '../../models/cart.model';
import { AddressModel, DeliveryTaxModel, TimeScheduleModel, WorkingHoursModel } from '../../models/order.model';
import { HelperService } from '../../../services/helper.service';
import * as CartActions from '../../store/cart.action';
import { Router } from '@angular/router';
import { ActiveToast } from "ngx-toastr";
***REDACTED***
