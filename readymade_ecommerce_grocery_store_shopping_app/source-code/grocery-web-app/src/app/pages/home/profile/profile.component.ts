import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from '../../../services/helper.service';
import { MessageModel } from '../../models/chat.model';
import { Router } from '@angular/router';
import { OTPModel, UserInfoModel } from '../../models/auth.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { AddressModel } from '../../models/order.model';
import { LanguagesModel } from "../../models/category.model";
***REDACTED***
