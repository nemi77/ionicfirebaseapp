import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart.reducer';
import { Router } from '@angular/router';
import { HelperService } from '../../../services/helper.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AuthGuardService } from '../../../services/auth-guard.service';
import { TranslateService } from "@ngx-translate/core";
import { CartService } from "../cart/cart.service";
import { LanguagesModel } from "../../models/category.model";
***REDACTED***
