import { Component, OnInit, TemplateRef } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ProductsModel, VariantModel } from '../../models/category.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HelperService } from '../../../services/helper.service';
import { CartState } from '../../store/cart.reducer';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { ActiveToast } from 'ngx-toastr';
***REDACTED***
