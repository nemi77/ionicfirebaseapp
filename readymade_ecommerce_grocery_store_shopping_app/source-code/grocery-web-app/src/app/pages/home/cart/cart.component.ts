import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart.reducer';
import { AddToCartModel, CartModel } from '../../models/cart.model';
import { HelperService } from '../../../services/helper.service';
import { Router } from '@angular/router';
import { ProductsModel } from '../../models/category.model';
import * as _ from 'lodash';
import { environment } from "../../../../environments/environment";
import { ActiveToast } from "ngx-toastr";
***REDACTED***
