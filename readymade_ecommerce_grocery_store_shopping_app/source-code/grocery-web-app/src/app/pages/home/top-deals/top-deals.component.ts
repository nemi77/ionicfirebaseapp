import { Component, OnInit } from '@angular/core';
import { DealsModel, DealTypeEnum } from "../../models/category.model";
import { Router } from "@angular/router";
import { environment } from "../../../../environments/environment";
import { DealsService } from "./deals.service";

@Component({
  selector: 'app-top-deals',
  templateUrl: './top-deals.component.html',
  styleUrls: ['./top-deals.component.scss']
***REDACTED***
