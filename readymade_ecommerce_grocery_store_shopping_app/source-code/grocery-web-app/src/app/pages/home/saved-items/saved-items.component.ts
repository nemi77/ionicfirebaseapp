import { Component, OnInit, TemplateRef } from '@angular/core';
import { FavouritesModel, ProductsModel, VariantModel } from '../../models/category.model';
import { HelperService } from "../../../services/helper.service";
import { Store } from "@ngrx/store";
import { CartState } from "../../store/cart.reducer";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { environment } from "../../../../environments/environment";
import { CartService } from "../cart/cart.service";
import { ProductsService } from "../products/products.service";

***REDACTED***
