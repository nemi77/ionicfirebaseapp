import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ProductsModel, SubCategoryModel, VariantModel } from '../../models/category.model';
import { HelperService } from '../../../services/helper.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from "@ngrx/store";
import { CartState } from "../../store/cart.reducer";
import { environment } from "../../../../environments/environment";
import { ProductsService } from "./products.service";
***REDACTED***
