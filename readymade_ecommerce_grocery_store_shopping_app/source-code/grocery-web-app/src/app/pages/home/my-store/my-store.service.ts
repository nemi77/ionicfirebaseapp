import { Injectable } from "@angular/core";
import { CrudService } from "../../../services/crud.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MyStoreService {
  constructor(private crud: CrudService) {
  }
***REDACTED***
