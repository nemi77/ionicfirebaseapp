import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { interval, Observable } from 'rxjs';
import { OrderListModel, OrderModel, RateModel } from '../../models/order.model';
import { CartModel } from '../../models/cart.model';
import { HelperService } from '../../../services/helper.service';
import * as moment from 'moment';
import { environment } from '../../../../environments/environment';
import { OrdersService } from "./orders.service";
import { Router } from '@angular/router';
***REDACTED***
