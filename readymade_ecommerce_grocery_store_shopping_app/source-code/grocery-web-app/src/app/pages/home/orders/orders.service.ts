import { Injectable } from "@angular/core";
import { CrudService } from "../../../services/crud.service";
import { Observable } from "rxjs";
import { RateModel } from "../../models/order.model";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  constructor(private crud: CrudService) {
***REDACTED***
