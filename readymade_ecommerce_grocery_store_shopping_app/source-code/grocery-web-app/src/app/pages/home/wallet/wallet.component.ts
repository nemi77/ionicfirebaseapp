import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';
import { walletHistoryModel, walletModel } from '../../models/auth.model';
import { WalletPaginationType } from '../../models/order.model';
import { ProfileService } from '../profile/profile.service';
import { WalletService } from './wallet.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
***REDACTED***
