import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../../services/crud.service';
import { LoginModel, OTPModel } from '../../models/auth.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart.reducer';
import { HelperService } from '../../../services/helper.service';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../../environments/environment";
***REDACTED***
