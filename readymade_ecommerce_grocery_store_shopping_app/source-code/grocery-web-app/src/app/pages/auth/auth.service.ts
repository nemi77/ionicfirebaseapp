import { Injectable } from "@angular/core";
import { CrudService } from "../../services/crud.service";
import { Observable } from "rxjs";
import { CheckoutService } from "../home/checkout/checkout.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private crud: CrudService, private checkoutService: CheckoutService) {
***REDACTED***
