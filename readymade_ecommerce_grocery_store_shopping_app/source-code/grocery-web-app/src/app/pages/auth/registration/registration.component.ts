import { Component, OnInit } from '@angular/core';
import { RegistrationModel, OTPModel } from '../../models/auth.model';
import { Router } from '@angular/router';
import { HelperService } from "../../../services/helper.service";
import { TranslateService } from "@ngx-translate/core";
import { FormControl, FormGroup, Validators, NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-registration',
***REDACTED***
