import {Action} from '@ngrx/store';
import {AddToCartModel, CartModel, DeleteCartProductModel, UpdateCartModel} from '../models/cart.model';

export const ADD_TO_CART = 'ADD_TO_CART';
export const UPDATE_CART_DATA = 'UPDATE_CART_DATA';
export const DELETE_CART_DATA = 'DELETE_CART_DATA';
export const GET_CART_DATA = 'GET_CART_DATA';
export const SET_CART_DATA = 'SET_CART_DATA';
export const APPLY_COUPON = 'APPLY_COUPON';
export const REMOVE_COUPON = 'REMOVE_COUPON';
***REDACTED***
