import { CartModel } from '../models/cart.model';
import * as CartActions from './cart.action';

export interface CartState {
  cartInfo: CartModel;
}

function getDefaultCartValues(): CartModel {
  return {
    products: [],
***REDACTED***
