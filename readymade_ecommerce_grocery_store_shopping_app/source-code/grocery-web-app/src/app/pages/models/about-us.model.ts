export interface BusinessInfoModel {
  email: string,
  description: string,
  address: string,
  facebookUrl: string,
  googleUrl: string,
  linkedInUrl: string,
  twitterUrl: string,
  instagramUrl: string,
  officeLocation: string,
***REDACTED***
