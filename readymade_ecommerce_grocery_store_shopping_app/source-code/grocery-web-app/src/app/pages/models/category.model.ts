export interface CategoryModel {
  status: number;
  _id: string;
  title: string;
  description: string;
  imageUrl: string;
  imageId: string;
  user: string;
  createdAt: string;
  updatedAt: string;
***REDACTED***
