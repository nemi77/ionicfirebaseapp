import { OfferModel, ProductImageModel } from './category.model';

export interface AddToCartModel {
  productId: string;
  variantsName: string;
  productName: string;
  title: string;
  imageUrl: string;
  description: string;
  quantity: number;
***REDACTED***
