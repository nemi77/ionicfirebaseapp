export interface LoginModel {
  mobileNumber: string;
  password: string;
}

export interface RegistrationModel {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
***REDACTED***
