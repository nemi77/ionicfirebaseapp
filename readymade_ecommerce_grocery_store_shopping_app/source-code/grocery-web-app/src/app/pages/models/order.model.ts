import { AddToCartModel, CartModel } from './cart.model';

export interface AddressModel {
  _id?: string;
  address: string;
  flatNo: string;
  apartmentName: string;
  landmark: string;
  postalCode: string;
  location: {
***REDACTED***
