import { Injectable } from "@angular/core";
import Socket = SocketIOClient.Socket;
import { MessageModel, NewMessageModel } from "../pages/models/chat.model";
import * as io from "socket.io-client";
import { environment } from "../../environments/environment";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
***REDACTED***
