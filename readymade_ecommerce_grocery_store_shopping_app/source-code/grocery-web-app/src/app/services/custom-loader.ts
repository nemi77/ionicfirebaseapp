import { TranslateLoader } from "@ngx-translate/core";
import { CrudService } from "./crud.service";
import { Observable } from "rxjs";
import { HelperService } from "./helper.service";
import { map } from "rxjs/operators";

export class CustomLoader implements TranslateLoader {
  constructor(private crud: CrudService, private helperService: HelperService) {
  }

***REDACTED***
