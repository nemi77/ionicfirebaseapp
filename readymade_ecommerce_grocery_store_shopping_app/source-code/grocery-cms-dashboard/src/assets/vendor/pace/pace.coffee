defaultOptions =
  # How long should it take for the bar to animate to a new
  # point after receiving it
  catchupTime: 100

  # How quickly should the bar be moving before it has any progress
  # info from a new source in %/ms
  initialRate: .03

  # What is the minimum amount of time the bar should be on the
***REDACTED***
