import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../service/crud.service';
import { TranslateService } from '@ngx-translate/core';
import { SettingService } from "../settings/setting.service";
import { OrderService } from "../orders/order.service";
import { ToastEnum, UtilService } from "../../service/util.service";

export interface GraphModel {
	name: string;
	value: number;
***REDACTED***
