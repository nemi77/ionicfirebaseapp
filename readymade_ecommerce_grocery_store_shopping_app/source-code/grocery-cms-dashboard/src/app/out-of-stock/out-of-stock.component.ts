import { Component, OnInit, TemplateRef } from '@angular/core';
import { OutOfStockService } from "./out-of-stock.service";
import { UtilService } from "../../service/util.service";
import { CrudService } from '../../service/crud.service';

export interface OutOfStockListModel {
	status: boolean;
	_id: string;
	productId: string;
	unit: string;
***REDACTED***
