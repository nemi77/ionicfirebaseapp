import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './orders.component';
import { ViewOrderComponent } from "./view-order/view-order.component";
const routes: Routes = [
    {
        path: '',
        component: OrderComponent,
        data: {
            title: 'Orders'
***REDACTED***
