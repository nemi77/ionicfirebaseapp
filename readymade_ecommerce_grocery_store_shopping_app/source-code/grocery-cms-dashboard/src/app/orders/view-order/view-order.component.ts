import { Component, OnInit } from '@angular/core';
import { OrderService } from 'app/orders/order.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { OrderDetailModel } from "../orders.model";

@Component({
	selector: 'app-view-order',
	templateUrl: './view-order.component.html',
	styleUrls: ['./view-order.component.scss']
***REDACTED***
