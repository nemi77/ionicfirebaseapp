export interface OrderDetailModel {
	order: OrderModel;
	cart: CartModel;
	deliveryBoyRating: DeliveryBoyRatingModel;
}

export interface OrderModel {
	isOrderAssigned: boolean;
	assignedToName: string;
	isAcceptedByDeliveryBoy: boolean;
***REDACTED***
