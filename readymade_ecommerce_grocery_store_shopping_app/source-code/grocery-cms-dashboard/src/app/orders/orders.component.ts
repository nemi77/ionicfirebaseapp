import { Component, OnInit, TemplateRef } from '@angular/core';
import { OrderService } from './order.service';
import { DeliveryBoyModel } from '../delivery-boy/delivery-boy.component';
import { forkJoin } from "rxjs";
import { DeliveryBoyService } from "../delivery-boy/delivery-boy.service";
import { SettingService } from "../settings/setting.service";
import { UtilService } from "../../service/util.service";
import { OrderModel } from "./orders.model";
import { ActivatedRoute, Router } from "@angular/router";

***REDACTED***
