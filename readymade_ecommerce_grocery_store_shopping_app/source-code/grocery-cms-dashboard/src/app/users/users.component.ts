import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { UserService } from './user.service';
import { UtilService } from "../../service/util.service";
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
***REDACTED***
