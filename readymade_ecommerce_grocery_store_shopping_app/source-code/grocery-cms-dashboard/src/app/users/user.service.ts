import { Injectable } from '@angular/core';
import { CrudService } from '../../service/crud.service';
import { Observable } from "rxjs";
import { ChangePasswordModel, UserInfoModel } from "../settings/my-profile/my-profile.component";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private crud: CrudService) {
***REDACTED***
