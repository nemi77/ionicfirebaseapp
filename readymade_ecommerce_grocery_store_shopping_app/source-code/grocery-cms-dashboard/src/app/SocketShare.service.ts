import { Injectable } from "@angular/core";
import * as io from "socket.io-client";
import { environment } from "../environments/environment";
import { BehaviorSubject, Observable } from "rxjs";
import { MessageModel } from "./settings/chat/chat.component";
import { CrudService } from "../service/crud.service";
import { Store } from "@ngrx/store";
import { ChatState } from "./chat-store/chat.reducer";
import * as ChatActions from "./chat-store/chat.action";
import { Router } from "@angular/router";
***REDACTED***
