import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './categories.component';
import { AddEditCategoryComponent } from "./add-edit-category/add-edit-category.component";
import { ViewCategoryComponent } from "./view-category/view-category.component";

const routes: Routes = [
	{
		path: '',
		component: CategoryComponent,
***REDACTED***
