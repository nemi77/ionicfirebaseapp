import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CouponComponent } from './coupons.component';
import { AddEditCouponComponent } from "./add-edit-coupon/add-edit-coupon.component";

const routes: Routes = [
	{
		path: '',
		component: CouponComponent,
		data: {
***REDACTED***
