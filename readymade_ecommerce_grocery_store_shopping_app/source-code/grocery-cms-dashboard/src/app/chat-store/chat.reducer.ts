import * as ChatActions from "./chat.action";
import { ChatDataModel, ChatUserMessagesModel } from "../settings/chat/chat.component";

// contains
export interface ChatState {
	chatState: ChatAppState;
}

export interface ChatAppState {
	newChat: ChatDataModel;
***REDACTED***
