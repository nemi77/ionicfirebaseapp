import { Component, OnInit } from '@angular/core';
import { DeliveryBoyModel, DialCodeList } from '../delivery-boy.component';
import { ActivatedRoute, Router } from "@angular/router";
import { UtilService } from "../../../service/util.service";
import { DeliveryBoyService } from "../delivery-boy.service";

@Component({
	selector: 'app-add-edit-delivery-boy',
	templateUrl: './add-edit-delivery-boy.component.html',
	styleUrls: ['./add-edit-delivery-boy.component.scss']
***REDACTED***
