import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DeliveryBoyService } from "./delivery-boy.service";
import { UtilService } from "../../service/util.service";
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";

export interface DeliveryBoyModel {
	_id?: string;
	firstName: string;
***REDACTED***
