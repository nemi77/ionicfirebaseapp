import { Injectable, EventEmitter } from "@angular/core";
import { CrudService } from "../../service/crud.service";
import { Observable } from "rxjs";

@Injectable()
export class NotificationService {
	notifyEvent: EventEmitter<number> = new EventEmitter();

	constructor(
		private crud: CrudService
***REDACTED***
