import { Component, OnInit } from '@angular/core';
import { NotificationService } from "./notification.service";
import { Router } from '@angular/router';

export interface NotificationListModel {
	createdAt: string;
	isRead: boolean;
	notifyType: string;
	orderID: string;
	orderId: string;
***REDACTED***
