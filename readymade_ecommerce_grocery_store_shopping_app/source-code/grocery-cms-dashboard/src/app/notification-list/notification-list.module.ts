import { RouterModule, Routes } from "@angular/router";

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { NotificationListComponent } from "./notification-list.component";
import { NotificationService } from "./notification.service";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { CustomLoader } from "../custom-loader";
***REDACTED***
