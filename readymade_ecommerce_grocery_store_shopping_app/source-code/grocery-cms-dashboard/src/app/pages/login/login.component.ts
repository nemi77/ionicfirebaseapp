import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketSharedService } from '../../SocketShare.service';
import { environment } from '../../../environments/environment';
import { AuthService, CMS_ROLES } from "./auth.service";
import { ToastEnum, UtilService } from "../../../service/util.service";

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
***REDACTED***
