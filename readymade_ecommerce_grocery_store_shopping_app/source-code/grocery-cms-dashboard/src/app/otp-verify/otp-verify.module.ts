import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {ChartistModule} from 'ng-chartist';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatchHeightModule} from "../shared/directives/match-height.directive";
import {OtpVerifyComponent} from './otp-verify.component';
import {OtpRoutingmodule} from './otp-verify-routing.module';
import {FormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {CustomLoader} from "../custom-loader";
***REDACTED***
