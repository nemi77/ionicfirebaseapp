import { Directive } from "@angular/core";
import { SidebarLinkDirective } from "./sidebarlink.directive";

@Directive({ selector: "[appSidebarList]" })
export class SidebarListDirective {
  protected navlinks: Array<SidebarLinkDirective> = [];
  public activeLinks: string[] = [];

  setList(list: Array<SidebarLinkDirective>) {
    this.navlinks = list;
***REDACTED***
