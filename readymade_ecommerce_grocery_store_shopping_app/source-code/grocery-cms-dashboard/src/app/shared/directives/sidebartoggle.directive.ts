import { Directive, HostBinding, Input, HostListener, AfterViewInit, ElementRef, Renderer2 } from '@angular/core';

@Directive({ selector: "[appSidebarToggle]" })
export class SidebarToggleDirective implements AfterViewInit {
  @HostBinding("attr.data-toggle")
  @Input()
  get dataToggle(): string {
    return this._dataToggle;
  }
  set dataToggle(value: string) {
***REDACTED***
