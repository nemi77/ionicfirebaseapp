import { Component, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { LayoutService } from '../services/layout.service';
import { ConfigService } from '../services/config.service';
import { Router } from '@angular/router';
import { SocketSharedService } from '../../SocketShare.service';
import { LanguagesModel } from '../../settings/languages/create-language/create-language.component';
import { SettingService } from "../../settings/setting.service";
import { ToastEnum, UtilService } from "../../../service/util.service";
***REDACTED***
