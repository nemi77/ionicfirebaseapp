import { Routes } from "@angular/router";
import { AuthGuard } from "../auth/auth-guard.service";

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: "settings",
    loadChildren: () =>
      import("../../settings/settings.module").then((m) => m.SettingsModule),
    canActivate: [AuthGuard],
***REDACTED***
