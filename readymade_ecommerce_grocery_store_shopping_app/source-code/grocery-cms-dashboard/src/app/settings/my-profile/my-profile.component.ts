import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from "../../users/user.service";
import { UtilService } from "../../../service/util.service";

export interface UserInfoModel {
	_id: string;
	firstName: string;
	lastName: string;
	email: string;
***REDACTED***
