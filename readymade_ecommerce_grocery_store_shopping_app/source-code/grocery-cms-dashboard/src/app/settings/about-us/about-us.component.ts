import { Component } from "@angular/core";
import { SettingService } from "../setting.service";
import { UtilService } from "../../../service/util.service";

export interface PageTypeModel {
	pageType: string;
	title: string;
	description: string;
}

***REDACTED***
