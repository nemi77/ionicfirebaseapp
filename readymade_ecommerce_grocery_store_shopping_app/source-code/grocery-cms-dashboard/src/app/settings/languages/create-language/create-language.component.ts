import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingService } from "../../setting.service";
import { ToastEnum, UtilService } from "../../../../service/util.service";

export interface LanguagesModel {
	languageCode: string;
	languageName: string;
***REDACTED***
