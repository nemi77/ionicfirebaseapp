import { Component, OnInit } from '@angular/core';
import { LanguagesModel } from './create-language/create-language.component';
import { SettingService } from "../setting.service";
import { ToastEnum, UtilService } from "../../../service/util.service";
import { Router } from "@angular/router";
import { CrudService } from 'service/crud.service';

@Component({
	selector: 'app-languages-component',
	templateUrl: './languages.component.html',
***REDACTED***
