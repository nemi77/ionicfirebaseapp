import { Component, OnInit } from '@angular/core';
import { SettingService } from "../setting.service";
import { UtilService } from "../../../service/util.service";

export interface DeliveryTaxModel {
	_id?: string;
	deliveryType: string;
	taxType: string;
	fixedDeliveryCharges: number;
	deliveryChargePerKm: number;
***REDACTED***
