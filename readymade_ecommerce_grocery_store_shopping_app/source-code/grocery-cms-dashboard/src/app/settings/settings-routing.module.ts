import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkingHourComponent } from './working-hours/working-hours.component';
import { ChatComponent } from './chat/chat.component';
import { DeliveryTaxComponent } from './delivey&Tax/delivery-tax.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { BusinessInfoComponent } from './business-info/business-info.component';
import { CurrencyComponent } from './currency/currency.component';
import { LanguagesComponent } from './languages/languages.component';
import { CreateLanguageComponent } from './languages/create-language/create-language.component';
***REDACTED***
