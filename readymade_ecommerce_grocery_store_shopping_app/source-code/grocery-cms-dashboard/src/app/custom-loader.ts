import {TranslateLoader} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {CrudService} from '../service/crud.service';
import {map} from "rxjs/operators";

export class CustomLoader implements TranslateLoader {
    constructor(private httpClient: HttpClient, private crud: CrudService) {
    }
***REDACTED***
