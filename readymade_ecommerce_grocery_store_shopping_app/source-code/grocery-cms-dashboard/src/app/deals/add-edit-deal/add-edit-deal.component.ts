import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DealService } from "../deals.service";
import { ProductService } from "../../products/products.service";
import { CategoryService } from "../../categories/category.service";
import { forkJoin } from "rxjs";
import { UtilService } from "../../../service/util.service";
import { DealModel } from '../deals.component';
import { CategoryModel } from "../../categories/categories.component";
import { ProductModel } from "../../products/products.model";
***REDACTED***
