import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DealComponent } from './deals.component';
import { AddEditDealComponent } from "./add-edit-deal/add-edit-deal.component";
import { ViewDealComponent } from "./view-deal/view-deal.component";

const routes: Routes = [
    {
        path: '',
        component: DealComponent,
***REDACTED***
