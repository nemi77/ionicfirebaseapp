export interface ProductModel {
    title: string;
    keyWords: string;
    description: string;
    imageUrl: string;
    categoryId: string;
    imageId: string;
    type: string;
    variant: Array<VariantModel>;
    subCategoryId?: string;
***REDACTED***
