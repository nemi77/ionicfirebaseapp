import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActiveToast } from 'ngx-toastr';
import { ImageModel, ProductModel } from '../products.model';
import { SubCategoryListModel } from '../../sub-category/sub-category.component';
import { ProductService } from "../products.service";
import { CategoryService } from "../../categories/category.service";
import { SubCategoryService } from "../../sub-category/sub-category.service";
import { UtilService } from "../../../service/util.service";
import { CategoryModel } from "../../categories/categories.component";
***REDACTED***
