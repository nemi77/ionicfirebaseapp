import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './products.component';
import { AddEditProductComponent } from "./add-edit-product/add-edit-product.component";
import { ViewProductComponent } from "./view-product/view-product.component";

const routes: Routes = [
    {
        path: '',
        component: ProductComponent,
***REDACTED***
