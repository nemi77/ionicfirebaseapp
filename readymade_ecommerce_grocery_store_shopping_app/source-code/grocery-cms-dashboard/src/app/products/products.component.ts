import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { CrudService } from '../../service/crud.service';
import { SubCategoryListModel } from "../sub-category/sub-category.component";
import { ProductService } from "./products.service";
import { CategoryService } from "../categories/category.service";
import { SubCategoryService } from "../sub-category/sub-category.service";
import { UtilService } from "../../service/util.service";
import { CategoryModel } from 'app/categories/categories.component';
import { ProductModel } from 'app/products/products.model';
***REDACTED***
