import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SocketSharedService } from './SocketShare.service';
import { CrudService } from '../service/crud.service';
import { TranslateService } from '@ngx-translate/core';
import { OrderService } from "./orders/order.service";
import { ProductService } from "./products/products.service";

***REDACTED***
