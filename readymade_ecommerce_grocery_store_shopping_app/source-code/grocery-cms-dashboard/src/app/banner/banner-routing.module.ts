import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannerComponent } from './banner.component';
import { AddEditBannerComponent } from "./add-edit-banner/add-edit-banner.component";

const routes: Routes = [
	{
		path: '',
		component: BannerComponent,
		data: {
***REDACTED***
