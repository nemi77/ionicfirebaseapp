import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BannerService } from "./banner.service";
import { UtilService } from "../../service/util.service";
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

export interface BannerModel {
	_id?: string;
	title: string;
	description: string;
***REDACTED***
