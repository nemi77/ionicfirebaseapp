import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubCategoryComponent } from './sub-category.component';
import { AddEditSubCategoryComponent } from './add-edit-subcategory/add-edit-subcategory.component';

const routes: Routes = [
	{
		path: '',
		component: SubCategoryComponent,
		data: {
***REDACTED***
