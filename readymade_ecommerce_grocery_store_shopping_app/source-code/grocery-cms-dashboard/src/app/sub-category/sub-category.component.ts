import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SubCategoryService } from "./sub-category.service";
import { UtilService } from "../../service/util.service";
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

export interface SubCategoryDataModel {
	title: string;
	categoryId: string;
	description: string;
***REDACTED***
