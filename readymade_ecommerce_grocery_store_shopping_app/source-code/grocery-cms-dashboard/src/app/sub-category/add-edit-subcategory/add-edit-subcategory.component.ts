import { Component, OnInit } from '@angular/core';
import { SubCategoryService } from "../sub-category.service";
import { UtilService } from "../../../service/util.service";
import { SubCategoryDataModel, SubCategoryListModel } from "../sub-category.component";
import { CategoryService } from "../../categories/category.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CategoryModel } from "../../categories/categories.component";

@Component({
	selector: 'app-add-edit-subcategory',
***REDACTED***
