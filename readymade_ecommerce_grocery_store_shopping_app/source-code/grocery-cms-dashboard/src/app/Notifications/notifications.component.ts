import { Component, OnInit } from '@angular/core';
import { UtilService } from "../../service/util.service";
import { NotificationService } from "../notification-list/notification.service";

export interface NotificationModel {
	title: string;
	body: string;
}

@Component({
***REDACTED***
