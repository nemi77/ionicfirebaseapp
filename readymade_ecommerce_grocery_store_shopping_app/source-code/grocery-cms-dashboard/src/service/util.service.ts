import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { CrudService } from "./crud.service";
import { Observable } from "rxjs";

export enum ToastEnum {
	SUCCESS = 'toast-success',
	ERROR = 'toast-error',
	WARNING = 'toast-warning',
	INFO = 'toast-info'
***REDACTED***
