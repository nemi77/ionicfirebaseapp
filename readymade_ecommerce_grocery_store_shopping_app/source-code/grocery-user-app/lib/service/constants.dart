import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'dart:io';

class Constants {
  // app name
  static String appName = 'Readymade Grocery App';

  // delopy url production
  static String apiUrl = DotEnv().env['API_URL'];

***REDACTED***
