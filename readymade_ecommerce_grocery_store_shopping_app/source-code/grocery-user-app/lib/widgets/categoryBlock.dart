import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:readymadeGroceryApp/service/constants.dart';
import 'package:readymadeGroceryApp/style/style.dart';
import 'package:readymadeGroceryApp/widgets/normalText.dart';

class CategoryBlock extends StatelessWidget {
  final image, title;
  final bool isHome;
  final isPath;
***REDACTED***
