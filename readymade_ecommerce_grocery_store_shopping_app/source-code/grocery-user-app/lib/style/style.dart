import 'package:flutter/material.dart';

final primary = const Color(0xFFFFCF2D);
final primaryLight = const Color(0xFFE6EFF7);

final blacktext = const Color(0xFF272A3F);

final red = const Color(0xFFF34949);

final grey = const Color(0xFFdddddd);
***REDACTED***
