import * as mongoose from 'mongoose';
const readline = require('readline');

import { AddressSchema } from './src/address/address.model';
import { BannerSchema } from './src/banner/banner.model';
import { BusinessSchema } from './src/business/business.model';
import { CartSchema } from './src/cart/cart.model';
import { CategorySchema } from './src/categories/categories.model';
import { ChatSchema } from './src/chat/chat.model';
import { CouponSchema } from './src/coupons/coupons.model';
***REDACTED***
