import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CartModule } from '../cart/cart.module';
import { CategoryModule } from '../categories/categories.module';
import { BannerModule } from '../banner/banner.module';
import { DealModule } from '../deals/deals.module';
import { FavouritesModule } from '../favourites/favourites.module';
import { RatingModule } from '../rating/rating.module';
import { SubCategoryModule } from '../sub-categories/sub-categories.module';
import { ProductController } from './products.controller';
***REDACTED***
