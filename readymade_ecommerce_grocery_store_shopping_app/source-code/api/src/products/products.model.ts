import * as mongoose from 'mongoose';
import { ArrayMinSize, IsBoolean, IsEmpty, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsPositive, Max, Min, ValidateNested, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer/decorators';
import { UploadImageResponseDTO } from 'src/utils/app.model';

export const ProductSchema = new mongoose.Schema({
	title: { type: String },
	description: { type: String },
	userId: { type: String },
***REDACTED***
