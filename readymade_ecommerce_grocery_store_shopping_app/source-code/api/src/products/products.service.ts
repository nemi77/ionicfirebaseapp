import { Injectable } from '@nestjs/common';
import { ProductsDTO, PuductStatusDTO, ProductListAdminDTO, ProductTitleListAdminDTO, ImportProductDTO, ProductsSaveDTO } from './products.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ProductService {
	constructor(
		@InjectModel('Product') private readonly productModel: Model<any>
	) { }
***REDACTED***
