import { ApiBearerAuth, ApiConsumes, ApiImplicitFile, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { Body, Controller, UseInterceptors, UploadedFile, Delete, Get, Param, Post, Put, UseGuards, Query, UploadedFiles } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductService } from './products.service';
import { ProductsDTO, PuductStatusDTO, ProductFilterQuery, ProductResponseDTO, ProductResponseUserDTO, ProductDetailsByIdDTO, ProductResponseByCategoryIdDTO, ProductResponseBySubCategoryIdDTO, ProductsResponsePaginationDTO, ProductsAdminResponse, ProductsDropDownResponse, ResponseProductsDetailsDTO, ResponseCategoryAndProductDTOPagenation, ProductsSaveDTO, ResponseCategoryByIdProductDTOPagenation, ProductsResponseBySubCategoryIdPagination } from './products.model';
import { UsersDTO } from '../users/users.model';
import { ExportedFileDTO } from '../users/users.model';
import { UserService } from '../users/users.service';
import { UtilService } from '../utils/util.service';
***REDACTED***
