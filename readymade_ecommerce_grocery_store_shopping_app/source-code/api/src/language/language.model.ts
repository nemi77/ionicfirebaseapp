import * as mongoose from 'mongoose';
import { IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsUrl, IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const LanguageSchema = new mongoose.Schema({
	languageCode: { type: String },
	languageName: { type: String },
	status: { type: Number, default: 1 },
	isDefault: { type: Number, default: 0 },
	webJson: { type: Object },
***REDACTED***
