import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LanguageDTO, LanguageStatusUpdateDTO } from './language.model';
import { LanguageJsonType } from '../utils/app.model';
@Injectable()
export class LanguageService {
    constructor(
        @InjectModel('Language') private readonly languageModel: Model<any>) {
    }
***REDACTED***
