import { Controller, Query, Body, Post, Put, Param, Get, UseGuards, Delete } from '@nestjs/common';
import { LanguageService } from './language.service';
import { LanguageDTO, LanguageStatusUpdateDTO, ResponseFavouritesDTO, ResponseLanguageDetails, ResponseLanguageCMSDetailsDTO } from './language.model';
import { UsersDTO } from '../users/users.model';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UtilService } from '../utils/util.service';
import { ResponseMessage, LanguageJsonType, CommonResponseModel, ResponseErrorMessage, ResponseSuccessMessage, ResponseBadRequestMessage } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';

***REDACTED***
