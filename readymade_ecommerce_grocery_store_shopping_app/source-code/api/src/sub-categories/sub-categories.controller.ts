import { Controller, Body, Post, Get, Put, Param, UseGuards, Delete, Query } from '@nestjs/common';
import { SubCategoryService } from './sub-categories.service';
import { CategoryService } from '../categories/categories.service';
import { SubCategoryStatusDTO, ResponseSubCategoryDTO, ResponseSubCategoryListDTO, ResponseSubCategoryUserListDTO, SubCategorySaveDTO, ResponseSubCategoryDrpodownDTO, ResponseSubCategoryDetailDTO } from './sub-categories.model';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { UtilService } from '../utils/util.service';
import { UsersDTO } from '../users/users.model';
import { ResponseMessage, ResponseSuccessMessage, ResponseErrorMessage, CommonResponseModel, ResponseBadRequestMessage, AdminQuery } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';
***REDACTED***
