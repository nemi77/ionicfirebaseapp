import *as mongoose from 'mongoose';
import { IsNotEmpty, IsString, IsBoolean, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const SubCategorySchema = new mongoose.Schema({
	title: { type: String },
	description: { type: String },
	categoryId: { type: String },
	categoryName: { type: String },
	userId: { type: String },
***REDACTED***
