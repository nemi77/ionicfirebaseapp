import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SubCategoryDTO, SubCategoryStatusDTO, SubCategoryUserDTO, SubCategoryDropdownDTO, SubCategorySaveDTO, } from './sub-categories.model';

@Injectable()
export class SubCategoryService {
	constructor(
		@InjectModel('SubCategory') private readonly subcategoryModel: Model<any>
	) {
***REDACTED***
