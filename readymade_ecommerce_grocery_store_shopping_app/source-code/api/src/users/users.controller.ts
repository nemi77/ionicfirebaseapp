import { Controller, Post, Body, Get, Delete, UseGuards, Param, Put, Res, UseInterceptors, UploadedFile, Query } from '@nestjs/common';
import {
	UserCreateDTO,
	LoginDTO, LogInMobileDTO,
	UsersDTO,
	ChangePasswordDTO,
	ForgotPasswordDTO,
	ResetPasswordDTO, ResetNumberPasswordDTO,
	LanguageUpdateDTO,
	UsersUpdateDTO, UserStatusDTO, AdminDeliveryDTO, ResponseLogin, ResponseMe, ResponseAdminUserList, ResponseAdminDeliveryList,
***REDACTED***
