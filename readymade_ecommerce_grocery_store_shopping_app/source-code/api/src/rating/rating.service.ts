import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RatingDTO, RatingSaveDTO } from './rating.model';

@Injectable()
export class RatingService {
	constructor(
		@InjectModel('Rating') private readonly ratingModel: Model<any>
	) {
***REDACTED***
