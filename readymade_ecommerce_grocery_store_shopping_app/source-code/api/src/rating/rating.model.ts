import * as mongoose from 'mongoose';
import { IsNotEmpty, IsNumber, IsOptional, Max, Min, IsString } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export const RatingSchema = new mongoose.Schema({
	rate: { type: Number },
	description: { type: String },
	productId: { type: String },
	userId: { type: String },
}, {
***REDACTED***
