import { Body, Controller, Headers, Post, UseGuards, Get, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { RatingService } from './rating.service';
import { UsersDTO } from '../users/users.model';
import { RatingSaveDTO } from './rating.model';
import { UtilService } from '../utils/util.service';
import { ResponseMessage, CommonResponseModel, ResponseSuccessMessage, ResponseBadRequestMessage, ResponseErrorMessage } from '../utils/app.model';
import { ProductService } from '../products/products.service';
import { GetUser } from '../utils/jwt.strategy';
***REDACTED***
