import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiImplicitQuery, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { CouponService } from './coupons.service';
import { UsersDTO } from '../users/users.model';
import { CouponsDTO, CouponStatusDTO, ResponseCouponsListData, ResponseCouponsData } from './coupons.model';
import { UtilService } from '../utils/util.service';
import { ResponseMessage, ResponseErrorMessage, ResponseSuccessMessage, AdminSettings, CommonResponseModel, ResponseBadRequestMessage, AdminQuery } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';

***REDACTED***
