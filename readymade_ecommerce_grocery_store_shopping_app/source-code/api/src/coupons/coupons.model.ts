import * as mongoose from 'mongoose';
import { IsArray, IsDate, IsEmpty, IsEnum, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsUrl, Max, Min, IsString, IsBoolean, IsInt } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export enum CouponType {
	PERCENTAGE = 'PERCENTAGE',
	AMOUNT = 'AMOUNT'
}

export const CouponSchema = new mongoose.Schema({
***REDACTED***
