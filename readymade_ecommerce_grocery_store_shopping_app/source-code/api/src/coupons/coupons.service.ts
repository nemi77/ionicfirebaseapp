import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CouponsDTO, CouponStatusDTO } from './coupons.model';

@Injectable()
export class CouponService {
	constructor(
		@InjectModel('Coupon') private readonly couponsModel: Model<any>,
	) { }
***REDACTED***
