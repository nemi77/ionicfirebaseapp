import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class SequenceService {
	constructor(
		@InjectModel('Sequence') private readonly sequenceModel: Model<any>
	) { }

***REDACTED***
