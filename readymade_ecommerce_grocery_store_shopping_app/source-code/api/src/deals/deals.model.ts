import * as mongoose from 'mongoose';
import { IsArray, IsDate, IsEmpty, IsEnum, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsUrl, Max, Min, IsString, IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export enum DealType {
	CATEGORY = 'CATEGORY',
	PRODUCT = 'PRODUCT'
}

export const DealSchema = new mongoose.Schema({
***REDACTED***
