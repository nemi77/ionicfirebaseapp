import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UseInterceptors, UploadedFile, Query } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiConsumes, ApiImplicitFile, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { DealService } from './deals.service';
import { UsersDTO } from '../users/users.model';
import { DealStatusDTO, DealType, DealSaveDTO } from './deals.model';
import { UtilService } from '../utils/util.service';
import { UploadService } from '../utils/upload.service';
import { CategoryService } from '../categories/categories.service';
***REDACTED***
