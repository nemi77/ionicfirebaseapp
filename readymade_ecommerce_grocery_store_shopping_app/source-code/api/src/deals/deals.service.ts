import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DealsDTO, DealStatusDTO, DealType, DealSaveDTO } from './deals.model';

@Injectable()
export class DealService {
	constructor(
		@InjectModel('Deal') private readonly dealsModel: Model<any>
	) {
***REDACTED***
