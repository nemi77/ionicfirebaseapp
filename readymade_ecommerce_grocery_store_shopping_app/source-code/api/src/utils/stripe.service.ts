import { Injectable } from '@nestjs/common';
import * as Stripe from 'stripe';
let stripe: any;

@Injectable()
export class StripeService {
	constructor() {
		if (process.env.STRIPE_SECRET_KEY) stripe = new Stripe(process.env.STRIPE_SECRET_KEY);
		else console.log("STRIPE_SECRET_KEY not set.");
	}
***REDACTED***
