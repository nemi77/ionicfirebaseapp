import { Injectable } from '@nestjs/common';
import { PassportStrategy, AuthGuard } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { createParamDecorator } from '@nestjs/common';
import { UserService } from '../users/users.service';
import { UtilService } from './util.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(
***REDACTED***
