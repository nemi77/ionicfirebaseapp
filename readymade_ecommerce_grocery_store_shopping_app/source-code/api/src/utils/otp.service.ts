import { Injectable } from '@nestjs/common';
import { TwilioResponseDTO, ResponseMessage } from './app.model';
const request = require('request');
var client;
@Injectable()
export class OtpService {
	constructor() {
		if (process.env.USE_SENDINBLUE == 'false') {
			if (process.env.TWILIO_ACCOUNT_SID && process.env.TWILIO_AUTH_TOKEN) client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
			else console.log("TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN is not set");
***REDACTED***
