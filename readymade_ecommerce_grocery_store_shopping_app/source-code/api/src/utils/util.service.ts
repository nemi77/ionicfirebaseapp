import { Injectable, HttpStatus, HttpException, BadRequestException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import * as geoLib from 'geolib';
import * as uuid from 'uuid/v1';
import { UserRoles, ResponseMessage, AdminQuery, AdminSettings, UserQuery, UserSettings } from './app.model';
import { Promise } from 'mongoose';
let language = "en";
let languageList = [];

@Injectable()
export class UtilService {
***REDACTED***
