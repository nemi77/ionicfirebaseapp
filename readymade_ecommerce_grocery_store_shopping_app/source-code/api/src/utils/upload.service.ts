import { Injectable } from '@nestjs/common';
import { UploadImageResponseDTO } from '../utils/app.model';
const ImageKit = require("imagekit");
let imagekit;

@Injectable()
export class UploadService {
	constructor() {
		if (process.env.IMAGEKIT_PUBLIC_KEY && process.env.IMAGEKIT_PRIVATE_KEY && process.env.IMAGEKIT_URL) {
			imagekit = new ImageKit({
***REDACTED***
