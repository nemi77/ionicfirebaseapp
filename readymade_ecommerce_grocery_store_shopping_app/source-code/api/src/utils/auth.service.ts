import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { genSalt, compare, hash } from 'bcrypt';

@Injectable()
export class AuthService {
	constructor(
		private jwtService: JwtService
	) {

***REDACTED***
