import { Injectable } from '@nestjs/common';
const Excel = require('exceljs');
const appRoot = require('app-root-path');
const fs = require('fs');
import { UploadService } from './upload.service';
import { UserService } from '../users/users.service';
import { ImportProductDTO } from '../products/products.model';
import { UtilService } from './util.service';
import { ResponseMessage } from './app.model';

***REDACTED***
