import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SubCategoryModule } from '../sub-categories/sub-categories.module';
import { ProductModule } from '../products/products.module';
import { BannerModule } from '../banner/banner.module';
import { DealModule } from '../deals/deals.module';
import { CategoryController } from './categories.controller';
import { CategorySchema } from './categories.model';
import { CategoryService } from './categories.service';

***REDACTED***
