import { Controller, UseGuards, Post, UseInterceptors, UploadedFile, Delete, Param, Body, Get, Put, Query } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CategoryService } from './categories.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiConsumes, ApiImplicitFile, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { CategoryStatusUpdateDTO, CategorySaveDTO, ResponseUserCategoryList, ResponseCategoryAdmin, CategoryAdminDetailDTO, ResponseDropDown } from './categories.model';
import { UtilService } from '../utils/util.service';
import { UploadService } from '../utils/upload.service';
import { ResponseMessage, UploadImageDTO, UploadImageResponseDTO, ResponseErrorMessage, ResponseSuccessMessage, CommonResponseModel, ResponseBadRequestMessage, AdminQuery } from '../utils/app.model';
***REDACTED***
