import * as mongoose from 'mongoose';
import { IsNotEmpty, IsOptional, IsUrl, IsMongoId, IsEmpty, IsBoolean, IsNumber, IsPositive, Min, Max, IsArray, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const CategorySchema = new mongoose.Schema({
	title: { type: String },
	description: { type: String },
	subCategoryCount: { type: Number, default: 0 },
	imageUrl: { type: String, required: true },
	imageId: { type: String, required: false },
***REDACTED***
