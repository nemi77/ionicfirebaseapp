import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CategoryStatusUpdateDTO, CategoryListDTO, CategoryAdminListDTO, CategoryAdminDetailDTO, CategorySaveDTO } from './categories.model';

@Injectable()
export class CategoryService {
	constructor(
		@InjectModel('Category') private readonly categoryModel: Model<any>
	) {
***REDACTED***
