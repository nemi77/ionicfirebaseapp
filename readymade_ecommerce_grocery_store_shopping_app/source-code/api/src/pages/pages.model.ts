import * as mongoose from 'mongoose';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, IsOptional } from 'class-validator';

export enum PageType {
	ABOUT_US = 'ABOUT_US',
	TERMS_AND_CONDITIONS = 'TERMS_AND_CONDITIONS',
	PRIVACY_POLICY = 'PRIVACY_POLICY'
}

***REDACTED***
