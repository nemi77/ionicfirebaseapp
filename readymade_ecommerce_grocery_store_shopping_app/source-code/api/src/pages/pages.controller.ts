import { Controller, Get, UseGuards, Query, Put, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiUseTags, ApiResponse } from '@nestjs/swagger';


import { UsersDTO } from '../users/users.model';
import { PageService } from './pages.service';
import { PageDTO, PageSaveDTO, PageType, ResponsePageDTO, ResponseAdminPageDTO } from './pages.model';
import { UtilService } from '../utils/util.service';
import { UserRoles, ResponseMessage, CommonResponseModel, ResponseErrorMessage, ResponseSuccessMessage, ResponseBadRequestMessage } from '../utils/app.model';
***REDACTED***
