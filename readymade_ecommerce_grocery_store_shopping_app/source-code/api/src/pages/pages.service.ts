import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PageSaveDTO, PageDTO, PageType } from './pages.model';

@Injectable()
export class PageService {
	constructor(
		@InjectModel('Page') private readonly pageModel: Model<any>
	) {
***REDACTED***
