import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { WalletDTO, WalletSaveDTO, WalletTransactionType } from './wallet.model';

@Injectable()
export class WalletService {
	constructor(
		@InjectModel('Wallet') private readonly walletModel: Model<any>
	) { }
***REDACTED***
