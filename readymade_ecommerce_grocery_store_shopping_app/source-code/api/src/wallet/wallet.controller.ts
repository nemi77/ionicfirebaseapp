import { Controller, UseGuards, Get, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { CommonResponseModel, ResponseErrorMessage, UserQuery } from '../utils/app.model';
import { WalletService } from './wallet.service';
import { ResponseWalletHistory } from './wallet.model';
import { UtilService } from '../utils/util.service';
import { GetUser } from '../utils/jwt.strategy';

***REDACTED***
