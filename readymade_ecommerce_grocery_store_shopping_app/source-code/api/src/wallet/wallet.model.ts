import * as mongoose from 'mongoose';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber, IsString, IsBase64, IsBoolean } from 'class-validator';

export enum WalletTransactionType {
	ORDER_CANCELLED = 'ORDER_CANCELLED',
	ORDER_PAYMENT = 'ORDER_PAYMENT'
}

export const WalletSchema = new mongoose.Schema({
***REDACTED***
