import { Body, Controller, Query, Get, Param, Post, UseGuards, Put, Res } from '@nestjs/common';
import { OrderService } from './order.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation, ApiImplicitQuery } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { OrderStatusDTO, AssignOrderDTO, PaymentType, OrderStatusType, PaymentFrom, OrderFilterQuery, OrderCreateDTO, StripePaymentStatus, DBStatusUpdateDTO, ResponseOrderDTOPagination, ResponseDataOfOrder, ResponseOrderAdminListDTO, ResponseOrderForAdmin, ResponseOrderDetailsOrderId, ResponseStatusList, ResponseChardOrderDTO, ResponseDeiveryBoyPagination, ResponseDeliveredOrderPagination, PaymentStatusType, ResponseAdminOrderDetailsOrderId } from './order.model';
import { ResponseMessage, AdminSettings, CommonResponseModel, ResponseErrorMessage, ResponseBadRequestMessage, ResponseSuccessMessage, UserQuery } from '../utils/app.model';
import { UtilService } from '../utils/util.service';
import { WalletService } from '../wallet/wallet.service';
import { WalletSaveDTO, WalletTransactionType } from '../wallet/wallet.model';
***REDACTED***
