import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategoryModule } from '../categories/categories.module';
import { CartModule } from '../cart/cart.module';
import { AddressModule } from '../address/address.module';
import { NotificationsModule } from '../notifications/notifications.module';
import { SettingModule } from '../settings/settings.module';
import { ProductModule } from '../products/products.module';
import { SequenceModule } from '../sequence/sequence.module';
import { WalletModule } from '../wallet/wallet.module';
***REDACTED***
