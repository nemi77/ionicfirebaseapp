import { Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { FavouriteService } from './favourites.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { FavouritesDTO, ResponseFavouritesDTO } from './favourites.model';
import { UtilService } from '../utils/util.service';
import { UserRoles, ResponseMessage, ResponseSuccessMessage, ResponseErrorMessage, CommonResponseModel, ResponseBadRequestMessage } from '../utils/app.model';
import { ProductService } from '../products/products.service';
import { CartService } from '../cart/cart.service';
***REDACTED***
