import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FavouritesDTO } from './favourites.model';

@Injectable()
export class FavouriteService {
	constructor(
		@InjectModel('Favourite') private readonly favouritesModel: Model<any>
	) { }
***REDACTED***
