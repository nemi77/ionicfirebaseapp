import * as mongoose from 'mongoose';
import { IsEmpty, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { FavourutesResponseDTO } from '../products/products.model'

export const FavouriteSchema = new mongoose.Schema({
	productId: { type: Array },
	userId: { type: String }
}, {
	timestamps: true
***REDACTED***
