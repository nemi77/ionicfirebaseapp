import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CartDataModel, UserCartDTO } from './cart.model';
import { UtilService } from '../utils/util.service';
import { ResponseMessage } from '../utils/app.model';
import { DeliveryType } from '../settings/settings.model';
import { NotificationType } from '../notifications/notifications.model';
import { CouponService } from '../coupons/coupons.service';
import { CouponType } from '../coupons/coupons.model';
***REDACTED***
