import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { CartService } from './cart.service';
import { UsersDTO } from '../users/users.model';
import { CartUpdateDTO, ResponseMyCartDetail, UpdateAddressDTO } from './cart.model';
import { CouponType } from '../coupons/coupons.model';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { UtilService } from '../utils/util.service';
import { SettingService } from '../settings/settings.service';
import { DeliveryType } from '../settings/settings.model';
***REDACTED***
