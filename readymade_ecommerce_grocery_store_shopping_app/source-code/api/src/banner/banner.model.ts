import * as mongoose from 'mongoose';
import { IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsUrl, IsString, IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export enum BannerType {
	CATEGORY = 'CATEGORY',
	PRODUCT = 'PRODUCT'
}

export const BannerSchema = new mongoose.Schema({
***REDACTED***
