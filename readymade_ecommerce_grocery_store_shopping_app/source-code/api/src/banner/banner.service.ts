import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BannerDTO, BannerSaveDTO, BannerType } from './banner.model';

@Injectable()
export class BannerService {
	constructor(
		@InjectModel('Banner') private readonly bannerModel: Model<any>,
	) {
***REDACTED***
