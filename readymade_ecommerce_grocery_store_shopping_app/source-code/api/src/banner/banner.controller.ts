import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, Query, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { BannerService } from './banner.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiConsumes, ApiImplicitFile, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { BannerType, BannerSaveDTO, ResponseUserBannerList, ResponseBannerList, ResponseBannerType, ResponseBanner } from './banner.model';
import { UtilService } from '../utils/util.service';
import { UserRoles, ResponseMessage, UploadImageDTO, UploadImageResponseDTO, ResponseErrorMessage, ResponseSuccessMessage, AdminSettings, CommonResponseModel, ResponseBadRequestMessage, AdminQuery } from '../utils/app.model';
import { UploadService } from '../utils/upload.service';
***REDACTED***
