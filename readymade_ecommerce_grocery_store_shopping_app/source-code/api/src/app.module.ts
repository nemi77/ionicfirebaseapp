import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { UsersModule } from './users/users.module';
import { CategoryModule } from './categories/categories.module';
import { DealModule } from './deals/deals.module';
import { ProductModule } from './products/products.module';
import { AddressModule } from './address/address.module';
import { FavouritesModule } from './favourites/favourites.module';
import { OrderModule } from './order/order.module';
***REDACTED***
