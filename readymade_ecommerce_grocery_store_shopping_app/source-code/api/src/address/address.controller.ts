import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { AddressService } from './address.service';
import { UsersDTO } from '../users/users.model';
import { AddressSaveDTO, ResponseAddress, ResponseAddressList, ResponseAddressDropdown } from './address.model';
import { UtilService } from '../utils/util.service';
import { ResponseMessage, ResponseErrorMessage, ResponseSuccessMessage, CommonResponseModel, ResponseBadRequestMessage } from '../utils/app.model';
import { SettingService } from '../settings/settings.service';
import { GetUser } from '../utils/jwt.strategy';
***REDACTED***
