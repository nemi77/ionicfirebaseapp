import * as mongoose from 'mongoose';
import { IsEnum, IsNotEmpty, IsOptional, ValidateNested, IsNumber, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export enum AddressType {
	HOME = 'HOME',
	WORK = 'WORK',
	OTHERS = 'OTHERS'
}
***REDACTED***
