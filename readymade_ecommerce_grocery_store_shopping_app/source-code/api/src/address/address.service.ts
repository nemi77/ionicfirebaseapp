import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AddressDTO, AddressSaveDTO, AddressType } from './address.model';

@Injectable()
export class AddressService {
	constructor(
		@InjectModel('Address') private readonly addressModel: Model<any>
	) {
***REDACTED***
