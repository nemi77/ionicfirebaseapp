import * as mongoose from 'mongoose';
import { IsMongoId, IsNotEmpty, IsOptional, IsUrl, IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const BusinessSchema = new mongoose.Schema({
	storeName: { type: String },
	address: { type: String },
	email: { type: String },
	phoneNumber: { type: Number },
	officeLocation: { type: String }
***REDACTED***
