import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BusinessDTO, BusinessAdminDTO, BusinessSaveDTO } from './business.model';

@Injectable()
export class BusinessService {
	constructor(
		@InjectModel('Business') private readonly businessModel: Model<any>
	) {
***REDACTED***
