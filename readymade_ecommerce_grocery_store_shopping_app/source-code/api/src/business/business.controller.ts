import { Controller, Body, Put, Param, Get, UseGuards } from '@nestjs/common';
import { BusinessService } from './business.service';
import { BusinessSaveDTO, ResponseBusinessUser, ResponseBusinessDetailAdmin } from './business.model';
import { UsersDTO } from '../users/users.model';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { ResponseMessage, ResponseErrorMessage, ResponseSuccessMessage, CommonResponseModel, ResponseBadRequestMessage } from '../utils/app.model';
import { UtilService } from '../utils/util.service';
import { GetUser } from '../utils/jwt.strategy';

***REDACTED***
