import { Controller, Get, UseGuards, Query } from '@nestjs/common';
import { ProductOutOfStockService } from './product-out-of-stock.service';
import { ApiOperation, ApiImplicitQuery, ApiResponse, ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../utils/jwt.strategy';
import { UsersDTO } from '../users/users.model';
import { OrderFilterQuery } from 'src/order/order.model';
import { ResponseErrorMessage, CommonResponseModel, AdminSettings, AdminQuery } from '../utils/app.model';
import { StockVariantDTO } from './product-out-of-stock.model';
import { UtilService } from '../utils/util.service';
***REDACTED***
