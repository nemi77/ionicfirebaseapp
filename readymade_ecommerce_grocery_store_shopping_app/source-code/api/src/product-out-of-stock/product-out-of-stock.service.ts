import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StockVariantDTO } from './product-out-of-stock.model';

@Injectable()
export class ProductOutOfStockService {

	constructor(
		@InjectModel('ProductOutOfStock') private readonly productOutOfStockModel: Model<any>
***REDACTED***
