import * as mongoose from 'mongoose';
import { IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsUrl, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export enum ChatSenyByType {
	USER = 'USER',
	STORE = 'STORE'
}

export const ChatSchema = new mongoose.Schema({
***REDACTED***
