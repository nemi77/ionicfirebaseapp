import { Controller, Get, UseGuards, Query, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ResponseErrorMessage, CommonResponseModel, AdminQuery, UserQuery } from '../utils/app.model';
import { UtilService } from '../utils/util.service';
import { GetUser } from '../utils/jwt.strategy';
import { ChatService } from './chat.service';
import { UsersDTO } from '../users/users.model';

@Controller('chats')
***REDACTED***
