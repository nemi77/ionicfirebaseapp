import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DeliveryBoyRatingSaveDTO } from './delivery-boy-ratings.model';

@Injectable()
export class DeliveryBoyRatingsService {
	constructor(
		@InjectModel('DeliveryBoyRating') private readonly deliveryBoyRatingModel: Model<any>
	) {
***REDACTED***
