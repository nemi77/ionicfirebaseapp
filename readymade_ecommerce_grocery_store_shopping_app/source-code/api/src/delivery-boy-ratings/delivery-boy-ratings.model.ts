import * as mongoose from 'mongoose';
import { IsNotEmpty, IsNumber, IsOptional, Max, Min, IsString } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export const DeliveryBoyRatingSchema = new mongoose.Schema({
	rate: { type: Number },
	description: { type: String },
	orderId: { type: String },
	userId: { type: String },
	deliveryBoyId: { type: String },
***REDACTED***
