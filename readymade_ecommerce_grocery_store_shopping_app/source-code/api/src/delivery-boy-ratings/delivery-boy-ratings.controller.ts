import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { ResponseSuccessMessage, ResponseBadRequestMessage, ResponseErrorMessage, CommonResponseModel, ResponseMessage } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';
import { DeliveryBoyRatingSaveDTO } from './delivery-boy-ratings.model';
import { DeliveryBoyRatingsService } from './delivery-boy-ratings.service';
import { UtilService } from '../utils/util.service';
import { OrderService } from '../order/order.service'
import { OrderStatusType } from '../order/order.model';
***REDACTED***
