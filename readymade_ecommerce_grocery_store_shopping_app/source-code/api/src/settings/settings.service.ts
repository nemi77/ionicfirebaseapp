import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SettingDTO, UpdateCurrencyDTO, SettingCurrencyAndLanguageListDTO, SettingWorkingHoursDTO, SettingCurrencyAndLanguageCodeDTO, SettingCurrencyAndLanguageDTO, WorkingHoursDTO, DeliveryTaxSaveDTO } from './settings.model';
import { UtilService } from '../utils/util.service';


@Injectable()
export class SettingService {
	constructor(
***REDACTED***
