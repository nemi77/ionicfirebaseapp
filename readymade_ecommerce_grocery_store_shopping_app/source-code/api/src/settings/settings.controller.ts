import { Controller, Headers, UseGuards, Post, Body, Delete, Param, Get, Put, Res } from '@nestjs/common';
import { SettingService } from './settings.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { UtilService } from '../utils/util.service';
import { CurrencyService } from '../utils/currency.service'
import { DeliveryTaxSaveDTO, UpdateCurrencyDTO, ResponseSettingDetails, ResponseTimeSlotDTO, ResponseSettigsAdminDTO, ResponseTimeSlotDetails, ResponseTimeDTO, ResponseCurrencyDetailsAdmin, ResponseCurrencyListDTO, ResponseTimeSlotDropDown } from './settings.model';
import { ResponseMessage, CommonResponseModel, ResponseErrorMessage, ResponseSuccessMessage, ResponseBadRequestMessage } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';
***REDACTED***
