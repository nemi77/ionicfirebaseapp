import * as mongoose from 'mongoose';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsArray, IsEmpty, IsNotEmpty, IsNumber, IsOptional, Max, Min, IsUrl, IsString, IsBoolean } from 'class-validator';

export enum NotificationType {
	ORDER_PLACED = 'ORDER_PLACED',
	ORDER_CANCELLED = 'ORDER_CANCELLED',
	ORDER_ACCEPTED_BY_DELIVERY_BOY = 'ORDER_ACCEPTED_BY_DELIVERY_BOY',
	ORDER_REJECTED_BY_DELIVERY_BOY = 'ORDER_REJECTED_BY_DELIVERY_BOY',
	PRODUCT_OUT_OF_STOCK = 'PRODUCT_OUT_OF_STOCK',
***REDACTED***
