import { Controller, Get, UseGuards, Query, Post, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { NotificationService } from './notifications.service';
import { UtilService } from '../utils/util.service';
import { CommonResponseModel, ResponseErrorMessage, ResponseSuccessMessage, ResponseBadRequestMessage, ResponseMessage, AdminSettings, AdminQuery } from '../utils/app.model';
import { GetUser } from '../utils/jwt.strategy';
import { ResponseNotificationListDTO, SendNotificationDTO, readNotificationDTO } from './notifications.model';
import { PushService } from '../utils/push.service';
***REDACTED***
