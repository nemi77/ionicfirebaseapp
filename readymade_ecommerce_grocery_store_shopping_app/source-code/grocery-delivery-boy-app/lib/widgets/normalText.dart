import 'package:flutter/material.dart';
import 'package:grocerydelivery/services/localizations.dart';
import 'package:grocerydelivery/styles/styles.dart';

Widget buildTextField(BuildContext context, title) {
  return TextFormField(
    enabled: false,
    controller: title,
    cursorColor: primary,
    decoration: InputDecoration(
***REDACTED***
