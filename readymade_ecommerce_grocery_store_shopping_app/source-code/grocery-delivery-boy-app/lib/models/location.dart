import 'package:flutter/foundation.dart';
import 'package:location/location.dart';

class LocationModel extends ChangeNotifier {
  Location location = new Location();
  LocationData _locationData;

  void requestLocation() async {
    PermissionStatus _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
***REDACTED***
