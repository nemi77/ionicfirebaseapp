import 'dart:convert';

import 'package:grocerydelivery/services/interCepter.dart';
import 'package:http/http.dart' show Client;
import 'package:http_interceptor/http_interceptor.dart';

import 'constants.dart';

Client client =
    HttpClientWithInterceptor.build(interceptors: [ApiInterceptor()]);
***REDACTED***
