import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Constants {
  // app name
  static String appName = 'Readymade Grocery Delivery App';

  // Base url
  static String apiURL = DotEnv().env['API_URL'];

***REDACTED***
