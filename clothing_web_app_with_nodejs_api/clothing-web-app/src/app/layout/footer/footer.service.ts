import { ConstantService } from './../../services/constant.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudService } from '../../services/crud.service';

@Injectable()
export class FooterService {
  constructor(private crud: CrudService, private http: HttpClient, private constService: ConstantService) {
  }

***REDACTED***
