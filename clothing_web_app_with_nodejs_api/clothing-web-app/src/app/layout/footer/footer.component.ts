import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FooterService } from './footer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  providers: [FooterService]
***REDACTED***
