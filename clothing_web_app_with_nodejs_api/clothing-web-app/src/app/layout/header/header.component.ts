import { Component, OnInit, EventEmitter, Inject, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { WINDOW } from '../../window.service';
import { LoginService } from '../../pages/clothing/login/login.service';
import { HeaderService } from './header.service';
import { HeaderDirective } from './header.directive';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
***REDACTED***
