import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ClothingModule } from './clothing/clothing.module';


export const routes = [
  { path: 'clothing', loadChildren: 'app/pages/clothing/clothing.module#ClothingModule' },

***REDACTED***
