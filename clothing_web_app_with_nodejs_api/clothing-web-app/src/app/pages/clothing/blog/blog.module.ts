import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlModule } from 'ng2-owl-carousel';
import { BlogLsComponent } from './blog-ls/blog-ls.component';
import { BlogNsComponent } from './blog-ns/blog-ns.component';
import { BlogRsComponent } from './blog-rs/blog-rs.component';
import { BlogSingleLsComponent } from './blog-single-ls/blog-single-ls.component';
import { BlogSingleNsComponent } from './blog-single-ns/blog-single-ns.component';
***REDACTED***
