import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';

@Injectable()
export class OrderDetailsService {
  constructor(private crud: CrudService) {
  }

  getOrderDetail(id: string) {
    return this.crud.getOne('orders', id);
***REDACTED***
