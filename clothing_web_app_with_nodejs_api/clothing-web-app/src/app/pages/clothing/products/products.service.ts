import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProductsService {
  constructor(private crud: CrudService, private http: HttpClient) {
  }

  // returns list of all products based on pagination limit
***REDACTED***
