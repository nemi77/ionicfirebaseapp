import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class AddressService {
  constructor(private crud: CrudService) {
  }

  getUserAddresses() {
    return this.crud.getUser('addresses');
***REDACTED***
