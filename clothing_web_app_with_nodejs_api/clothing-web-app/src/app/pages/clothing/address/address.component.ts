import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AddressService } from './address.service';
import { ToastrService } from 'ngx-toastr';
import * as fromCart from '../shop/store/cart.reducers';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';

***REDACTED***
