import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';

@Injectable()
export class PaymentService {
  constructor(private crud: CrudService) {
  }

  // returns payment related information i.e sandbox id, private key, public ket etc.
  getPaymentDetails() {
***REDACTED***
