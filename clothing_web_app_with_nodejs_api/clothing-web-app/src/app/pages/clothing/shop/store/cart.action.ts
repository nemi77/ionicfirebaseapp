import { Action } from '@ngrx/store';

export const ADD_TO_CART = 'ADD_TO_CART';   //add item to cart type
export const UPDATE_CART_ITEM = 'UPDATE_CART_ITEM';  // update cart item type
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';  // delete cart item type

// dispatches this action when user add item to cart
export class AddToCart implements Action {
  readonly type = ADD_TO_CART;

***REDACTED***
