import { Component, OnInit } from '@angular/core';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { ShopService } from '../shop.services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shop-list-ls',
  templateUrl: './shop-list-ls.component.html',
  styleUrls: ['./shop-list-ls.component.scss'],
  providers: [NgbAccordionConfig, ShopService]
***REDACTED***
