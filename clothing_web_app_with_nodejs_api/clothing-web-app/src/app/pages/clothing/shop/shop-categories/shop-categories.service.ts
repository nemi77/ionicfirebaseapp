import { Injectable } from '@angular/core';
import { CrudService } from '../../../../services/crud.service';

@Injectable()
export class ShopCategoryService {
  constructor(private crud: CrudService) {
  }

  // returns list of all categories based on pagination limit
  getCategories(page, limit) {
***REDACTED***
