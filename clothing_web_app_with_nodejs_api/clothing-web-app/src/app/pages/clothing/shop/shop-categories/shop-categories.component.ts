import { Component, OnInit } from '@angular/core';
import { ShopCategoryService } from './shop-categories.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop-categories',
  templateUrl: './shop-categories.component.html',
  styleUrls: ['./shop-categories.component.scss'],
  providers: [ShopCategoryService]
})
***REDACTED***
