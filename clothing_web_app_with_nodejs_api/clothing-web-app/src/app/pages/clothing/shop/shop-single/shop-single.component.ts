import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { ShopService } from '../shop.services';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import * as fromCart from '../store/cart.reducers';
import * as CartActions from '../store/cart.action';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-shop-single',
***REDACTED***
