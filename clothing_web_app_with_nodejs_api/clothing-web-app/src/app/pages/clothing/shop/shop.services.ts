import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CrudService } from '../../../services/crud.service';

@Injectable()
export class ShopService {

  constructor(public http: HttpClient, private crud: CrudService) {
  }
***REDACTED***
