import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromCart from '../store/cart.reducers';
import * as CartActions from '../store/cart.action';
import { CartService } from './cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';

***REDACTED***
