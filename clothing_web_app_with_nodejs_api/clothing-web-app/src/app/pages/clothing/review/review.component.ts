import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ReviewService } from './review.service';
import swal from 'sweetalert2';
import * as fromCart from '../shop/store/cart.reducers';
import * as CartActions from '../shop/store/cart.action';
import { Observable } from 'rxjs/Observable';

@Component({
***REDACTED***
