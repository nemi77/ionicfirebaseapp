import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from '../../../services/constant.service';
import { CrudService } from '../../../services/crud.service';

@Injectable()
export class LoginService {
  constructor(private http: HttpClient, private crud: CrudService, private _costantService: ConstantService) {
  }

***REDACTED***
