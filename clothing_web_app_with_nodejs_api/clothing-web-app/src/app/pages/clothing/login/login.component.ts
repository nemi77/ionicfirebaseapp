import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angular4-social-login';
import { FacebookService, LoginOptions, LoginResponse } from 'ngx-facebook';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie';

declare const gapi: any;
***REDACTED***
