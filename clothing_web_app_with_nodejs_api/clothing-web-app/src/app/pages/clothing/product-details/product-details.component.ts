import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductDetailsService } from './product-details.service';
import swal from 'sweetalert2';
import * as fromCart from '../shop/store/cart.reducers';
import * as CartActions from '../shop/store/cart.action';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
***REDACTED***
