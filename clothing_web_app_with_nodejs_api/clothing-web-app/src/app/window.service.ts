import { ClassProvider, FactoryProvider, InjectionToken } from "@angular/core";

export function _window(): any {
  return window;
}

export const WINDOW = new InjectionToken("WindowToken");

export abstract class WindowRef {
  get nativeWindow(): Window {
***REDACTED***
